use retracer::interface::component_editor::InputTypeEditor;

/// A named counter
struct Counter {
    name: String,
    value: usize,
}

impl Counter {
    pub fn type_descriptions() -> ElementTypeEditor {
        ElementTypeEditor::new("Counter").attributes(vec!["name", "value"])
    }
}

/// Incrementable and decrementable Counters
struct Board {
    title: String,
    counters: Vec<Counter>,
    incrementor: Incrementor,
}

impl Board {
    pub fn type_descriptions() -> ElementTypeEditor {
        ElementTypeEditor::new("Board").attributes(vec!["title", "counters", "incrementor"])
    }
    pub fn title(&self) -> &String {
        &self.title
    }
    pub fn counters(&self) -> &Vec<Counter> {
        &self.counters
    }
    pub fn incrementor(&self) -> &Incrementor {
        &self.incrementor
    }
    pub fn title(&mut self) -> &mut String {
        &mut self.title
    }
    pub fn counters(&mut self) -> &mut Vec<Counter> {
        &mut self.counters
    }
    pub fn incrementor(&mut self) -> &mut Incrementor {
        &mut self.incrementor
    }
}

/// Increments and decrements a selection of Counters.
/// The increment and decrement values are Counters themselves.
struct Incrementor {
    selection: Vec<usize>,
    inc: Counter,
    dec: Counter,
}

impl Incrementor {
    pub fn type_descriptions() -> ElementTypeEditor {
        ElementTypeEditor::new("Incrementor").attributes(vec!["selection", "inc", "dec"])
    }
}

////////  Elements  ////////

pub fn register_element_types() {
    history
        .register_new_element_type(vec![
            Counter::type_description(),
            Board::type_description(),
            Incrementor::type_description(),
        ])
        .unwrap()
}

////////  Inputs  ////////

pub fn register_input_types() {
    history
        .register_new_element_type(vec![
            InputTypeEditor::new("Integer"),
            InputTypeEditor::new("Name"),
        ])
        .unwrap()
}
