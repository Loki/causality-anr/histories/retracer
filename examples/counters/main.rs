mod commands;
mod elements;


use retracer::{
    interface::{
        change::command_factory::CommandFactory,
        history::History,
    }, misc::database::nodb::NoDB,
};

fn main() {
    // Initialize the history with a database type and a command factory
    let mut history = History<NoDB>::init(CommandFactory::new());

    // register types
    commands::register_command_types();
    elements::register_element_types();
    elements::register_input_types();

}
