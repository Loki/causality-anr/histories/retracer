/*! Simple example
*/

struct IncrementByOne {}

impl Command for IncrementByOne {
    fn run(counter: usize) -> Result<(), ()> {
        counter.set(counter.value() + 1);
        Ok(())
    }
}

fn main() -> Result<(), ()> {
    let view = retracer::Retracer::new();
    view.run(init_with, ElementVersion("Counter1", 0)))
    let cmd = IncrementByOne {};

    view.run(counter);

    Ok(())
}
