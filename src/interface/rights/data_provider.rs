use crate::{database::Database, history::reference::Reference, interface::view::View};

pub trait DataProvider<D: Database> {
    fn get_record(&self, reference: &dyn Into<Reference>) -> Option<D::RecordData>;
}

impl<'a, R, D: Database> DataProvider<D> for View<R, D> {
    fn get_record(&self, reference: &dyn Into<Reference>) -> Option<<D as Database>::RecordData> {
        self.query(reference).data(&self)
    }
}
