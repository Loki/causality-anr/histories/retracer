pub mod data_provider;
pub mod history_provider;
pub mod rights_1_read_only;
pub mod rights_2_meta_data_edition;
pub mod rights_3_tes_management;
pub mod rights_4_execution_recording;
pub mod rights_5_history_edition;
