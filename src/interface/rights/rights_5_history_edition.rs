/*! Complete history editing rights
In this mode, the history can be edited while keeping its coherence.
*/

/// History edition rights marker
pub(crate) struct HistoryEditionRights();

// /// Methods available with HistoryEdition rights
//pub(crate) trait HistoryEdition<R>: HistoryProvider {}
// put everything in the control module and only keep the marker struct for queries
// nor HistoryInterface nor View can get these rights. There is thus no trait for it
