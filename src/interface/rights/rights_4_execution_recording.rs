use crate::{
    database::Database,
    history::reference::input::InputReference,
    interface::{change::input::Input, view::View},
};

use super::history_provider::HistoryProvider;

pub(crate) struct ExecutionRecordingRights();

pub(crate) trait ExecutionRecording: HistoryProvider {
    fn record_input(&mut self, input: &dyn Input) -> Result<InputReference, ()> {}
}

impl<D: Database> ExecutionRecording for View<ExecutionRecordingRights, D> {}
