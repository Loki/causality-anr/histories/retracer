/*! Read only history access rights
In this mode, the queries will allow reading the history's data only.
*/

use crate::{
    database::Database,
    history::ids::{CommandTypeName, ElementTypeName, Identifier, InputTypeName, NameType},
    interface::{
        history::HistoryInterface,
        query::{
            command_type::CommandTypeQuery, command_type_iterator::CommandTypeQueryIterator,
            element::ElementQuery, element_iterator::ElementQueryIterator,
            element_type::ElementTypeQuery, element_type_iterator::ElementTypeQueryIterator,
            input_type::InputTypeQuery, input_type_iterator::InputTypeQueryIterator,
            into_query::IntoQuery, session::SessionQuery, session_iterator::SessionQueryIterator,
        },
        view::View,
    },
};

use super::{
    history_provider::HistoryProvider, rights_2_meta_data_edition::MetaDataEditionRights,
    rights_3_types_entities_creation::TypeAndEntityCreationRights,
};

/// Read-only rights marker
pub(crate) struct ReadOnlyRights();

/// Methods available with ReadOnly rights and higher
pub(crate) trait ReadOnly<R>: HistoryProvider {
    fn element_type(&self, name: ElementTypeName) -> ElementTypeQuery<R> {
        ElementTypeQuery::new::<R>(&self, Identifier::Name(name))
    }

    fn command_type(&self, name: CommandTypeName) -> CommandTypeQuery<R> {
        CommandTypeQuery::new::<R>(&self, Identifier::Name(name))
    }

    fn input_type(&self, name: InputTypeName) -> InputTypeQuery<R> {
        InputTypeQuery::new::<R>(&self, Identifier::Name(name))
    }

    fn element(&self, name: NameType) -> ElementQuery<R> {
        ElementQuery::new::<R>(&self, Identifier::Name(name))
    }

    fn session(&self, name: NameType) -> SessionQuery<R> {
        SessionQuery::new::<R>(&self, Identifier::Name(name))
    }

    fn element_types(&self) -> ElementTypeQueryIterator<R> {
        ElementTypeQueryIterator::new::<R>(&self, None)
    }

    fn command_types(&self) -> CommandTypeQueryIterator<R> {
        CommandTypeQueryIterator::new::<R>(&self, None)
    }

    fn input_types(&self) -> InputTypeQueryIterator<R> {
        InputTypeQueryIterator::new::<R>(&self, None)
    }

    fn elements(&self) -> ElementQueryIterator<R> {
        ElementTypeQueryIterator::new::<R>(&self, None)
    }

    fn sessions(&self) -> SessionQueryIterator<R> {
        SessionQueryIterator::new::<R>(&self, None)
    }

    fn query<T: IntoQuery>(&self, reference: T) -> T::Query {
        reference.query::<R>(&self)
    }
}

impl<D: Database> ReadOnly<ReadOnlyRights> for HistoryInterface<ReadOnlyRights, D> {}
impl<D: Database> ReadOnly<ReadOnlyRights> for HistoryInterface<MetaDataEditionRights, D> {}
impl<D: Database> ReadOnly<ReadOnlyRights> for HistoryInterface<TypeAndEntityCreationRights, D> {}

impl<D: Database> ReadOnly<ReadOnlyRights> for View<ReadOnlyRights, D> {}
//todo: propagate for View too? Or do the blancket traits work?
