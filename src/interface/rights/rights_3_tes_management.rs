use crate::{
    database::Database,
    history::{ids::Identifier, reference::state::StateReference},
    interface::{
        builder::{
            command_type_builder::CommandTypeBuilder, element_builder::ElementBuilder,
            element_type_builder::ElementTypeBuilder, input_type_builder::InputTypeBuilder,
        },
        history::HistoryInterface,
        query::{
            command_type::CommandTypeQuery, element::ElementQuery, element_type::ElementTypeQuery,
            input_type::InputTypeQuery,
        },
        shared::Shared,
        view::View,
    },
};

use super::{history_provider::HistoryProvider, rights_1_read_only::ReadOnly};

/// Type and Entity creation rights marker
pub(crate) struct TypeAndEntityCreationRights();

/// Methods available with Type and Entity Creation rights and higher
pub(crate) trait TypeAndEntityCreation<R>: HistoryProvider {
    fn new_element_type(
        &mut self,
        element_type: ElementTypeBuilder,
    ) -> Result<ElementTypeQuery<R>, ElementTypeBuilder> {
        let name = self
            .history()
            .new_element_type(element_type)
            .map_ok(|e_t| e_t.name())?;
        ElementTypeQuery::new::<R>(&self, Identifier::Name(name))
    }

    fn new_command_type(
        &mut self,
        command_type: CommandTypeBuilder,
    ) -> Result<CommandTypeQuery<R>, CommandTypeBuilder> {
        let name = self
            .history()
            .new_command_type(command_type)
            .map_ok(|c_t| c_t.name())?;
        CommandTypeQuery::new::<R>(&self, Identifier::Name(name))
    }

    fn new_input_type(
        &mut self,
        input_type: InputTypeBuilder,
    ) -> Result<InputTypeQuery<R>, InputTypeBuilder> {
        let name = self
            .history()
            .new_input_type(input_type)
            .map_ok(|i_t| i_t.name())?;
        InputTypeQuery::new::<R>(&self, Identifier::Name(name))
    }

    fn new_element(&mut self, element: ElementBuilder) -> Result<ElementQuery<R>, ElementBuilder> {
        let uid = self.history().new_element(element);
        ElementQuery::new::<R>(&self, Identifier::Id(uid))
    }

    fn new_session<D: Database>(&mut self, database: &Shared<D>) -> View<R, D> {
        let session = self.history().new_session(None);
        View::new(&self, database, session)
    }

    fn new_session_from<D: Database>(
        &mut self,
        database: &Shared<D>,
        initial_state: StateReference,
    ) -> View<R, D> {
        let session = self.history().new_session(initial_state);
        View::new(&self, database, session)
    }
}

impl<T: TypeAndEntityCreation> ReadOnly for T {}
impl<D: Database> TypeAndEntityCreation for HistoryInterface<TypeAndEntityCreationRights, D> {}
impl<D: Database> TypeAndEntityCreation for View<TypeAndEntityCreationRights, D> {}
