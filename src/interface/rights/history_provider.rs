use crate::{
    database::Database,
    history::History,
    interface::{history::HistoryInterface, mode::Mode, shared::Shared, view::View},
};

pub trait HistoryProvider {
    fn history(&self) -> Shared<History>;
    //fn history_mut(&mut self) -> &'a mut Shared<History>;
    fn mode(&self) -> Mode;
}

impl<R, D: Database> HistoryProvider for View<R, D> {
    fn history(&self) -> Shared<History> {
        &self.history
    }

    /*
    fn history_mut(&mut self) -> &'a mut Shared<History> {
        &mut self.history
    }*/

    fn mode(&self) -> Mode {
        self.mode
    }
}

impl<R, D: Database> HistoryProvider for HistoryInterface<R, D> {
    fn history(&self) -> Shared<History> {
        &self.history
    }

    /*
    fn history_mut(&mut self) -> &'a mut Shared<History> {
        &mut self.history
    }*/

    fn mode(&self) -> Mode {
        self.mode
    }
}
