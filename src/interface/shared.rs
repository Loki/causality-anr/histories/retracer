// todo
//#[cfg(not(feature = "sync"))]
//use std::{cell::RefCell, rc::Rc};
//#[cfg(not(feature = "sync"))]
//pub type Shared<T> = Rc<RefCell<T>>;
//#[cfg(feature = "sync")]
use std::sync::{Arc, RwLock};
//#[cfg(feature = "sync")]
pub type Shared<T> = Arc<RwLock<T>>;
pub type RcType<T> = Arc<T>;
pub type MutType<T> = RwLock<T>;

/*
pub struct SShared<T, RcT, MutT>(RcT<MutT<T>>);

impl<T> SShared<T, Rc, RefCell> {
    pub fn new(data: T) -> Self {
        SShared(Rc::new(RefCell::new(data)))
    }
}

impl<T> SShared<T, Arc, RwLock> {
    pub fn new(data: T) -> Self {
        SShared(Arc::new(RwLock::new(data)))
    }
}
*/
