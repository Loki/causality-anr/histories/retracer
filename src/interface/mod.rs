/*! History interface.

# History

## Setup

A history structure is created at the startup of the application or the project, and will track every change applied to the targetted data (we'll use the Counters example as an illustration).
```rust
let mut history = History<NoDB>::init(CommandFactory::instance());
history.register_new_element_types(vec![
    Counter::register(),
    Board::register(),
    Incrementor::register(),
]).unwrap();
history.register_new_command_types(vec![
    AddCounter::register(),
    RemoveCounter::register(),
    IncrementCounter::register(),
]).unwrap();
history.register_new_input_types(vec![
    Integer::register(),
]).unwrap();
```
*/

pub mod builder;
pub mod change;
pub mod history;
pub mod listeners;
pub mod mode;
pub mod query;
pub mod rights;
pub mod shared;
pub mod view;
