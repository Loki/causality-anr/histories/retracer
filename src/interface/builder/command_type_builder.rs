use std::collections::HashMap;

use crate::history::{
    command::command_type::CommandType,
    common::{annotation::Annotations, tag::Tags},
    ids::{ArgumentName, CommandTypeName, Identify, NameType, TypeName},
};

#[derive(Default)]
pub struct CommandTypeBuilder {
    pub arguments: HashMap<ArgumentName, TypeName>,
    pub inherits: Vec<TypeName>,
    pub name: CommandTypeName,
    pub tags: Tags,
    pub annotations: Annotations,
}

impl CommandTypeBuilder {
    pub fn new(name: CommandTypeName) -> Self {
        Self {
            name,
            ..Default::default()
        }
    }
    pub fn arguments(self, arguments: HashMap<ArgumentName, TypeName>) -> Self {
        self.arguments = arguments;
        self
    }
    pub fn inherits(self, inherits: Vec<TypeName>) -> Self {
        self.inherits = inherits;
        self
    }
    pub fn tags(self, tags: Tags) -> Self {
        self.tags = tags;
        self
    }
    pub fn annotations(self, annotations: Annotations) -> Self {
        self.annotations = annotations;
        self
    }
}

impl Into<CommandType> for CommandTypeBuilder {
    fn into(self) -> CommandType {
        let mut command_type = CommandType::new(self.name);
        *command_type.arguments_mut() = self.arguments;
        *command_type.inherits_mut() = self.inherits;
        *command_type.tags_mut() = self.tags;
        *command_type.annotations_mut() = self.annotations;
        command_type
    }
}

impl<IdxT> Identify<CommandTypeName> for CommandTypeBuilder {
    fn id(&self) -> Option<CommandTypeName> {
        Some(self.name)
    }

    fn name(&self) -> Option<NameType> {
        Some(self.name)
    }

    fn index(&self) -> Option<IdxT> {
        None
    }
}
