use crate::history::{
    common::{annotation::Annotations, tag::Tags},
    ids::{ElementTypeName, NameType},
};

#[derive(Default)]
pub struct ElementBuilder {
    pub from: Vec<ElementTypeName>,
    pub name: ElementTypeName,
    pub tags: Tags,
    pub annotations: Annotations,
}

impl ElementBuilder {
    pub fn new(name: NameType) -> Self {
        Self {
            name,
            ..Default::default()
        }
    }
    pub fn from(self, inherits: Vec<ElementTypeName>) -> Self {
        self.inherits = inherits;
        self
    }
    pub fn tags(self, tags: Tags) -> Self {
        self.tags = tags;
        self
    }
    pub fn annotations(self, annotations: Annotations) -> Self {
        self.annotations = annotations;
        self
    }
}
