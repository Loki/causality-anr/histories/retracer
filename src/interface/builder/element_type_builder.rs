use std::collections::HashMap;

use crate::history::{
    common::{annotation::Annotations, tag::Tags},
    element::element_type::ElementType,
    ids::{AttributeName, ElementTypeName, Identify, NameType, TypeName},
};

#[derive(Default, Debug, Clone)]
pub struct ElementTypeBuilder {
    pub attributes: HashMap<AttributeName, TypeName>,
    pub inherits: Vec<TypeName>,
    pub name: ElementTypeName,
    pub tags: Tags,
    pub annotations: Annotations,
}

impl ElementTypeBuilder {
    pub fn new(name: NameType) -> Self {
        Self {
            name,
            ..Default::default()
        }
    }
    pub fn attributes(self, attributes: HashMap<AttributeName, TypeName>) -> Self {
        self.attributes = attributes;
        self
    }
    pub fn inherits(self, inherits: Vec<TypeName>) -> Self {
        self.inherits = inherits;
        self
    }
    pub fn tags(self, tags: Tags) -> Self {
        self.tags = tags;
        self
    }
    pub fn annotations(self, annotations: Annotations) -> Self {
        self.annotations = annotations;
        self
    }

    pub(crate) fn build(self) -> ElementType {}
}

impl Into<ElementType> for ElementTypeBuilder {
    fn into(self) -> ElementType {
        let mut element_type = ElementType::new(self.name);
        *element_type.attributes_mut() = self.attributes;
        *element_type.inherits_mut() = self.inherits;
        *element_type.tags_mut() = self.tags;
        *element_type.annotations_mut() = self.annotations;
        element_type
    }
}

impl<IdxT> Identify<ElementTypeName> for ElementTypeBuilder {
    fn id(&self) -> Option<ElementTypeName> {
        Some(self.name)
    }

    fn name(&self) -> Option<NameType> {
        Some(self.name)
    }

    fn index(&self) -> Option<IdxT> {
        None
    }
}
