use crate::history::{
    common::{annotation::Annotations, tag::Tags},
    ids::{Identify, InputTypeName, TypeName},
    input::input_type::InputType,
};

#[derive(Default)]
pub struct InputTypeBuilder {
    pub name: InputTypeName,
    pub inherits: Vec<TypeName>,
    pub tags: Tags,
    pub annotations: Annotations,
}

impl InputTypeBuilder {
    pub fn new(name: InputTypeName) -> Self {
        Self {
            name,
            ..Default::default()
        }
    }
    pub fn inherits(self, inherits: Vec<TypeName>) -> Self {
        self.inherits = inherits;
        self
    }
    pub fn tags(self, tags: Tags) -> Self {
        self.tags = tags;
        self
    }
    pub fn annotations(self, annotations: Annotations) -> Self {
        self.annotations = annotations;
        self
    }
}

impl Into<InputType> for InputTypeBuilder {
    fn into(self) -> InputType {
        let mut input_type = InputType::new(self.name);
        *input_type.inherits_mut() = self.inherits;
        *input_type.tags_mut() = self.tags;
        *input_type.annotations_mut() = self.annotations;
        input_type
    }
}

impl<IdxT> Identify<InputTypeName> for InputTypeBuilder {
    fn id(&self) -> Option<InputTypeName> {
        Some(self.name)
    }

    fn name(&self) -> Option<crate::history::ids::NameType> {
        Some(self.name)
    }

    fn index(&self) -> Option<IdxT> {
        None
    }
}
