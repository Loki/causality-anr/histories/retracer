use crate::history::{
    common::{annotation::Annotations, tag::Tags},
    ids::NameType,
};

#[derive(Default)]
pub struct MetaDataBuilder {
    pub name: Option<NameType>,
    pub tags: Tags,
    pub annotations: Annotations,
}

impl MetaDataBuilder {
    pub fn new() -> Self {
        Self {
            ..Default::default()
        }
    }
    pub fn name(self, name: NameType) -> Self {
        self.name = Some(name);
        self
    }
    pub fn tags(self, tags: Tags) -> Self {
        self.tags = tags;
        self
    }
    pub fn annotations(self, annotations: Annotations) -> Self {
        self.annotations = annotations;
        self
    }
}
