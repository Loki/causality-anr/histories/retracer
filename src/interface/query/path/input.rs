pub struct InputPath {
    root: InputPathRoot,
    path: Vec<(InputPathOp, InputPathBlock)>,
}

pub(crate) enum InputPathRoot {
}

pub(crate) enum InputPathOp {
}

pub(crate) enum InputPathBlock {
}




