use crate::history::ids::CommandTypeName;

pub struct ChangePath {
    root: ChangePathRoot,
    path: Vec<(ChangePathOp, ChangePathBlock)>,
}

pub(crate) enum ChangePathRoot {
    Change,
    SubChange,
    Path(Box<ChangePath>),
}

pub(crate) enum ChangePathOp {
    /// "/" token
    Down,
    /// "//" token
    FindBelow,
    /// ".." token
    Up,
    /// "^" token
    FindAbove,
    /// "." token
    Here,
    /// ">" token
    Next,
    /// ">>" token
    FindAfter,
    /// "<" token
    Previous,
    /// "<<" token
    FindBefore,
}

pub(crate) enum ChangePathBlock {
    /// "num" token
    Index(usize),
    /// "#alphanum" token
    CommandType(CommandTypeName),
}
