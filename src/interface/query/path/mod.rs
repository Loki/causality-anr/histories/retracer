pub mod state;
pub mod change;
pub mod input;

use parse_display::{Display, FromStr};

use crate::{interface::{shared::Shared, mode::Mode}, history::{History, reference::Reference, ids::PathIdx}, Path};

use self::state::StatePathBlock;


#[derive(Clone, Debug)]
pub struct PathQuery<R> {
    history: Shared<History>,
    idx: Reference,
    mode: Mode,
    path: Path,
    path_idx: PathIdx,
}

#[derive(Display, FromStr, PartialEq, Debug)]
pub(crate) enum PathBlock {
    /// "%St" token
   #[display("%St")]
    MoveToEndState,
    /// "%St" token
   #[display("%St({0}, {1})")]
    MoveToState(usize, usize),
    /// "%I" token
   #[display("%I({0})")]
    MoveToInput(usize),
    /// "%Ch" token
   #[display("%Ch")]
    MoveToChange,
   #[display("{0}")]
    StatePathBlock(StatePathBlock),
   #[display("{0}")]
    ChangePathBlock(ChangePathBlock),
   #[display("{0}")]
    InputPathBlock(InputPathBlock),


}
