use parse_display::{Display, FromStr};

use crate::{history::{ids::{AttributeName, ElementUID, ElementTypeName, PathIdx, CommandTypeName, ChangeUID, SessionUID}, History, reference::state::StateReference, path::StatePath, common::{tag::Tags, annotation::Annotations}}, interface::{shared::Shared, mode::Mode}};


#[derive(Clone, Debug)]
pub struct StatePathQuery<R> {
    history: Shared<History>,
    idx: StateReference,
    mode: Mode,
    path: StatePath,
    path_idx: PathIdx,
}

#[derive(Display, FromStr, PartialEq, Debug)]
pub(crate) enum StatePathBlock {
    /// "/" token
   #[display("/{0}")]
    Down(AtEETMD),
    /// "//" token
   #[display("//{0}")]
    FindBelow(AtEETMD),
    /// "^" token
   #[display("^")]
    Up,
    /// "^^" token
   #[display("^^{0}")]
    FindAbove(EETMD),
    /// "." token
   #[display(".{0}")]
    Here(MD),
    /// ">" token
   #[display(">{0}")]
    Next(ChSCTMD),
    /// ">>" token
   #[display(">>{0}")]
    FindAfter(StatePathBlock),
    /// "<" token
   #[display("<")]
    Previous,
    /// "<<" token
   #[display("<<{0}")]
    FindBefore(ChSCTMD),
    /// "*" token
   #[display("*{0}")]
    Repeat(u8),
    /// "|" token
   #[display("\\|{0}")]
    Filter(StateFilter),
}

#[derive(Display, FromStr, PartialEq, Debug)]
#[display("{0}")]
pub(crate) enum AtEETMD {
    /// "alphanum" token
   #[from_str(regex = "(?<0>[\\w]+)")]
    Attribute(AttributeName),
    EETMD(EETMD),
}

#[derive(Display, FromStr, PartialEq, Debug)]
pub(crate) enum EETMD {
    /// "&alphanum" token
   #[display("&{0}")]
    Element(ElementUID),
    /// "#alphanum" token
   #[display("#{0}")]
    ElementType(ElementTypeName),
    /// ".…" token
   #[display(".{0}")]
    MetaData(MD),
}


#[derive(Display, FromStr, PartialEq, Debug)]
pub(crate) enum ChSCTMD {
   #[display("{0}")]
    Change(ChangeUID),
   #[display("&{0}")]
    Session(SessionUID),
   #[display("#{0}")]
    CommandType(CommandTypeName),
   #[display(".{0}")]
    MetaData(MD),
}

#[derive(Display, FromStr, PartialEq, Debug)]
pub(crate) enum MD {
    /// ".name(…)" token
   #[display("{0}")]
   #[from_str(regex = "name\\((?<0>[\\w]+)\\)")]
    Name(String),
    /// ".tags(…)" token
   #[display("tags({0})")]
    Tags(Tags),
    /// ".annotations(…)" token
   #[display("annotations({0})")]
    Annotations(Annotations),

}

#[derive(Display, FromStr, PartialEq, Debug)]
pub(crate) enum StateFilter {
   #[display("{0}")]
    EETMD(EETMD),
}
