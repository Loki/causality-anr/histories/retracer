use crate::history::reference::change::ChangeReference;
use crate::history::{ids::CTID, History};

use crate::interface::Shared;

#[derive(Clone, Debug)]
pub struct ChangeRecordQuery<M> {
    history: Shared<History>,
    id: ChangeReference,
    mode: Mode,
    _phantom: PhantomData<M>,
}
