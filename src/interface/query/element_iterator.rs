use super::element::ElementQuery;
use crate::{database::Database, interface::register::Register};

/** Iterates over the [Hystory]'s [Element]s.
*/
#[derive(Clone, Debug)]
pub struct ElementQueryIterator<D: Database> {
    register: Register<D>,
    idx: usize,
}

impl<D: Database> ElementQueryIterator<D> {
    pub(crate) fn new(register: &Register<D>) -> Self {
        Self {
            register: register.clone(),
            idx: 0,
        }
    }
}

impl<D: Database> Iterator for ElementQueryIterator<D> {
    type Item = ElementQuery<D>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.register.element_count() > self.idx {
            let query = ElementQuery::new(&self.register, self.idx);
            self.idx += 1;
            Some(query)
        } else {
            None
        }
    }
}
