use crate::database::Database;

use super::{
    command_type::CommandTypeQuery, element_type::ElementTypeQuery, input_type::InputTypeQuery,
};

#[derive(Clone, Debug)]
pub enum TypeQuery<D: Database> {
    Element(ElementTypeQuery<D>),
    Command(CommandTypeQuery<D>),
    Input(InputTypeQuery<D>),
}

#[derive(Clone, Debug)]
pub struct TypeQueryMut<D: Database>(TypeQuery<D>);
