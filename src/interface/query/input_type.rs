use std::marker::PhantomData;

use crate::{
    history::{
        common::{meta_data::MetaDataMap, tag::Tag},
        ids::InputTypeName,
        History,
    },
    interface::{Mode, Shared},
};

use super::ytype::TypeQuery;

#[derive(Clone, Debug)]
pub struct InputTypeQuery<M> {
    history: Shared<History>,
    name: InputTypeName,
    idx: usize,
    mode: Mode,
    rights: PhantomData<M>,
}

impl<M> InputTypeQuery<M> {
    pub(crate) fn new(
        history: Shared<History>,
        idx: usize,
        name: InputTypeName,
        mode: Mode,
    ) -> Self {
        Self {
            history,
            idx,
            name,
            mode,
            rights: PhantomData,
        }
    }
}

impl<M> InputTypeQuery<M> {
    pub fn inherits(&self) -> Vec<TypeQuery<M>> {
        self.history
            .borrow()
            .input_type(self.idx)
            .cp()
            .inherits()
            .into()
    }

    pub fn name(&self) -> &InputTypeName {
        &self.name
    }
    pub fn tags(&self) -> &Vec<Tag> {
        self.history.borrow().input_type(self.idx).cp().tags()
    }
    pub fn meta_data(&self) -> &MetaDataMap {
        self.history.borrow().input_type(self.idx).cp().meta_data()
    }
    pub fn input_iter(&self) -> InputQueryIterator<M> {
        InputQueryIterator::new(IQI::IT(self.idx), self.history, self.mode)
    }
}

// Context changes //
impl<M> InputTypeQuery<M> {
    pub fn tags_mut(&self) -> &mut Vec<Tag> {
        self.history
            .borrow()
            .input_type(self.idx)
            .cp_mut()
            .tags_mut()
    }
    pub fn meta_data_mut(&self) -> &mut MetaDataMap {
        self.history
            .borrow()
            .input_type(self.idx)
            .cp_mut()
            .meta_data_mut()
    }
}

impl<M> Into<InputTypeName> for InputTypeQuery<M> {
    fn into(self) -> InputTypeName {
        self.name()
    }
}
