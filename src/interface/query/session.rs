use std::marker::PhantomData;

use crate::{
    history::{
        command::execution::ExecutionState,
        common::{meta_data::MetaDataMap, tag::Tag},
        ids::{ElementID, SessionID},
        History,
    },
    interface::{Mode, Shared},
};

#[derive(Clone, Debug)]
pub struct SessionQuery<M> {
    history: Shared<History>,
    name: SessionID,
    idx: usize,
    mode: Mode,
    rights: PhantomData<M>,
}

impl<M> SessionQuery<M> {
    pub(crate) fn new(history: Shared<History>, idx: usize, name: ElementID, mode: Mode) -> Self {
        Self {
            history,
            idx,
            name,
            mode,
            rights: PhantomData,
        }
    }
}

impl<M> SessionQuery<M> {
    pub fn name(&self) -> &SessionID {
        &self.name
    }
    pub fn tags(&self) -> &Vec<Tag> {
        self.history.borrow().session(self.idx).cp().tags()
    }
    pub fn meta_data(&self) -> &MetaDataMap {
        self.history.borrow().session(self.idx).cp().meta_data()
    }
    pub fn change_iter(&self) -> ChangeQueryIterator<M> {
        ChangeQueryIterator::new(ChQIter::S(self.idx), self.history, self.mode)
    }
}

// Context changes //
impl<M> SessionQuery<M> {
    pub fn tags_mut(&self) -> &mut Vec<Tag> {
        self.history.borrow().session(self.idx).cp_mut().tags_mut()
    }
    pub fn meta_data_mut(&self) -> &mut MetaDataMap {
        self.history
            .borrow()
            .session(self.idx)
            .cp_mut()
            .meta_data_mut()
    }
}

impl<M> Into<SessionID> for SessionQuery<M> {
    fn into(self) -> SessionID {
        self.name()
    }
}
