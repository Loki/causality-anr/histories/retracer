use super::command_type::CommandTypeQuery;
use crate::{database::Database, interface::register::Register};

/** Iterates over the [Hystory]'s [CommandType]s.
*/
#[derive(Clone, Debug)]
pub struct CommandTypeQueryIterator<D: Database> {
    register: Register<D>,
    idx: usize,
}

impl<D: Database> CommandTypeQueryIterator<D> {
    pub(crate) fn new(register: &Register<D>) -> Self {
        Self {
            register: register.clone(),
            idx: 0,
        }
    }
}

impl<D: Database> Iterator for CommandTypeQueryIterator<D> {
    type Item = CommandTypeQuery<D>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.register.command_type_count() > self.idx {
            let query = CommandTypeQuery::new(&self.register, self.idx);
            self.idx += 1;
            Some(query)
        } else {
            None
        }
    }
}
