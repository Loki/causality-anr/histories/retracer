use super::session::SessionQuery;
use crate::{database::Database, interface::register::Register};

/** Iterates over the [Hystory]'s [Element]s.
*/
#[derive(Clone, Debug)]
pub struct SessionQueryIterator<D: Database> {
    register: Register<D>,
    idx: usize,
}

impl<D: Database> SessionQueryIterator<D> {
    pub(crate) fn new(register: &Register<D>) -> Self {
        Self {
            register: register.clone(),
            idx: 0,
        }
    }
}

impl<D: Database> Iterator for SessionQueryIterator<D> {
    type Item = SessionQuery<D>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.register.session_count() > self.idx {
            let query = SessionQuery::new(&self.register, self.idx);
            self.idx += 1;
            Some(query)
        } else {
            None
        }
    }
}
