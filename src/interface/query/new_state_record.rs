use crate::{interface::{mode::Mode, rights::rights_4_execution_recording::ExecutionRecordingRights}, history::{ids::AttributeName, reference::state::StateReference}};


#[derive(Clone, Debug)]
pub struct NewStateRecordQuery<R> {
    //change_track: (usize, usize),
    history: Shared<History>,
    idx: StateReference,
    mode: Mode,
}





impl NewStateRecordQuery<ExecutionRecordingRights> {
    /// Adds a child to the Element.
    pub fn insert(&mut self, child_name: AttributeName, child: StateReference) -> Result<NewStateRecordQuery<R>, ()> {
        if let Mode::RecordExecution(change) = self.mode {
            change.write().unwrap().insert(self.idx, child_name, child);
            Ok(())
        }
        else {
            Err(())
        }
    }

    /// Removes a child from the Element.
    pub fn remove(&mut self, child_name: AttributeName) -> Result<(), ()> {
        if let Mode::RecordExecution(change) = self.mode {
            change.write().unwrap().remove(self.idx, child_name);
            Ok(())
        }
        else {
            Err(())
        }
    }

    /// State restoration
    pub fn restore(&self, state: State<D>) {
        todo!()
    }

}
