use std::str::Chars;

use itertools::Itertools;

use crate::history::ids::{AttributeName, ElementUID};
use crate::history::record::state::StateRecord;
use crate::history::reference::state::StateReference;
use crate::history::History;
use crate::interface::mode::Mode;
use crate::interface::rights::history_provider::HistoryProvider;
use crate::interface::rights::rights_4_execution_recording::ExecutionRecordingRights;
use crate::interface::shared::{RcType, Shared};
use crate::Path;

use super::new_state_record::NewStateRecordQuery;
use super::path::PathQuery;
use super::track::Track;
use super::Query;

#[derive(Clone, Debug)]
pub struct StateRecordQuery<R> {
    history: Shared<History>,
    idx: StateReference,
    mode: Mode,
}

impl<R> StateRecordQuery<R> {
    pub(crate) fn new(history: &dyn HistoryProvider, state: StateReference) -> StateRecordQuery<R> {
        StateRecordQuery {
            history: RcType::clone(&history),
            idx: state,
        }
    }

    pub fn add_track(state: &dyn Into<StateRecord>) -> Track<R> {}

    pub(crate) fn follow<'a>(self, path: &'a str) -> Result<PathQuery<'a, R>, Self> {
        let mut chars = path.chars();
        let token = chars.next().unwrap();
        let query = match token {
            '/' => {
                let 
                if chars.peekable().peek() == '/' {
                    chars.next(); // skip '/'
                    // search
                    self.(chars.as_str().parse_block())
                } else {
                    // child
                    self.(chars.as_str().parse_attribute())
                }
                StateRecordQuery::new(self, state);

                Query::StateRecord()
            }
            //"//" => Query::StateRecord(),
            '.' => {
                if chars.peekable().peek() == '.' {
                    // get parent State
                    chars.next();
                    Query::StateRecord(StateRecordQuery::new(
                        self.idx.parent_state(&*self.history.read().unwrap()),
                        &*self.history.read().unwrap(),
                    ))
                } else {
                    // get meta data
                    match chars.as_str().parse_meta_data() {
                        "name" => 
                    }
                }
            }
            //".." => Query::StateRecord(),
            '<' => Query::StateRecord(),
            '>' => Query::StateRecord(),
            '&' => Query::Element(),
            '#' => Query::ElementType(),
            '^' => Query::StateRecord(),
            _ => return Err(self),
        };
        PathQuery::new(query, path)
    }
}

impl StateRecordQuery<ExecutionRecordingRights> {
    /// Creates a detached view based off of this State
    pub fn detach(&self) -> View<ExecutionRecordingRights> {
        //todo: force lifetimes
        todo!()
    }

    pub fn change(&self) -> Result<NewStateRecordQuery<R>, ()> {
        todo!()
    }

    /// Replace this state
    pub fn replace_with(&self) -> Result<NewStateRecordQuery<R>, ()> {
        todo!()
    }

}

impl<R> Into<StateRecord> for StateRecordQuery<R> {
    fn into(self) -> StateRecord {
        StateRecord::new(self.idx)
    }
}
//fn register(view: View<>)
