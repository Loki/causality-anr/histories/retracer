use super::input_type::InputTypeQuery;
use crate::{database::Database, interface::register::Register};

/** Iterates over the [Hystory]'s [InputType]s.
*/
#[derive(Clone, Debug)]
pub struct InputTypeQueryIterator<D: Database> {
    register: Register<D>,
    /// if None, iterate over all of the history's InputTypes, or else iterate over the given selection only
    selection: Option<Vec<usize>>,
    idx: usize,
}

impl<D: Database> InputTypeQueryIterator<D> {
    pub(crate) fn new(register: &Register<D>) -> Self {
        Self {
            register: register.clone(),
            idx: 0,
        }
    }
}

impl<D: Database> Iterator for InputTypeQueryIterator<D> {
    type Item = InputTypeQuery<D>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.register.input_type_count() > self.idx {
            let query = InputTypeQuery::new(&self.register, self.idx);
            self.idx += 1;
            Some(query)
        } else {
            None
        }
    }
}
