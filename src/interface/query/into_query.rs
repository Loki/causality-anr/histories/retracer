use crate::{
    database::Database,
    history::reference::{change::ChangeReference, input::InputReference, state::StateReference},
    interface::{rights::history_provider::HistoryProvider, view::View}, Path,
};

use super::{
    change_record::ChangeRecordQuery, input_record::InputRecordQuery,
    state_record::StateRecordQuery,
};

pub(crate) trait IntoQuery<R> {
    type Query;
    /// Return a Query corresponding to the given Reference or Path.
    /// The Path will be interpreted by taking into account the sub_state if any.
    fn into_query<D: Database>(&self, view: &View<R, D>) -> Self::Query;
}

impl<R> IntoQuery<R> for ChangeReference {
    type Query = ChangeRecordQuery<R>;

    fn into_query<D: Database>(&self, view: &View<R, D>) -> Self::Query {
        let mut r = self.clone();
        r.compute_idx(&*view.history().read().unwrap());
        ChangeRecordQuery::new::<R>(&view, r.idx.unwrap())
    }
}

impl<R> IntoQuery<R> for StateReference {
    type Query = StateRecordQuery<R>;

    fn into_query<D: Database>(&self, view: &View<R, D>) -> Self::Query {
        let mut r = self.clone();
        r.compute_idx(&*view.history().read().unwrap());
        StateRecordQuery::new::<R>(&view, r.idx.unwrap())
    }
}

impl<R> IntoQuery<R> for InputReference {
    type Query = InputRecordQuery<R>;

    fn into_query<D: Database>(&self, view: &View<R, D>) -> Self::Query {
        let mut r = self.clone();
        r.compute_idx(&*view.history().read().unwrap());
        InputRecordQuery::new::<R>(&view, r.idx.unwrap())
    }
}

impl<R> IntoQuery<R> for Path {
    type Query = Query<R>;

    fn into_query<D: Database>(&self, view: &View<R, D>) -> Self::Query {
        todo!()
    }
}
