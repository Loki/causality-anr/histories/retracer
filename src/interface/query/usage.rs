use crate::{
    database::Database, history::common::usage::Usage as HistoryUsage,
    interface::state::query::register::Register,
};

use super::super::{
    command::command_version::CommandVersion, entity::entity_version::EntityVersion,
};

#[derive(Clone, Debug)]
pub struct UsageQuery<R> {
    user: CommandVersion<D, Reg>,
    role: VersionRole<D>,
}

impl<R> UsageQuery<R> {
    pub fn user(&self) -> &CommandVersion<D> {
        &self.user
    }
    pub fn role(&self) -> &VersionRole<D> {
        &self.role
    }
    pub fn to_user(self) -> CommandVersion<D> {
        self.user
    }
    pub fn next(self) -> Option<EntityVersion<D>> {
        match self.role {
            VersionRole::Target(correspondent) => correspondent,
            VersionRole::Result(correspondent) => correspondent,
            _ => None,
        }
    }
}

impl<D: Database + Default> From<&HistoryUsage> for Usage<D> {
    fn from(_: &HistoryUsage) -> Self {
        todo!()
    }
}

#[derive(Clone, Debug)]
pub enum VersionRole<D: Database + Default> {
    Target(Option<EntityVersion<D>>),
    Result(Option<EntityVersion<D>>),
    Input,
    Parameter,
}
