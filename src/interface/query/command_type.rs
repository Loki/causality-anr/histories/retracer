//! Command type description
//!
//! The commands Retracer records are abstracted away, leaving a generic [Command] trait
//! as an interface for manipulation. For Retracer to be able to manipulate these commands
//! and make some safety and coherence checks (what is the command doing?), it needs some
//! information. [CommandType]s give a small description of these commands' type.
//! Name, arguments and their types, and other types it can replace (a light inheritance system)
//! are the main characteristics that can be given to Retracer for each command type.
//! It is a light type system in a way, which can be bypassed if wanted, as long as a type name
//! is given, for Retracer to be able to create new instances of the command itself, which is
//! needed for [replays](history-replay) for instance.

use std::collections::HashMap;
use std::marker::PhantomData;

use crate::history::common::meta_data::MetaDataMap;
use crate::history::common::tag::Tag;
use crate::history::ids::CommandTypeName;
use crate::history::History;
use crate::interface::{Mode, Shared};

use super::ytype::TypeQuery;

#[derive(Clone, Debug)]
pub struct CommandTypeQuery<M> {
    history: Shared<History>,
    name: CommandTypeName,
    idx: usize,
    mode: Mode,
    rights: PhantomData<M>,
}

impl<M> CommandTypeQuery<M> {
    pub(crate) fn new(
        history: Shared<History>,
        idx: usize,
        name: CommandTypeName,
        mode: &dyn Into<M>,
    ) -> Self {
        Self {
            history,
            idx,
            name,
            mode,
            rights: PhantomData,
        }
    }
}

impl<M> CommandTypeQuery<M> {
    /// Returns a mapping of all argument entries to their respective type
    pub fn arguments(&self) -> HashMap<&String, TypeQuery<M>> {
        self.history
            .borrow()
            .command_type(self.idx)
            .arguments()
            .into()
    }

    /// All the types this one inherits from
    ///
    /// This tells retracer that this type can substitute the inherited ones
    pub fn inherits(&self) -> Vec<TypeQuery<M>> {
        self.history
            .borrow()
            .command_type(self.idx)
            .cp()
            .inherits()
            .into()
    }

    pub fn name(&self) -> &CommandTypeName {
        &self.name
    }
    pub fn tags(&self) -> &Vec<Tag> {
        self.history.borrow().command_type(self.idx).cp().tags()
    }
    pub fn meta_data(&self) -> &MetaDataMap {
        self.history
            .borrow()
            .command_type(self.idx)
            .cp()
            .meta_data()
    }
    pub fn command_iter(&self) -> ChangeQueryIterator<M> {
        ChangeQueryIterator::new(CQI::CT(self.idx), self.history, self.mode)
    }
}

// Context changes //
impl<M> CommandTypeQuery<M> {
    pub fn tags_mut(&self) -> &mut Vec<Tag> {
        self.history
            .borrow()
            .command_type(self.idx)
            .cp_mut()
            .tags_mut()
    }
    pub fn meta_data_mut(&self) -> &mut MetaDataMap {
        self.history
            .borrow()
            .command_type(self.idx)
            .cp_mut()
            .meta_data_mut()
    }
}

impl<M> Into<CommandTypeName> for CommandTypeQuery<M> {
    fn into(self) -> CommandTypeName {
        self.name()
    }
}
