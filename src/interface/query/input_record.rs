use crate::history::{ids::ITID, History};

use crate::interface::Shared;

#[derive(Clone, Debug)]
pub struct InputRecordQuery<R> {
    history: Shared<History>,
    idx: ITID,
}
