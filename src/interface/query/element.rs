use std::marker::PhantomData;

use crate::{
    database::Database,
    history::{
        command::execution::ExecutionState,
        common::{meta_data::MetaDataMap, tag::Tag},
        ids::{ElementID, ElementTypeName},
        History,
    },
    interface::{history::HistoryInterface, Mode, Shared, rights::rights_4_execution_recording::ExecutionRecordingRights},
};

use super::{element_type::ElementTypeQuery, new_state_record::NewStateRecordQuery};

#[derive(Clone, Debug)]
pub struct ElementQuery<M> {
    history: Shared<History>,
    name: ElementID,
    idx: usize,
    mode: Mode,
    rights: PhantomData<M>,
}

impl<M> ElementQuery<M> {
    pub(crate) fn new(history: Shared<History>, idx: usize, name: ElementID, mode: Mode) -> Self {
        Self {
            history,
            idx,
            name,
            mode,
            rights: PhantomData,
        }
    }
}

impl<M> ElementQuery<M> {
    pub fn element_type(&self) -> ElementTypeQuery<M> {
        ElementTypeQuery::new(
            &self.history,
            self.history.borrow().element(self.idx).element_type(),
        )
    }
    pub fn name(&self) -> &ElementID {
        &self.name
    }
    pub fn tags(&self) -> &Vec<Tag> {
        self.history.borrow().element(self.idx).cp().tags()
    }
    pub fn meta_data(&self) -> &MetaDataMap {
        self.history.borrow().element(self.idx).cp().meta_data()
    }
    pub fn state_iter(&self) -> StateQueryIterator<M> {
        StateQueryIterator::new(SQI::ET(self.idx), self.history, self.mode)
    }
}

// Context changes //
impl<M> ElementQuery<M> {
    pub fn tags_mut(&self) -> &mut Vec<Tag> {
        self.history.borrow().element(self.idx).cp_mut().tags_mut()
    }
    pub fn meta_data_mut(&self) -> &mut MetaDataMap {
        self.history
            .borrow()
            .element(self.idx)
            .cp_mut()
            .meta_data_mut()
    }
}

impl ElementQuery<ExecutionRecordingRights> {
    pub fn instantiate(
        &self,
        parent: ElementQuery<D>,
        //argument_name: Option<ArgumentName>, todo: move to EV wrapper
        //parent_argument_name: Option<ArgumentName>,
    ) -> Result<ElementVersionQuery<D>, ChangeError> {
        todo!()
    }
    pub fn new_state(&self, name: &str) -> NewStateRecordQuery<ExecutionRecordingRights> {
        NewStateRecordQuery::new(&self.history, self.history.borrow().new_element(name))
    }

}

impl<M> Into<ElementID> for ElementQuery<M> {
    fn into(self) -> ElementID {
        self.name()
    }
}

pub enum ElementQueryIterator<M> {
    Type {
        history: Shared<History>,
        type_name: ElementTypeName,
        type_idx: usize,
        iter_idx: usize,
    },
    Vec {
        history: Shared<History>,
        vector: Vec<ElementID>,
        iter_idx: usize,
    },
}

impl<M> ElementQueryIterator<M> {
    //pub fn from_type(value: ElementTypeName, history: Into<HistoryInterface<D>) -> Self {
    //ElementQueryIterator::Type { history: history.into(), type_name: value, type_idx: value.idx(), iter_idx: 0 }
    //}

    pub(crate) fn from_vec(value: Vec<ElementID>, history: Shared<History>) -> Self {
        ElementQueryIterator::Vec {
            history,
            vector: value,
            iter_idx: 0,
        }
    }
}

impl<M> From<ElementTypeQuery<M>> for ElementQueryIterator<M> {
    fn from(value: ElementTypeQuery<M>) -> Self {
        ElementQueryIterator::Type {
            history: value.history_cloned(),
            type_name: value.name(),
            type_idx: value.idx(),
            iter_idx: 0,
        }
    }
}
