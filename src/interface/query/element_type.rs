/*! ElementType Query
    The [ElementType]'s fields are accessible, as well as the list of [Element]s.
*/

use std::{marker::PhantomData, rc::Rc};

use crate::{
    history::{
        common::{meta_data::MetaDataMap, tag::Tag},
        ids::{AttributeName, ElementTypeName, IDType, Identifier},
        History, element::element_type::ElementType,
    },
    interface::{rights::history_provider::HistoryProvider, Mode, Shared},
};

use super::{element::ElementQuery, element_iterator::ElementQueryIterator, ytype::TypeQuery};

#[derive(Clone, Debug)]
pub struct ElementTypeQuery<M> {
    history: Shared<History>,
    name: ElementTypeName,
    idx: usize,
    mode: Mode,
    rights: PhantomData<M>,
}

impl<M> ElementTypeQuery<M> {
    pub(crate) fn new(provider: &dyn HistoryProvider, id: Identifier<ElementTypeName>) -> Self {
        todo!();
        Self {
            history: provider.history(),
            idx,
            name,
            mode: provider.mode(),
            rights: PhantomData,
        }
    }
    pub(crate) fn idx() -> usize {
        self.idx
    }
    pub(crate) fn history_cloned(&self) -> Shared<History> {
        Rc::clone(&self.history)
    }
}

impl<M> ElementTypeQuery<M> {
    pub fn attributes(&self) -> &Vec<AttributeName> {
        self.history.borrow().element_type(self.idx).attributes()
    }

    pub fn inherits(&self) -> Vec<TypeQuery<M>> {
        self.history
            .borrow()
            .element_type(self.idx)
            .cp()
            .inherits()
            .into()
    }

    pub fn name(&self) -> &ElementTypeName {
        &self.name
    }
    pub fn tags(&self) -> &Vec<Tag> {
        self.history.borrow().element_type(self.idx).cp().tags()
    }
    pub fn meta_data(&self) -> &MetaDataMap {
        self.history
            .borrow()
            .element_type(self.idx)
            .cp()
            .meta_data()
    }
}

// Context changes //
impl<M> ElementTypeQuery<M> {
    pub fn tags_mut(&self) -> &mut Vec<Tag> {
        self.history
            .borrow()
            .element_type(self.idx)
            .cp_mut()
            .tags_mut()
    }
    pub fn meta_data_mut(&self) -> &mut MetaDataMap {
        self.history
            .borrow()
            .element_type(self.idx)
            .cp_mut()
            .meta_data_mut()
    }
    pub fn new_element(&self, name: &str) -> ElementQuery<M> {
        ElementQuery::new(
            self.history_cloned(),
            self.mode,
            self.history.borrow().new_element(name),
        )
    }
}

impl<M> Into<ElementTypeName> for ElementTypeQuery<M> {
    fn into(self) -> ElementTypeName {
        self.name()
    }
}

impl<M> Into<ElementType> for ElementTypeQuery<M> {
    fn into(self) -> ElementType {
        todo!()
    }
}

impl<M> IntoIterator for ElementTypeQuery<M> {
    type Item = ElementQuery<M>;

    type IntoIter = ElementQueryIterator<M>;

    fn into_iter(self) -> Self::IntoIter {
        self.into()
    }
}
