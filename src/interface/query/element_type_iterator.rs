use std::{marker::PhantomData, rc::Rc};

use super::element_type::{ElementTypeQuery, ElementTypeQueryMut};
use crate::{
    database::Database,
    history::{
        ids::{ElementID, ElementUID},
        History,
    },
    interface::{register::Register, Mode, Shared},
};

/** Iterates over the [Hystory]'s [ElementType]s.
*/
#[derive(Clone, Debug)]
pub struct ElementTypeQueryIterator<M> {
    history: Shared<History>,
    name: ElementUID,
    idx: usize,
    /// if None, iterate over all of the history's InputTypes, or else iterate over the given selection only
    selection: Option<Vec<usize>>,
    mode: Mode,
    __phantom: PhantomData<M>,
}

impl<M> ElementTypeQueryIterator<M> {
    pub(crate) fn new(history: Shared<History>, mode: M) -> Self {
        Self {
            history,
            name: history.element_type(0).cp().name().clone(), // todo: mark IDs clone
            idx: 0,
            mode: mode.into(),
            __phantom: PhantomData,
        }
    }
}

impl<M> Iterator for ElementTypeQueryIterator<M> {
    type Item = ElementTypeQuery<M>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.register.element_type_count() > self.idx {
            let query =
                ElementTypeQuery::new(Rc::clone(&self.history), self.idx, self.name, self.mode);
            self.idx += 1;
            Some(query)
        } else {
            None
        }
    }
}

/** Iterates over the [Hystory]'s [ElementType]s mutably.
*/
#[derive(Clone, Debug)]
pub struct ElementTypeQueryIteratorMut<D: Database> {
    register: Register<D>,
    idx: usize,
}

impl<D: Database> ElementTypeQueryIteratorMut<D> {
    pub(crate) fn new(register: &Register<D>) -> Self {
        Self {
            register: register.clone(),
            idx: 0,
        }
    }
}

impl<D: Database> Iterator for ElementTypeQueryIteratorMut<D> {
    type Item = ElementTypeQuery<D>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.register.element_type_count() > self.idx {
            let query = ElementTypeQueryMut::new(&self.register, self.idx);
            self.idx += 1;
            Some(query)
        } else {
            None
        }
    }
}
