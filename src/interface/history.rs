use std::marker::PhantomData;

use crate::{database::Database, history::History};

use super::{
    mode::Mode,
    shared::{RcType, Shared},
};

pub struct HistoryInterface<R, D: Database> {
    history: Shared<History>,
    database: Shared<D>,
    mode: Mode,
    _phantom: PhantomData<R>,
    //factory: Rc<dyn CommandFactory<D>>,
}

impl<R, D: Database> HistoryInterface<R, D> {
    /*
    fn new(
        name: Into<String>,
        init: fn(&mut HistoryInterfaceInit<D>),
        database: Shared<D>,
    ) -> HistoryInterface<D> {
        let hist = Self {
            history: History::new(name),
            database,
        };
        // initialize the history
        init(&mut hist.into());
        hist
    }*/

    pub(crate) fn new_with(history: Shared<History>, database: Shared<D>, mode: Mode) -> Self {
        Self {
            history,
            database,
            _phantom: PhantomData,
            mode,
        }
    }
    pub(crate) fn history_cloned(&self) -> Shared<History> {
        RcType::clone(&self.history)
    }
}

impl<R, D: Database> Into<Shared<History>> for HistoryInterface<R, D> {
    fn into(self) -> Shared<History> {
        self.history
    }
}

/*
#[cfg(test)]
mod init_tests {
    use crate::interface::builder::element_builder::ElementBuilder;
    use crate::interface::builder::element_type_builder::ElementTypeBuilder;
    use crate::interface::history::History;
    use crate::modules::database::hashmapdb::HashMapDB;

    fn init_hist(history: History<HashMapDB<usize>>) -> Result<_, _> {
        history.register_element_type(ElementTypeBuilder::new("Integer"));
        history.register_element(ElementBuilder::new("Counter"));
        let state = history.element("Counter").new_state("Init");
        state.set_data(0);
        history.register(state);
        Ok(())
    }

    #[test]
    fn init() {
        let history = History::new("CounterTest", init_hist);
    }
}
*/
