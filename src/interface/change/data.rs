use std::collections::HashMap;

use crate::{database::Database, history::{record::state::StateRecord, reference::input::InputReference}};

pub(crate) struct ChangeData<D: Database> {
    states: HashMap<StateRecord, D::RecordData>,
    // The InputReferences are new ones
    inputs: HashMap<InputReference, D::RecordData>,
}
