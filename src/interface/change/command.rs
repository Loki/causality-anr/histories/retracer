use crate::{
    daily::Try,
    database::Database,
    history::{
        command::arguments::Arguments,
        common::{annotation::Annotations, tag::Tags},
        ids::{ArgumentName, CommandTypeName, NameType},
    },
    interface::view::View,
};

use super::input::Input;

pub trait Command<D: Database> {
    type ReturnType: Try;
    fn command_type(&self) -> CommandTypeName;
    fn set_arguments(&mut self, data: Arguments<dyn Input<D>>) -> Result<(), Vec<ArgumentName>>;
    fn arguments(&self) -> Arguments<dyn Input<D>>;
    fn run(&self, arguments: Arguments<dyn Input<D>>, view: View<D>) -> Self::ReturnType; //todo: is there a way to prevent manual execution by the app?
    /// Checks whether the Command can be run in this View
    fn valid(&self, view: View<D>) -> bool;
    fn name() -> NameType;
    fn tags() -> Tags;
    fn annotations() -> Annotations;
}

// fn(Arguments, View<D>) -> Result<Success, Error>;
