use crate::database::Database;

use super::{command::Command, data::ChangeData, Change};

pub(crate) struct OngoingChange<D: Database, C: Command<D>> {
    change: Change,
    data: ChangeData<D>,
    command: Option<C>,
}
