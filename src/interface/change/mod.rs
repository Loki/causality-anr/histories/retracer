use std::collections::HashMap;

use crate::{history::{
    command::{command_type::CommandType, session::Session, arguments::Argument},
    common::{annotation::Annotations, tag::Tags},
    element::{element::Element, element_type::ElementType},
    ids::{ChangeUID, ComponentID},
    input::input_type::InputType,
    record::{change::ChangeRecord, input::InputRecord, state::StateRecord},
    reference::Reference,
}, database::Database};

use self::{input::Input, paradox::Paradox};

//pub mod arguments;
//pub mod change;
pub mod command;
pub mod data;
pub mod input;
pub mod ongoing_change;
pub mod paradox;
pub mod region;
pub mod solver;
//pub mod command_factory;
//pub mod execution;
//pub mod paradox;
//pub mod version;

pub(crate) struct Change {
    /// The currently created change's unique identifier
    /// Will help identifying it when exchanged with other histories
    id: ChangeUID,
    /// The current change's record, containing the corresponding CommandVersion
    record: ChangeRecord,
    /// contains the created ElementVersions
    states: HashMap<usize, StateRecord>,
    /// contains the CommandVersions of the Commands ran
    changes: Vec<Change>,
    /// contains the registered InputVersions
    inputs: Vec<InputRecord>,
    new_elements: Vec<Element>,
    new_sessions: Vec<Session>,
    new_element_types: Vec<ElementType>,
    new_command_types: Vec<CommandType>,
    new_input_types: Vec<InputType>,
    used_targets: Vec<Reference>,
    used_parameters: Vec<Reference>,
    changed_tags: HashMap<ComponentID, Tags>,
    changed_annotations: HashMap<ComponentID, Annotations>,
}

impl Change {
    pub(crate) fn new() -> Self {}

    // override any T/I/P link
    pub fn override_usage<D: Database>(index: usize, data: Argument<dyn Input<D>>) {}
    // for simplicity
    pub fn override_paradox<D: Database>(pdx: Paradox, data: Argument<dyn Input<D>>) {}
}
