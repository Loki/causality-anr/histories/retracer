use crate::{
    database::Database, history::reference::change::ChangeReference, interface::view::View,
};

pub trait Region {
    fn update<D: Database>(change: ChangeReference, view: View<D>) -> Self;
}
