use std::fmt::Display;

use serde::{de::DeserializeOwned, Serialize};

use crate::{
    database::Database,
    interface::{change::Change, view::View},
};

pub trait Solver: Serialize + DeserializeOwned {
    fn solve<D: Database>(
        &mut self,
        change: &mut Change,
        view: &View<D>,
    ) -> Result<(), Box<dyn Display>>;
}

/*
pub struct Remove<D: Database + Default, T: Display>(Paradox<D, T>);

impl<D: Database + Default, T: Display> Solver<D, T> for Remove<D, T> {
    type Result = ();
    fn set_paradox(&mut self, paradox: Paradox<D, T>) {
        self.0 = paradox;
    }

    fn run(&mut self) -> Self::Result {
        self.paradox_mut().change_mut().cancel_purge();
        ()
    }

    fn paradox_mut(&mut self) -> &mut Paradox<D, T> {
        &mut self.0
    }
}*/
