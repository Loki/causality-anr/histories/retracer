use crate::{history::{
    command::arguments::{Argument, Dynamic},
    common::{annotation::Annotations, tag::Tags},
    ids::{ArgumentName, ChangeUID},
    reference::Reference,
}, Path};

pub enum ParadoxPlace {
    Argument(ArgumentName),
    Input(Path),
    Parameter(Path),
    Target(Path),
}

pub struct Paradox {
    change: ChangeUID,
    origin: Path,
    initial: Argument<Reference>,
    ambiguous: Option<Argument<Reference>>,
    candidates: Vec<Candidate>,
}

pub struct Candidate {
    /// reason to choose this candidate
    mark: String,
    argument: Argument<Reference>,
}

impl Candidate {}
