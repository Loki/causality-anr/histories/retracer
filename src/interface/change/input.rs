use crate::{
    database::Database,
    history::{
        common::{annotation::Annotations, tag::Tags},
        ids::{InputTypeName, NameType},
    },
};

/** New data added to the history.
The data will be saved in a database, and registered in the history along with its type, tags and annotations.
*/
pub trait Input<D: Database> {
    fn input_type(&self) -> InputTypeName;
    fn data(&self) -> Option<D::RecordData>;
    fn name(&self) -> NameType;
    fn tags(&self) -> Tags;
    fn annotations(&self) -> Annotations;
}
