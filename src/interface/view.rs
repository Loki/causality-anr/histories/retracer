use std::marker::PhantomData;

use crate::{
    database::Database,
    history::{
        command::arguments::Arguments, ids::{ArgumentName, SessionUID}, reference::{state::StateReference, Reference}, History,
    }, Path,
};

use super::{
    change::{command::Command, ongoing_change::OngoingChange, solver::Solver, Change, data::ChangeData},
    history::HistoryInterface,
    listeners::Listeners,
    mode::Mode,
    query::{into_query::IntoQuery, state_record::StateRecordQuery},
    rights::{
        rights_3_types_entities_creation::TypeAndEntityCreationRights,
        rights_4_execution_recording::ExecutionRecordingRights,
    },
    shared::{RcType, Shared},
};

pub struct View<D: Database, R = TypeAndEntityCreationRights> {
    history: Shared<History>,
    database: Shared<D>,
    //command_factory: Rc<dyn CommandFactory<D>>,
    current: StateReference,
    /// In detached mode, any of the History's recorded States can be worked on.
    /// The current state is not attached to the View.
    /// Any State can be loaded, but the moves between States won't be registered since the View is
    /// not tracking them.
    /// If in detached mode, contains the UID of the Session the View is the interface of.
    detached: Option<SessionUID>,
    //paradox_listeners: Vec<dyn ParadoxListener>,
    listeners: Listeners,
    mode: Mode,
    rights: R,
}

impl<R, D: Database> View<R, D> {
    pub(crate) fn new(
        history: &Shared<History>,
        database: &Shared<D>,
        current: Option<StateReference>,
        mode: Mode,
    ) -> Self {
        Self {
            history: RcType::clone(history),
            database: RcType::clone(database),
            current,
            detached: todo!(),
            listeners: Default::default(),
            mode,
            rights: PhantomData,
        }
    }
}

impl<R, D: Database> View<R, D> {
    pub fn run<C: Command>(
        &mut self,
        cmd: C,
    ) -> Result<(C::ReturnType, Option<(Change, ChangeData<D>)>), (C::ReturnType, Change, ChangeData<D>)> {
        // Option because when success without ambiguity, auto recording happens. use run_only to
        // prevent this

        // check command arguments //
        if let Some(args) = self.check_arguments(cmd.arguments()) {
            return Err(args);
        };
        // new Change //
        let view = self.clone();
        // run Cmd //
        return Ok(cmd.run(cmd.arguments(), view));
    }

    fn check_arguments(&self, args: Arguments) -> Option<Vec<ArgumentName>> {}

    pub fn solve<C: Command>(
        &mut self,
        change: Change<D>,
        solver: &mut dyn Solver,
    ) -> Result<(C::ReturnType, C), (C::ReturnType, Change<D>)> {
    }

    /// Register Changes generated from running Commands
    pub fn register<C: Command>(change: OngoingChange<D, C>) -> Result<(), ()> {
        todo!()
    }

    pub fn update(arguments: Arguments /*region: Option<Region>*/) {}

    pub fn current_state(&mut self) -> StateRecordQuery<R, D> {
        self.query(self.current)
    }

    pub fn query<T>(&self, reference: &T) -> T::Query
    where T: IntoQuery<R> {
        reference.into_query(self)
    }
    
}

impl<D: Database> View<ExecutionRecordingRights, D> {
    //
    pub(crate) fn new_session_from_here(&self) -> Self {
        let session = self.history().new_session(self.current);
        Self::new(self.history(), &self.database, self.current, self.mode())
    }

    /// Sets the current StateReference and therefore creates a new View State with it.
    /// Only works if there aren't any pending changes to be recorded, or else it will return false.
    pub fn focus_on<T>(&mut self, reference: &T) -> bool
    where T: Into<StateReference> {
        todo!()
    }

    /// Records the pending changes.
    pub fn record_changes(&mut self) -> bool {
        todo!()
    }

}

impl<D: Database> View<TypeAndEntityCreationRights, D> {
    //
    pub(crate) fn new_session_from_here(&self) -> Self {
        let session = self.history().new_session(self.current);
        Self::new(self.history(), &self.database, self.current, self.mode())
    }
}

/*
impl<D: Database> View<RecordMode<D>, D> {
    //// mutation methods
    pub fn argument(name: ArgumentName) -> Argument {}
}*/

// can't return anything else than a reference
/*impl<MT: Into<Mode>, D: Database> Index<Path> for View<MT, D> {
    type Output = Reference;

    fn index(&self, index: Path) -> &Self::Output {
        &self
            .get(index)
            .expect("The View has no current state to index into")
    }
}*/

impl<R, D: Database> Into<HistoryInterface<R, D>> for View<R, D> {
    fn into(self) -> HistoryInterface<R, D> {
        HistoryInterface::new_with(self.history, self.database, self.mode)
    }
}

pub enum UpdateConstraint {
    Element,
    Type,
    Path,
}
