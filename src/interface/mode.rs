use super::{change::Change, shared::Shared};

/// History navigation mode
pub(crate) enum Mode {
    ReadOnly,
    ManageDescriptions,
    RecordExecution(Shared<Change>),
    EditHistory,
}
