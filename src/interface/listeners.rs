use std::collections::HashMap;

use crate::database::Database;

use super::view::View;

/// Bit flags:
/// (Inconsistent, Ambiguous, NoParadox) * (Ok, Err, NoResult)
pub type EventFlags = u16;
pub type Listeners = HashMap<EventFlags, dyn ExecutionListener>;

pub trait ExecutionListener {
    fn run<D: Database>(&mut self, view: View<D>);
}
