use std::fmt::Debug;
use std::marker::PhantomData;

use crate::database::Database;

pub struct NoDB<D>(PhantomData<D>);

impl<D> Database for NoDB<D>
where
    D: Default + Debug + Clone,
{
    type Snapshot = D;

    type SnapshotID = D;

    fn new() -> Self {
        Self(PhantomData::default())
    }

    fn insert(&mut self, data: Self::Snapshot) -> Self::SnapshotID {
        data
    }

    fn remove(&mut self, key: Self::SnapshotID) -> Self::Snapshot {
        key
    }

    fn get(&self, key: Self::SnapshotID) -> Self::Snapshot {
        key
    }

    fn set(&mut self, key: Self::SnapshotID, data: Self::Snapshot) {
        todo!()
    }

    fn mark_checkpoint(&mut self, key: Self::SnapshotID) {
        todo!()
    }
}
