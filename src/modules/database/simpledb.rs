use crate::database::Database;
use std::fmt::Debug;

type Checkpoint = bool;

pub struct SimpleDB<D> {
    data: Vec<Option<(D, Checkpoint)>>,
}

impl<D> Database for SimpleDB<D>
where
    D: Default + Debug + Clone,
{
    type Snapshot = D;

    type SnapshotID = usize;

    fn new() -> Self {
        Self {
            data: Default::default(),
        }
    }

    fn insert(&mut self, data: Self::Snapshot) -> Self::SnapshotID {
        let key = self.data.len();
        self.data.push(Some((data, false)));
        key
    }

    fn remove(&mut self, key: Self::SnapshotID) -> Self::Snapshot {
        self.data[key].take().expect("Missing data").0
    }

    fn get(&self, key: Self::SnapshotID) -> Self::Snapshot {
        self.data[key].as_ref().expect("Missing data").0.clone()
    }

    fn set(&mut self, key: Self::SnapshotID, data: Self::Snapshot) {
        self.data[key].as_mut().expect("Missing data").0 = data;
    }

    fn mark_checkpoint(&mut self, key: Self::SnapshotID) {
        self.data[key].as_mut().expect("Missing data").1 = true;
    }
}
