use std::collections::HashMap;

use crate::database::{Database, Key};

pub type HashMapDB<V> = HashMap<Key, (bool, V)>;

impl<Key, Value> Database for HashMap<Key, (bool, _)> {
    type VersionData = Value;

    fn insert(
        &mut self,
        key: crate::database::Key,
        data: Self::VersionData,
    ) -> crate::database::Key {
        self.insert::<HashMap>(key, data);
        key
    }

    fn remove(&mut self, key: crate::database::Key) -> Option<Self::VersionData> {
        self.remove::<HashMap>(key).map(|(c, d)| d)
    }

    fn get(&self, key: crate::database::Key) -> Option<&Self::VersionData> {
        self.get::<HashMap>(key)
    }

    fn set(&mut self, key: crate::database::Key, data: Self::VersionData) {
        self.insert::<HashMap>(key, (false, data));
    }

    fn mark_checkpoint(&mut self, key: crate::database::Key, checkoint: bool) {
        let (c, d) = self[key];
        self.insert::<HashMap>(key, (!c, *d));
    }
}
