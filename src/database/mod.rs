use std::{any::Any, clone::Clone};

use crate::{
    history::{record::attribute_tree::AttributeTree, reference::Reference},
    Path,
};

/// The command history's database
///
/// All snapshot data is kept by the database.
/// The data is linked to a unique key.
pub trait Database: Any {
    /// Snapshot data
    type VersionData: Clone;
    type RecordData: Clone + RecordInstance<Self::VersionData>;
    /// New database
    fn new() -> Self;
    /// Insert Record data if it isn't already in the database.
    /// If the key already exists, the function will overwrite the existing data if any and return Err with the existing Reference.
    fn insert_record(&mut self, key: Reference, data: Self::RecordData) -> Reference;
    /// Remove the corresponding RecordData
    fn remove_record(&mut self, key: Reference) -> Option<Self::RecordData>;
    /// Retrieve record data.
    /// If the data isn't available, it will return the list of Refernces to be computed to get the
    /// data, or None if the key is not in the database to begin with.
    fn record(&self, key: Reference) -> Result<Self::RecordData, Option<Vec<Reference>>>;
    /// Marks an snapshot as used as checkpoint
    /// Checkpoints mark important reconstruction snapshots.
    /// A minimal version of the database can be obtained by deleting all non-checkpoint snapshots it contains.
    /// That data will be reconstructible by the history.
    fn mark_checkpoint(&mut self, key: Reference, checkoint: bool);
    //todo: sync up deleted snapshots with keys held by the history
}

pub trait RecordInstance<VD: Clone> {
    fn version_at(&self, path: Path) -> Option<&VD>;
    fn attributes_tree(&self) -> AttributeTree;
    fn record_at(&self, path: Path) -> Option<&Self>;
}

impl<VD: Clone> RecordInstance<VD> for VD {
    fn version_at(&self, path: Path) -> Option<&VD> {
        //todo: depends on the path
        Some(&self)
    }

    fn attributes_tree(&self) -> AttributeTree {
        Default::default()
    }

    fn record_at(&self, path: Path) -> Option<&Self> {
        //todo: depends on the path
        None
    }
}
