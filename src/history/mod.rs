/*! The core history structure.
    This module contains the structures composing the editing trace.
    The interface module provides means to build and read this trace, and the control module allows for editing it.

    # State data
    ## Elements
    [Element]s represent an entity, like a specific layer in a drawing application, who's evolution will be tracked and recorded.
    An [Element] should be created for all data who's evolution should be tracked.
    Every [Element] has an [ElementType] describing it's nature, and a list of [ElementVersion]s.
    An [ElementVersion] represents a state the Element is or has been in.

    ## Inputs
    Aside from [Element]s are Inputs, representing untracked data used by [Commands] like user inputs for instance.
    They're saved within an [InputVersion] typed via an [InputType].

    # Change data
    ## Commands
    Commands manipulate the Element's state, and in doing so generate new [ElementVersions].
    On each Command execution, a [CommandVersion] is generated, associated to a [CommandType].

    # Inputs and Commands
    Inputs and Commands aren't meant to evolve, and thus no [Input](super::interface::change::input::Input) and [Command](super::interface::change::command::Command) structure exists in the trace, unlike [Element]s which gather all their [ElementVersions].
    It is however possible to link two [InputVersion]s or [CommandVersion]s together to represent a occasional evolution if necessary, through [TRIP links](self::command::execution).

    # Types
    The typing system is mainly used to check Command Arguments validity.
    It has a basic inheritance system to express equivalence relations between types.
*/

//use log::debug;
use serde::{Deserialize, Serialize};

use std::{
    collections::HashMap,
    ops::{Index, IndexMut},
};

//use crate::fuses::MAX_RETRIES;

use self::{
    command::{command_type::CommandType, command_version::CommandVersion, session::Session},
    element::{element::Element, element_type::ElementType, element_version::ElementVersion},
    ids::{
        new_uuid, CommandTypeName, ElementTypeName, ElementUID, HistoryUID, IDType, Identify,
        InputTypeName, SessionUID, CVID, EVID, IVID,
    },
    input::{input_type::InputType, input_version::InputVersion},
    registry::Registry,
    state::State,
};

pub mod command;
pub mod metadata;
pub mod element;
pub mod ids;
pub mod input;
pub mod path;
pub mod record;
pub mod reference;
pub mod registry;
pub mod state;

/** The history structure.
It contains the trace describing the evolution of [Element]s, modified through the execution of [Command]s during editing [Session]s.

*/
#[derive(Debug, Serialize, Deserialize)]
pub struct History {
    uid: HistoryUID,

    element_types: HashMap<ElementTypeName, ElementType>,
    input_types: HashMap<InputTypeName, InputType>,
    command_types: HashMap<CommandTypeName, CommandType>,

    elements: Vec<Element>,
    sessions: Vec<Session>,

    /// Monitors ids, incomplete changes and edits
    registry: Registry,
}

impl History {
    /// Create a new empty History
    pub fn new() -> Self {
        Self {
            uid: new_uuid(),
            ..Default::default()
        }
    }
}

impl Default for History {
    fn default() -> Self {
        Self::new()
    }
}

impl History {
    pub fn uid(&self) -> HistoryUID {
        self.uid
    }
    pub fn element_types(&self) -> &HashMap<ElementTypeName, ElementType> {
        &self.element_types
    }
    pub fn elements(&self) -> &Vec<Element> {
        &self.elements
    }
    pub fn element_versions(&self, element: usize) -> &Vec<ElementVersion> {
        &self.elements[element].versions()
    }
    pub fn input_types(&self) -> &HashMap<InputTypeName, InputType> {
        &self.input_types
    }
    pub fn input_versions(&self, input_type: InputTypeName) -> &Vec<InputVersion> {
        &self.input_types[&input_type].versions()
    }
    pub fn command_types(&self) -> &HashMap<CommandTypeName, CommandType> {
        &self.command_types
    }
    pub fn sessions(&self) -> &Vec<Session> {
        &self.sessions
    }

    pub fn command_version(&self, command_version_idx: CVID) -> &CommandVersion {
        &self.session(command_version_idx.session())[command_version_idx.version()]
    }

    /// gets a session by its SessionUID if there is any.
    /// # Panics
    /// if the session index is out of bounds
    pub fn session_by_uid(&self, uid: SessionUID) -> Option<&Session> {
        self.registry.session_id(uid).map(|idx| &self.sessions[idx])
    }
    pub fn session_versions(&self, session: usize) -> &Vec<CommandVersion> {
        &self.sessions[session].command_versions()
    }

    pub fn element_type_mut(&mut self, id: ElementTypeName) -> &mut ElementType {
        self.element_types.get_mut(&id).unwrap()
    }
    pub fn element_mut(&mut self, id: usize) -> &mut Element {
        &mut self.elements[id]
    }
    pub fn element_version_mut(&mut self, id: EVID) -> &mut ElementVersion {
        &mut self.element_mut(id.element()).versions_mut()[id.version()]
    }
    pub fn input_type_mut(&mut self, id: InputTypeName) -> &mut InputType {
        self.input_types.get_mut(&id).unwrap()
    }
    pub fn input_version_mut(&mut self, id: IVID) -> &mut InputVersion {
        &mut self.input_type_mut(id.input_type()).versions_mut()[id.version()]
    }
    pub fn command_type_mut(&mut self, id: CommandTypeName) -> &mut CommandType {
        self.command_types.get_mut(&id).unwrap()
    }
    //todo: IDType instead of usize
    pub fn session(&self, id: IDType) -> &Session {
        &self.sessions[id]
    }
    pub fn session_mut(&mut self, id: usize) -> &mut Session {
        &mut self.sessions[id]
    }
    pub fn command_version_mut(&mut self, id: CVID) -> &mut CommandVersion {
        &mut self.session_mut(id.session()).command_versions_mut()[id.version()]
    }

    /*
    /// Generate a new ElementUID
    pub fn new_element_uid(&mut self) -> ElementUID {
        self.registry.new_element_uid()
    }

    /// Generate a new SessionUID
    pub fn new_session_uid(&mut self) -> SessionUID {
        self.registry.new_session_uid()
    }
    */

    ///  Adds an ElementType to the history.
    ///  Returns a reference to it if the history doesn't contain it already, or else returns the
    ///  given structure to the type already present.
    pub fn new_element_type<ET: Into<ElementType> + Identify<ElementTypeName>>(
        &mut self,
        element_type: ET,
    ) -> Result<ElementTypeName, ET /*already exists*/> {
        /*
        let id = element_type.name().ok_or_else(|| element_type)?;
        if self.element_types().contains_key(&id) {
            Err(element_type)
        } else {
            self.element_types.insert(id, element_type.into());
            Ok(id)
        }*/todo!()
    }

    ///  Adds a CommandType to the history.
    ///  Returns a reference to it if the history doesn't contain it already, or else returns the
    ///  given structure to the type already present.
    pub fn new_command_type<CT: Into<CommandType> + Identify<CommandTypeName>>(
        &mut self,
        command_type: CT,
    ) -> Result<CommandTypeName, CT /*already exists*/> {
        /*
        let id = command_type.name().ok_or_else(|| command_type)?;
        if self.command_types().contains_key(&id) {
            Err(command_type)
        } else {
            self.command_types.insert(id, command_type.into());
            Ok(id)
        }*/todo!()
    }

    ///  Adds a InputType to the history.
    ///  Returns a reference to it if the history doesn't contain it already, or else returns the
    ///  given structure to the type already present.
    pub fn new_input_type<IT: Into<InputType> + Identify<InputTypeName>>(
        &mut self,
        input_type: IT,
    ) -> Result<InputTypeName, IT /*already exists*/> {
        /*
        let id = input_type.name().ok_or_else(|| input_type)?;
        if self.input_types().contains_key(&id) {
            Err(input_type)
        } else {
            self.input_types.insert(id, input_type.into());
            Ok(&self.input_types()[id].unwrap())
        }*/todo!()
    }

    /*
    pub fn drop_new_element_uid(
        &mut self,
        element_uid: ElementUID,
    ) -> Result<Option<ElementUID>, ()> {
        self.registry.drop_new_element_uid(element_uid)
    }
    pub fn drop_new_session_uid(
        &mut self,
        session_uid: SessionUID,
    ) -> Result<Option<SessionUID>, ()> {
        self.registry.drop_new_session_uid(session_uid)
    }

    ///  Adds an InputType to the history.
    ///  Returns a reference to it if the history doesn't contain it already, or else returns a reference
    ///  to the version already present.
    pub fn try_add_input_type(&mut self, input_type: InputType) -> Result<&InputType, &InputType> {
        let name = input_type.name().clone();
        if self.input_types.contains_key(&name) {
            return Err(&self.input_types[&name]);
        } else {
            self.input_types
                .insert(input_type.name().clone(), input_type);
            return Ok(&self.input_types[&name]);
        }
    }
    pub fn try_add_element(
        &mut self,
        element: Element,
        element_uid: ElementUID,
    ) -> Result<&Element, Option<&Element>> {
        if self
            .registry
            .try_set_new_element(element_uid, self.elements.len())
            .is_ok()
        {
            self.elements.push(element);
            return Ok(&self.elements[&self.elements.len() - 1]);
        } else {
            return Err(self
                .registry
                .element_id(element_uid)
                .map(|id| &self.elements[id]));
        }
    }
    pub fn try_add_session(
        &mut self,
        session: Session,
        session_uid: SessionUID,
    ) -> Result<&Session, Option<&Session>> {
        if self
            .registry
            .try_set_new_session(session_uid, self.sessions.len())
            .is_ok()
        {
            self.sessions.push(session);
            return Ok(&self.sessions[&self.sessions.len() - 1]);
        } else {
            return Err(self
                .registry
                .session_id(session_uid)
                .map(|id| &self.sessions[id]));
        }
    }*/

    pub fn new_element(
        &mut self,
        meta_data: Option<ComponentMetaData>,
        element_uid: ElementUID,
    ) -> Result<&Element, Option<&Element>> {
        /*
        if self
            .registry
            .try_set_new_element(element_uid, self.elements.len())
            .is_ok()
        {
            self.elements.push(element);
            return Ok(&self.elements[&self.elements.len() - 1]);
        } else {
            return Err(self
                .registry
                .element_id(element_uid)
                .map(|id| &self.elements[id]));
        }*/
        todo!()
    }

    pub fn new_session(&mut self, start: Option<State>) -> &Session {
        /*
        let mut uid = self.registry.new_uid();
        let mut retried = 0;
        while self.registry.uids().contains(uid) && retried < MAX_RETRIES {
            uid = self.registry.new_uid();
            retried += 1;
        }
        debug!("retried getting a UUID {} times to get a new one", retried);
        // create a session
        self.sessions.push(Session::new(uid));
        // create a corresponding View Element
        //self.new_element(uid);
        todo!();
        //debug_assert!()

        if let Some(state) = start {
        } else {
        }*/todo!()
    }
    /*
    pub fn try_add_element_version(&mut self, element_version: ElementVersion) -> Option<EVID> {
        let e = element_version.element();
        self.elements
            .get_mut(e)?
            .versions_mut()
            .push(element_version);
        Some((e, self.elements[e].versions().len() - 1))
    }
    pub fn try_add_command_version(&mut self, command_version: CommandVersion) -> Option<CVID> {
        let s = command_version.session();
        self.sessions
            .get_mut(s)?
            .versions_mut()
            .push(command_version);
        Some((s, self.sessions[s].versions().len() - 1))
    }
    pub fn try_add_input_version(&mut self, input_version: InputVersion) -> Option<IVID> {
        let i = input_version.input_type().clone();
        self.input_types
            .get_mut(&i)?
            .versions_mut()
            .push(input_version);
        Some((i.clone(), self.input_types[&i].versions().len() - 1))
    }*/
}

impl Index<EVID> for History {
    type Output = ElementVersion;

    fn index(&self, index: EVID) -> &Self::Output {
        todo!()//&self.element(index.element())[index.version()]
    }
}
impl IndexMut<EVID> for History {
    fn index_mut(&mut self, index: EVID) -> &mut Self::Output {
        &mut self.element_mut(index.element())[index.version()]
    }
}

impl Index<CVID> for History {
    type Output = CommandVersion;

    fn index(&self, index: CVID) -> &Self::Output {
        &self.session(index.session())[index.version()]
    }
}
impl IndexMut<CVID> for History {
    fn index_mut(&mut self, index: CVID) -> &mut Self::Output {
        &mut self.session_mut(index.session())[index.version()]
    }
}

#[cfg(test)]
mod add_elements_and_sessions {

    #[test]
    fn drop_element_uid_success() {}
    #[test]
    fn drop_session_uid_success() {}
    #[test]
    fn drop_element_uid_failure() {}
    #[test]
    fn drop_session_uid_failure() {}

    #[test]
    fn add_element_success() {}
    #[test]
    fn add_session_success() {}
    #[test]
    fn add_element_failure() {}
    #[test]
    fn add_session_failure() {}
}

#[cfg(test)]
mod add_types {

    #[test]
    fn add_element_type_success() {}
    #[test]
    fn add_input_type_success() {}
    #[test]
    fn add_command_type_success() {}
    #[test]
    fn add_element_type_failure() {}
    #[test]
    fn add_input_type_failure() {}
    #[test]
    fn add_command_type_failure() {}
}

#[cfg(test)]
mod add_versions {

    #[test]
    fn add_element_version_success() {}
    #[test]
    fn add_input_version_success() {}
    #[test]
    fn add_command_version_success() {}
    #[test]
    fn add_element_version_failure() {}
    #[test]
    fn add_input_version_failure() {}
    #[test]
    fn add_command_version_failure() {}
}
