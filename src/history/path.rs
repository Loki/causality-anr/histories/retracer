use serde::{Deserialize, Serialize};

#[derive(Clone, Default, Debug, Serialize, Deserialize)]
pub struct Path {
    path: String,
}

#[derive(Clone, Default, Debug, Serialize, Deserialize)]
pub struct StatePath {
    path: String,
}

#[derive(Clone, Default, Debug, Serialize, Deserialize)]
pub struct ChangePath {
    path: String,
}
