use crate::history::ids::AttributeName;

#[derive(Debug, Default, Clone)]
pub struct AttributeTree {
    /// each argument and its children's index
    arguments: Vec<(AttributeName, Vec<usize>)>,
}
