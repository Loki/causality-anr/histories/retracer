use serde::{Deserialize, Serialize};

use crate::history::command::command_version::CommandVersion;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct ChangeRecord {
    command_version: CommandVersion,
}
