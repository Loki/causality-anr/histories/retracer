use serde::{Deserialize, Serialize};

use crate::history::{input::input_version::InputVersion, reference::change::ChangeReference};

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct InputRecord {
    input_version: InputVersion,
    origin: ChangeReference,
}
