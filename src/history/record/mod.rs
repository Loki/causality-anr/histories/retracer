pub mod attribute_tree;
pub mod change;
pub mod input;
pub mod state;

use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum Record {
    State(state::StateRecord),
    Change(change::ChangeRecord),
    Input(input::InputRecord),
}

/*
impl Record {
    pub(crate) fn data<D: Database>(&self, view: View<D>) -> DataTree<D> {
        match self {
            Record::State(state) => state.data(view),
            Record::Change(change) => change.data(view),
            Record::Input(input) => input.data(view),
        }
    }
}*/
