use serde::{Deserialize, Serialize};

use crate::history::{
    element::element_version::ElementVersion, reference::change::ChangeReference,
};

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct StateRecord {
    element_versions: Vec<ElementVersion>,
    origin: ChangeReference,
    //data: Vec<>,
}
