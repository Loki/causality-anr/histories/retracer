/*! Element type
*/

use serde::{Deserialize, Serialize};

use std::collections::HashMap;

use crate::history::{
    metadata::Metadata,
    ids::{AttributeName, ElementTypeName, TypeName},
};

/// Describes the type of [super::element::Element]s.
///
/// Types help knowing the nature of the data tracked by [super::element::Element]s.
/// They specify the attributes the [super::element::Element] will always have throughout its evolution.
/// Equivalence between [super::element::Element]s can be inferred by their [ElementType].
/// Two [super::element::Element]s of the same type are usable interchangeably, and the inheritance information establishes further equivalence between types.
/// An [ElementType] can specify the other types it is equivalent to.
///
/// # Examples
///
/// ```
/// let mut color_space = ElementType::new("ColorSpace");
///
/// let mut color = ElementType::new("Color");
/// 
/// let mut rgb = ElementType::new("RGB");
/// rgb.attributes_mut().insert("red", Some("Integer"));
/// rgb.attributes_mut().insert("green", Some("Integer"));
/// rgb.attributes_mut().insert("blue", Some("Integer"));
/// rgb.inherits_mut().push("Color");
/// 
/// let mut rgba = ElementType::new("RGBA");
/// rgba.attributes_mut().insert("alpha", Some("Integer"));
/// rgba.inherits_mut().push("RGB");
/// 
/// let mut hsl = ElementType::new("HSL");
/// hsl.attributes_mut().insert("hue", Some("Integer"));
/// hsl.attributes_mut().insert("saturation", Some("Integer"));
/// hsl.attributes_mut().insert("lightness", Some("Integer"));
/// hsl.inherits_mut().push("Color");
/// 
/// let mut hsv = ElementType::new("HSV");
/// hsv.attributes_mut().insert("hue", Some("Integer"));
/// hsv.attributes_mut().insert("saturation", Some("Integer"));
/// hsv.attributes_mut().insert("value", Some("Integer"));
/// hsv.inherits_mut().push("Color");
/// 
/// let mut canvas = ElementType::new("Canvas");
/// canvas.attributes_mut().insert("color_space", Some("ColorSpace"));
/// canvas.attributes_mut().insert("dimensions", None);
///
///
///
/// history.add_element_type(color_space);
/// history.add_element_type(color);
/// history.add_element_type(rgb);
/// history.add_element_type(rgba);
/// history.add_element_type(hsl);
/// history.add_element_type(hsv);
/// history.add_element_type(canvas);
/// ```
///
#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct ElementType {
    /// Unique identifier.
    name: ElementTypeName,
    /// List of always existing attributes.
    attributes: HashMap<AttributeName, Option<TypeName>>,
    /// Types this one can substitute.
    inherits: Vec<TypeName>,
    metadata: Metadata,
}

// Name as uid
impl PartialEq for ElementType {
    fn eq(&self, other: &Self) -> bool {
        self.name.eq(&other.name)
    }
}

impl Eq for ElementType {}


impl ElementType {
    pub fn new(name: ElementTypeName) -> Self {
        Self {
            name,
            ..Default::default()
        }
    }

    pub fn attributes(&self) -> &HashMap<AttributeName, Option<TypeName>> {
        &self.attributes
    }

    pub fn attributes_mut(&mut self) -> &mut HashMap<AttributeName, Option<TypeName>> {
        &mut self.attributes
    }

    pub fn inherits(&self) -> &Vec<TypeName> {
        &self.inherits
    }

    pub fn inherits_mut(&mut self) -> &mut Vec<TypeName> {
        &mut self.inherits
    }

    pub fn metadata(&self) -> &Metadata {
        &self.metadata
    }

    pub fn metadata_mut(&mut self) -> &mut Metadata {
        &mut self.metadata
    }
}



//#[cfg(test)]
//mod tests {
    //#[test]
//}
