/*! Elements.
Elements identify uniquely a coherent unit of information, like a canvas, a layer, a 2D/3D object, …
*/

use serde::{Deserialize, Serialize};

use std::ops::{Deref, DerefMut, Index, IndexMut};

use crate::history::{
    common::component_meta_data::ComponentMetaData,
    ids::{ElementTypeName, ElementUID, IDType, NameType},
    state::State,
};

use super::element_version::ElementVersion;

#[derive(Default, Debug, Clone, Serialize, Deserialize)]
pub struct Element {
    uid: ElementUID,
    element_type: ElementTypeName,
    cp: ComponentMetaData,
    /// Elements built from one of this Element's States
    copies: Vec<IDType>,
    element_versions: Vec<ElementVersion>,
}

impl Deref for Element {
    type Target = ComponentMetaData;

    fn deref(&self) -> &Self::Target {
        &self.cp
    }
}

impl DerefMut for Element {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.cp
    }
}

impl Element {
    pub fn uid(&self) -> ElementUID {
        self.uid
    }
    pub fn uid_mut(&mut self) -> &mut ElementUID {
        &mut self.uid
    }
    pub fn element_type(&self) -> &ElementTypeName {
        &self.element_type
    }
    pub fn element_type_mut(&mut self) -> &mut ElementTypeName {
        &mut self.element_type
    }
    pub fn relatives(&self) -> &Vec<ElementUID> {
        todo!()//&self.relatives
    }
    pub fn relatives_mut(&mut self) -> &mut Vec<ElementUID> {
        todo!()//&mut self.relatives
    }
    pub fn versions(&self) -> &Vec<ElementVersion> {
        self.element_versions.as_ref()
    }
    pub fn versions_mut(&mut self) -> &mut Vec<ElementVersion> {
        &mut self.element_versions
    }
}

impl Element {
    pub(crate) fn new(name: NameType, element_type: ElementTypeName) -> Self {
        Element {
            element_type,
            cp: ComponentMetaData::new(name),
            ..Default::default()
        }
    }
    pub(crate) fn new_from(name: NameType, element_type: ElementTypeName, state: State) -> Self {
        Element {
            element_type,
            cp: ComponentMetaData::new(name),
            ..Default::default()
        }; todo!("Add this Element to the state's Element's relatives")
    }
}

impl Index<IDType> for Element {
    type Output = ElementVersion;

    fn index(&self, index: IDType) -> &Self::Output {
        &self.element_versions[index]
    }
}

impl IndexMut<IDType> for Element {
    fn index_mut(&mut self, index: IDType) -> &mut Self::Output {
        &mut self.element_versions[index]
    }
}
