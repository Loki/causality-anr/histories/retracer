/*! Element module.
Everything related to representing edited data.
*/

pub mod element;
pub mod element_type;
pub mod element_version;
