/*! Element version representing a state of it at a moment in time.
*/
use serde::{Deserialize, Serialize};

use std::{
    collections::HashMap,
    ops::{Deref, DerefMut},
};

use crate::history::{
    common::version_component::VersionComponent,
    ids::{AttributeName, IDType, EVID},
};

/// A version of an [Element]
///
/// [ElementVersion]s are the representation of the state of an Element at a given moment in time.
/// They keep track of the position of the [Element] in the [State] tree as well as its snapshot.
/// They are also given their own name, tags and annotations. These are mutable and not versioned.
/// Everything relating to the corresponding [Element] is versioned and thus immutable.
/// A new [ElementVerion] has to be created to represent the new [Element] state.
#[derive(Clone, Default, Debug, Serialize, Deserialize)]
pub struct ElementVersion {
    /// The [Element] this [ElementVersion] is representing
    element: IDType,

    // The parent [ElementVersion] at creation
    parent: Option<EVID>,
    /// The child [ElementVersion]s, indexed by [ElementName]
    /// since there is only one [ElementVersion] per [Element] at each given State.
    /// AttributeNames that aren't part of this map will be given to the constructed element for interpretation.
    children: HashMap<AttributeName, EVID>, //children: HashMap<ElementName, EVID>,
    /// Component data
    cp: VersionComponent,
}

impl Deref for ElementVersion {
    type Target = VersionComponent;

    fn deref(&self) -> &Self::Target {
        &self.cp
    }
}

impl DerefMut for ElementVersion {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.cp
    }
}

impl ElementVersion {
    pub fn element(&self) -> IDType {
        self.element
    }
    pub fn element_mut(&mut self) -> &mut IDType {
        &mut self.element
    }
    pub fn parent(&self) -> Option<EVID> {
        self.parent
    }
    pub fn parent_mut(&mut self) -> &mut Option<EVID> {
        &mut self.parent
    }
    pub fn children(&self) -> &HashMap<AttributeName, EVID> {
        &self.children
    }
    pub fn children_mut(&mut self) -> &mut HashMap<AttributeName, EVID> {
        &mut self.children
    }
}
