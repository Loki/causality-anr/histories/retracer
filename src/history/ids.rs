/*! History trace identifiers.
 */

use serde::{Deserialize, Serialize};

use crate::serialization;

use super::reference::Reference;

pub type IDType = usize;
pub type UIDType = uuid::Uuid;
pub type NameType = String;
//pub type DataIDType = String;
/// Index of the Execution's corresponding TRIP link of the Usage
pub type TRIPID = usize;

pub type ChangeTrackIdx = usize;
pub type ChangeTrackStateIdx = usize;

pub type PathIdx = usize;


pub type HistoryUID = UIDType;
pub type ChangeUID = UIDType;

// Elements //
/// Name of an [ElementType]
pub type ElementTypeName = NameType;
/// Name of an [Attribute]
pub type AttributeName = NameType;
// Type of an Arguments updater
//pub type UpdaterType = NameType;
/// [Element] identifier
#[derive(Copy, Clone, PartialEq, PartialOrd, Eq, Hash, Ord, Debug, Serialize, Deserialize)]
pub struct ElementUID(UIDType);

impl Default for ElementUID {
    fn default() -> Self {
        ElementUID(new_uuid())
    }
}

/*
impl From<SessionUID> for ElementUID {
    fn from(value: SessionUID) -> Self {
        ElementUID(value.0)
    }
}*/

/// [ElementVersion] identifier (element, version)
#[derive(
    Copy, Clone, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize,
)]
pub struct EVID(IDType, IDType);

impl EVID {
    pub fn new(element: IDType, version: IDType) -> Self {
        Self(element, version)
    }
    pub fn element(&self) -> IDType {
        self.0
    }
    pub fn version(&self) -> IDType {
        self.1
    }
}
// Commands //
/// Name of a [CommandType]
pub type CommandTypeName = NameType;
/// Name of an [Argument]
pub type ArgumentName = NameType;
/// [Session] identifier
pub type SessionUID = ElementUID;
/*
#[derive(Copy, Clone, PartialEq, PartialOrd, Eq, Hash, Ord, Debug, Serialize, Deserialize)]
pub struct SessionUID(UIDType);
impl Default for SessionUID {
    fn default() -> Self {
        SessionUID(new_uuid())
    }
}*/

/// [CommandVersion] identifier (session, version)
#[derive(
    Copy, Clone, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize,
)]
pub struct CVID(IDType, IDType);

impl CVID {
    pub fn new(session: IDType, version: IDType) -> Self {
        Self(session, version)
    }
    pub fn session(&self) -> IDType {
        self.0
    }
    pub fn version(&self) -> IDType {
        self.1
    }
}

// Inputs //
/// Name of an [InputType]
pub type InputTypeName = NameType;
/// [InputVersion] identifier
#[derive(
    Clone, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize,
)]
pub struct IVID(InputTypeName, IDType);

impl IVID {
    pub fn new(input_type: InputTypeName, version: IDType) -> Self {
        Self(input_type, version)
    }
    pub fn input_type(&self) -> InputTypeName {
        self.0.clone()
    }
    pub fn version(&self) -> IDType {
        self.1
    }
}

// Type Names
#[derive(Clone, Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum TypeName {
    Element(ElementTypeName),
    Command(CommandTypeName),
    Input(InputTypeName),
}

// Version types
#[derive(Clone, Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum VID {
    Element(EVID),
    Command(CVID),
    Input(IVID),
}

#[derive(Clone, Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum ComponentID {
    Type(TypeName),
    Element(ElementUID),
    Session(SessionUID),
    Record(Reference),
}

//#[derive(Clone, Debug, PartialEq, Eq, Hash)]
//pub enum FullCmpID {
//History(HistoryUID, ComponentID),
//Change(ChangeUID, ComponentID),
//}

// Timestamp
#[derive(Clone, Debug, PartialEq, PartialOrd, Ord, Eq, Hash, Serialize, Deserialize)]
pub struct Timestamp(#[serde(with = "serialization::timestamp")] chrono::DateTime<chrono::Utc>);

impl Timestamp {
    pub fn new() -> Self {
        Self(chrono::Utc::now())
    }
}

impl Default for Timestamp {
    fn default() -> Self {
        Self(chrono::Utc::now())
    }
}

pub fn new_uuid() -> uuid::Uuid {
    // create a Uuid
    /*
    use std::time::SystemTime;
    let ts = SystemTime::now()
        .duration_since(SystemTime::UNIX_EPOCH)
        .expect("Current time is earlier than the UNIX epoch.");
    let random_bytes = rand::prelude::random();
    uuid::Builder::from_ufrom_unix_timestamp_millis(
        ts.as_millis()
            .try_into()
            .expect("Couldn't get milliseconds from current time"),
        &random_bytes,
    )
    .into_uuid() */
    uuid::Uuid::new_v4()
}

pub enum Identifier<Id> {
    Index(IDType),
    Name(NameType),
    Id(Id),
}

pub trait Identify<IdentityT> {
    fn id(&self) -> Option<IdentityT>;
    fn name(&self) -> Option<NameType>;
    fn index(&self) -> Option<IDType>;
}
