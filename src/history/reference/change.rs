use std::hash::Hash;

use log::debug;
use serde::{Deserialize, Serialize};

use crate::history::{
    ids::{HistoryUID, SessionUID, Timestamp, CVID, ChangeUID},
    History, path::ChangePath,
};

#[derive(Default, Clone, Debug, Serialize, Deserialize)]
pub struct ChangeReference {
    uid: (HistoryUID, SessionUID, Timestamp),
    // when the change is pending
    running_execution: Option<ChangeUID>,
    sub_change: Option<ChangePath>,
    idx: Option<CVID>,
}

impl PartialEq for ChangeReference {
    fn eq(&self, other: &Self) -> bool {
        self.uid == other.uid
    }
}

impl Eq for ChangeReference {}

impl Hash for ChangeReference {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        todo!()
    }
}

impl ChangeReference {
    pub fn new(history: HistoryUID, session: SessionUID, timestamp: Timestamp) -> Self {
        Self {
            uid: (history, session, timestamp),
            idx: None,
            ..Default::default()
        }
    }
    pub fn history_uid(&self) -> HistoryUID {
        self.uid.0
    }

    pub fn session_uid(&self) -> SessionUID {
        self.uid.1
    }

    pub fn timestamp(&self) -> Timestamp {
        self.uid.2.clone()
    }
}

impl ChangeReference {
    pub(crate) fn compute_idx(&mut self, history: &History) -> bool {
        if self.idx.is_some() {
            return true;
        }
        /*
        let lock: Shared<History> = history.into();
        let history = lock.read().inspect_err(|err| {
            debug!("history lock is poisoned: {}", err);
        })?;*/
        if history.uid() != self.history_uid() {
            debug!("This ChangeReference is not from this history");
            return false;
        }
/*
        let session = history.session_by_uid(self.session_uid());
        if session.command_versions().last().timestamp() < self.timestamp() {
            debug!("The history is not up to date");
            return false;
        }
        self.idx = session
            .command_versions()
            .binary_search_by_key(&self.timestamp(), |&v| v.timestamp())?;
        return true;
    */
        todo!()
    }
}
