pub mod change;
pub mod input;
pub mod state;

use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum Reference {
    State(state::StateReference),
    Change(change::ChangeReference),
    Input(input::InputReference),
}
