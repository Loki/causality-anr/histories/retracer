use serde::{Deserialize, Serialize};

use crate::history::{ids::IVID, History};

use super::change::ChangeReference;

#[derive(Clone, Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct InputReference {
    uid: (ChangeReference, usize),
    idx: Option<IVID>,
}

impl InputReference {
    pub fn new(change: ChangeReference, index: usize) -> Self {
        Self {
            uid: (change, index),
            idx: None,
        }
    }
}

impl InputReference {
    pub(crate) fn compute_idx(&mut self, history: &History) -> bool {
        if self.idx.is_some() {
            return true;
        }
/*
        self.uid.0.compute_idx(history)?;

        let command_version_idx = self.uid.0.idx.unwrap(/*just got computed*/);
        self.idx = history[command_version_idx].execution().inputs().version;
        return true;
        */
        todo!()
    }

}
