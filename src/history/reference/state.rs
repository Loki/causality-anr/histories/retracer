use std::hash::Hash;

use serde::{Deserialize, Serialize};

use crate::history::{path::{Path, StatePath}, state::State, History, ids::{ChangeTrackIdx, ChangeTrackStateIdx}};

use super::{change::ChangeReference, Reference};

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct StateReference {
    uid: (ChangeReference, ChangeTrackIdx, ChangeTrackStateIdx),
    sub_state: Option<StatePath>,
    idx: Option<State>,
}

impl PartialEq for StateReference {
    fn eq(&self, other: &Self) -> bool {
        self.uid == other.uid
    }
}

impl Eq for StateReference {}

impl Hash for StateReference {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        todo!()
    }
}

impl StateReference {
    pub fn new(change: ChangeReference, state_id: usize, path: Path) /*-> Self*/ {}
    /*
    pub fn data<D: Database>(&self, view: View<D>) -> StateData<D> {}
    pub fn version_data<D: Database>(&self, view: View<D>) -> D::VersionData {
        view.data()
    }
    fn compute_state(&mut self, view: &View<D>) {
        if self.idx.is_some() {
            return;
        }
    }*/
}

impl Into<Reference> for StateReference {
    fn into(self) -> Reference {
        Reference::State(self)
    }
}
/*
pub struct StateData<D: Database> {
    path: Path,
    record: StateReference,
    data: D::VersionData,
    children: Vec<StateData<D>>,
}*/

impl StateReference {
    pub(crate) fn compute_idx(&mut self, history: &History) -> bool {
        /*
        if self.idx.is_some() {
            self.idx.unwrap().compute_tree(history);
            return true;
        }

        self.uid.0.compute_idx(history)?;

        /*
        let lock: Shared<History> = history.into();
        let history = lock.read().inspect_err(|err| {
            debug!("history lock is poisoned: {}", err);
        })?;
        if *history.uid() != self.history_uid() {
            debug!("This StateReference is not from this history");
            return false;
        }*/

        self.idx = Some(State::from(self.uid, history));
        self.idx.unwrap().compute_tree(history);
        return true;
        */todo!()
    }
}
