/*! Input version.
Represents outside data.
*/
use serde::{Deserialize, Serialize};

use crate::history::ids::InputTypeName;

use super::super::common::version_component::VersionComponent;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct InputVersion {
    input_type: InputTypeName,
    cp: VersionComponent,
}

// Getters //
impl InputVersion {
    pub fn new(type_name: InputTypeName) -> Self {
        Self {
            input_type: type_name,
            cp: Default::default(),
        }
    }
    pub fn input_type(&self) -> &InputTypeName {
        &self.input_type
    }

    pub fn input_type_mut(&mut self) -> &mut InputTypeName {
        &mut self.input_type
    }
}

/*
impl InputVersion {
    pub fn new<D: Database + Default>(history: History<D>, name: String, input_type: InputTypeName) -> Self {
        Self {
            input_type,
            cp: VersionComponent::new(history, name),
        }
    }
}*/
