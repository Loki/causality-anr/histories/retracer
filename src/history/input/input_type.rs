/*! Input type
*/
use std::ops::{Deref, DerefMut};

use serde::{Deserialize, Serialize};

use crate::history::common::type_component::TypeComponent;

use super::input_version::InputVersion;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct InputType {
    cp: TypeComponent,
    input_versions: Vec<InputVersion>,
}

impl Deref for InputType {
    type Target = TypeComponent;

    fn deref(&self) -> &Self::Target {
        &self.cp
    }
}

impl DerefMut for InputType {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.cp
    }
}

impl InputType {
    pub(crate) fn new(name: String) -> Self {
        Self {
            cp: TypeComponent::new(name),
            input_versions: Vec::new(),
        }
    }

    pub fn versions(&self) -> &Vec<InputVersion> {
        self.input_versions.as_ref()
    }

    pub(crate) fn versions_mut(&mut self) -> &mut Vec<InputVersion> {
        &mut self.input_versions
    }
}
