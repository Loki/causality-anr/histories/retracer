/*! Input module.
Everything related to representing not edited data.
*/
pub mod input_type;
pub mod input_version;
