/*! Recording information for cross history identifier and trace synchronisation.
*/

use serde::{Deserialize, Serialize};

use std::collections::HashMap;

use super::{
    ids::{ElementUID, SessionUID},
    reference::change::ChangeReference,
};

#[derive(Default, Debug, Serialize, Deserialize)]
pub enum HistoryEditing {
    #[default]
    None,
    Requested,
    Ongoing,
}

#[derive(Default, Debug, Serialize, Deserialize)]
pub struct Registry {
    elements: HashMap<ElementUID, Option<usize>>,
    active_sessions: Vec<SessionUID>,
    running_commands: Vec<ChangeReference>,
    editing: HistoryEditing,
    queries: usize,
}

impl Registry {
    pub fn element_id(&self, uid: ElementUID) -> Option<usize> {
        self.elements[&uid]
    }
    pub fn session_id(&self, uid: SessionUID) -> Option<usize> {
        todo!()//self.active_sessions[&uid]
    }
    /// Generate a new ElementUID
    pub fn new_uid(&mut self) -> ElementUID {
        let mut uuid = ElementUID::default();
        while self.elements.contains_key(&uuid) {
            uuid = ElementUID::default();
        }
        self.elements.insert(uuid, None);
        uuid
    }

    /*
    pub fn drop_new_element_uid(
        &mut self,
        element_uid: ElementUID,
    ) -> Result<Option<ElementUID>, ()> {
        let res = self.elements.get(&element_uid);
        // the ElementUID exists
        if let Some(uid) = res {
            // it is used in the history
            if let Some(idx) = uid {
                Err(())
            } else {
                self.elements.remove(&element_uid);
                Ok(Some(element_uid))
            }
        } else {
            Ok(None)
        }
    }

    pub fn try_set_new_element(
        &mut self,
        element_uid: ElementUID,
        idx: usize,
    ) -> Result<(), Option<usize>> {
        if self.elements.get(&element_uid).is_none() {
            self.elements.insert(element_uid, Some(idx));
            Ok(())
        } else {
            Err(self.elements[&element_uid])
        }
    }
    */
}
