use serde::{Deserialize, Serialize};

use std::collections::HashMap;

use crate::Path;
use crate::history::ids::{AttributeName, NameType, CVID, EVID};
//use crate::history::path::Path;
//use crate::interface::shared::Shared;

use super::ids::ElementUID;
use super::reference::state::StateReference;
use super::History;

/** Pointer to a particular Element's State in history.
    States always represent a state that has existed sometime in the history.
    They provide a means of pointing to a specific moment in the history of an Element.
    It's the base tool to navigate data, aside from navigating individual components by themselves.
*/
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct State {

    at: Path,

    // State identifier in the corresponding CommandVersion.
    /// The command that generated the state.
    origin: (CVID, Attachment<usize, (usize, usize)>),

    /// The ElementVersion at the root of the State
    root: EVID,
    /// Pointers to every ElementVersion making up th state.
    /// The first ElementVersion corresponds to the root of the root of the State.
    tree: StateTree,
}

impl State {
    pub fn from(state_reference: StateReference, history: &History) -> Self {
        todo!()
    }
    pub fn origin(&self) -> CVID {
        self.origin.0
    }
    pub fn tree(&self) -> &StateTree {
        &self.tree
    }
    /*<pub fn compute_tree(&mut self, history: &dyn Into<Shared<History>>) {
        todo!()
    }*/

    /// The UID of the State's View.
    /// If the State is attached, it will return the  View it got generated from.
    /// In the case of detached States, it will also move to the previous State until it joins back on an
    /// attached State, and will return that as well.
    pub fn view_uid(&self) -> Attachment<ElementUID, (ElementUID, ElementUID)> {
        todo!()
    }

    /// The UID of the State's root pointed at by the View.
    pub fn root_uid(&self) -> ElementUID {
        todo!()
    }

    /// Returns whether the State is detached or attached.
    pub fn detached(&self) -> bool {
        todo!()
    }
}
/*
impl State {
    /// creates a substate for the given Element if it is present.
    pub fn sub_state<D: Database + Default>(
        &self,
        history: &History<D>,
        path: Path,
    ) -> Result<State, ()> {
        todo!()
    }
}*/

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct StateTree {
    /// Targetted version.
    version: EVID,
    /// The version up to date children.
    /// Can be left empty to postpone the state resolution.
    children: HashMap<AttributeName, StateTree>, // ElementName
}

impl StateTree {
    pub fn version(&self) -> EVID {
        self.version
    }
    pub fn children(&self) -> &HashMap<NameType, StateTree> {
        &self.children
    }
}




#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum Attachment<T, U> {
    Attached(T),
    Detached(U),
}
