use std::ops::Deref;

use serde::{Deserialize, Serialize};

use crate::history::{ids::NameType, reference::Reference, History};

use super::{component_meta_data::ComponentMetaData, usage::Usage};

#[derive(Default, Clone, Debug, Serialize, Deserialize)]
pub struct VersionComponent {
    cp: ComponentMetaData,
    usages: Vec<Usage>,
    //sources: Vec<VID>,
    data_id: Option<Reference>,
    records: Vec<Reference>,
    checkpoint: bool,
}

impl Deref for VersionComponent {
    type Target = ComponentMetaData;

    fn deref(&self) -> &Self::Target {
        &self.cp
    }
}

impl VersionComponent {
    pub(crate) fn new(_history: History, name: NameType) -> Self {
        //let name = history.register_name(name, component_type);
        Self {
            cp: ComponentMetaData::new(name),
            usages: Default::default(),
            //sources: Default::default(),
            data_id: Default::default(),
            records: Default::default(),
            checkpoint: Default::default(),
        }
    }
}

// Getters //
impl VersionComponent {
    pub fn usages(&self) -> &Vec<Usage> {
        &self.usages
    }
    pub fn usages_mut(&mut self) -> &mut Vec<Usage> {
        &mut self.usages
    }
    pub fn data_id(&self) -> &Option<Reference> {
        &self.data_id
    }
    pub fn data_id_mut(&mut self) -> &mut Option<Reference> {
        &mut self.data_id
        // has to update potential `checkpoint` info
    }
    pub fn records(&self) -> &Vec<Reference> {
        &self.records
    }
    pub fn records_mut(&mut self) -> &mut Vec<Reference> {
        &mut self.records
    }
    pub fn checkpoint(&self) -> bool {
        self.checkpoint
    }
    pub fn checkpoint_mut(&mut self) -> &mut bool {
        &mut self.checkpoint
    }
}
