/*! Information about the CommandVersions' [Commands] that used this Version.
*/
use serde::{Deserialize, Serialize};

use crate::history::ids::{TRIPID, CVID};

/// How a Version got used by a ['Command'](crate::interface::change::command::Command).
#[derive(Default, Clone, Debug, Serialize, Deserialize)]
pub struct Usage {
    /// The ['CommandVersion'](crate::history::command::command_version::CommandVersion)s that read it
    pub parameters: Vec<(CVID, TRIPID)>,
    /// The ['CommandVersion'](crate::history::command::command_version::CommandVersion)s that changed it
    pub targets: Vec<(CVID, TRIPID)>,
    /// The ['CommandVersion'](crate::history::command::command_version::CommandVersion) that generated the [ElementVersion], and the identifier for the corresponding result TRIP link.
    pub result: (CVID, TRIPID),
}
