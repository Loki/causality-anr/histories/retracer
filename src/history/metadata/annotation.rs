/*! Component annotation.
*/

use serde::{Deserialize, Serialize};

use std::collections::HashMap;

pub type Annotations = HashMap<String, Annotation>;

/** Component annotation.
Additional information added to the component.
*/
#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub enum Annotation {
    Text(String),
    Binary(Vec<u8>),
    Map(Box<Annotations>),
}
