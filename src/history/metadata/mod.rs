/*! Additional information that can be added to any component.
*/

pub mod annotation;
pub mod tag;

use serde::{Deserialize, Serialize};

use annotation::Annotations;
use tag::Tags;

/// Additional information to describe the component.
/// 
/// # Examples
/// ```
/// let mut layer_md = Metadata::default();
/// layer_md.name_mut() = "Background_Layer".into();
/// layer_md.annotation_mut().insert("thumbnail", thumbnail.into::<Vec<u8>>());
///
/// let mut window_md = Metadata::default();
/// layer_md.tags_mut().push("UI".into());
/// layer_md.tags_mut().push("global".into());
///
/// let mut state_md = Metadata::default();
/// state_md.tags_mut().push("bookmarked".into());
/// state_md.tags_mut().push("rating:5".into());

/// ```
#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct Metadata {
    /// A given name, that can be used instead of the component's identifying id or name for
    /// display purposes.
    name: String,
    /// Descriptions of traits to be used as filters.
    tags: Tags,
    /// Miscellaneous data to be linked to the component.
    annotations: Annotations,
}

impl Metadata {
    pub fn name(&self) -> &String {
        &self.name
    }
    pub fn name_mut(&mut self) -> &mut String {
        &mut self.name
    }
    pub fn tags(&self) -> &Tags {
        &self.tags
    }
    pub fn tags_mut(&mut self) -> &mut Tags {
        &mut self.tags
    }
    pub fn annotations(&self) -> &Annotations {
        &self.annotations
    }
    pub fn annotations_mut(&mut self) -> &mut Annotations {
        &mut self.annotations
    }
}

