use serde::{Deserialize, Serialize};

/** Component tag.
Information that can be used for filtering and searching components.
# Examples
```rust
"favorite"         // simple tag
"brush/watercolor" // subtags
"status:Active"    // state
```
*/

pub type Tags = Vec<Tag>;

#[derive(Clone, Debug, Default, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct Tag(String);
