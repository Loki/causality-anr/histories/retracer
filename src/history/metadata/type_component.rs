/*! Type component data.
Adds inheritance information and generic component information.
*/
use serde::{Deserialize, Serialize};

use std::ops::{Deref, DerefMut};

use crate::history::ids::TypeName;

use super::component_meta_data::ComponentMetaData;

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct TypeComponent {
    cp: ComponentMetaData,
    inherits: Vec<TypeName>,
}

impl Deref for TypeComponent {
    type Target = ComponentMetaData;

    fn deref(&self) -> &Self::Target {
        &self.cp
    }
}

impl DerefMut for TypeComponent {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.cp
    }
}

impl TypeComponent {
    pub fn new(name: String) -> Self {
        Self {
            cp: ComponentMetaData::new(name),
            inherits: Default::default(),
        }
    }
    pub fn inherits(&self) -> &Vec<TypeName> {
        &self.inherits
    }
    pub fn inherits_mut(&mut self) -> &mut Vec<TypeName> {
        &mut self.inherits
    }
}
