use serde::{Deserialize, Serialize};
use std::ops::{Index, IndexMut};

use crate::history::ids::{IDType, SessionUID};

use super::super::command::command_version::CommandVersion;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Session {
    uid: SessionUID,
    //view: ElementUID,
    //initial_state: Option<State>,
    command_versions: Vec<CommandVersion>,
    // Associates to a CV in the commands vector the resulting computed State
    //states_cache: HashMap<usize, State>,
}

impl Session {
    pub(crate) fn new(uid: SessionUID) -> Self {
        //todo: check for uniqueness in the history's  SessionUIDs and ElementUIDs
        Self {
            uid,
            //initial_state: start,
            command_versions: Default::default(),
            //states_cache: Default::default(),
        }
    }
    pub fn uid(&self) -> &SessionUID {
        &self.uid
    }

    pub fn command_versions(&self) -> &Vec<CommandVersion> {
        self.command_versions.as_ref()
    }

    pub(crate) fn command_versions_mut(&mut self) -> &mut Vec<CommandVersion> {
        &mut self.command_versions
    }
}

impl Index<IDType> for Session {
    type Output = CommandVersion;

    fn index(&self, index: IDType) -> &Self::Output {
        &self.command_versions[index]
    }
}

impl IndexMut<IDType> for Session {
    fn index_mut(&mut self, index: IDType) -> &mut Self::Output {
        &mut self.command_versions[index]
    }
}
