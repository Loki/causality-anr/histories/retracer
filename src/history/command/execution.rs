use std::{fmt::Debug, collections::HashMap};

use serde::{Deserialize, Serialize};

use crate::{
    history::{
        common::component_meta_data::ComponentMetaData,
        ids::{AttributeName, IVID, VID, TRIPID, CVID, ArgumentName},
        state::State, reference::{Reference, state::StateReference},
    },
    //interface::change::region::Region,
    Path,
};

use super::arguments::Argument;

/// Trace of the execution of a ['Command'](crate::interface::change::command::Command).
///
/// The timestamp and author, along the generated changes are recorded.
#[derive(Default, Clone, Debug, Serialize, Deserialize)]
pub struct Execution {
    /// The root of the \View the \Command is run at
    // how was that deleted again? : at: Path,
    /// Current state of the execution
    execution_data: ExecutionData,
    /// Versions
    trip: Vec<TRIPLink>,
    /// Result links grouped by created States. The View follows them and ends at the last state
    /// after execution.
    view_states: Vec<(Vec<usize>, ComponentMetaData)>,
    /// Result links grouped by created detached States. The State they start on is referenced,
    /// followed by the created states
    detached_states: Vec<(StateReference, Vec<(Vec<usize>, ComponentMetaData)>)>,
    inputs: Vec<(usize, Option<ComponentMetaData>)>,
    change_meta_data: Option<ComponentMetaData>,
    // Versions consumed by the command.
    // Versions created by the command. (The results are separated into several groups, the first one impacting the View, and the others being silent)
    // Input data versions created by the command.
    // Versions read by the command.
}

#[derive(Default, Clone, Debug, Serialize, Deserialize)]
pub struct ExecutionData {
    argument_paradoxes: HashMap<ArgumentName, Paradox<ArgumentName>>,
    paradoxes: HashMap<TRIPID, Paradox<(CVID, TRIPID)>>,
    overrides: HashMap<TRIPID, Argument<Reference>>,
}

impl Execution {
    pub fn trip(&self) -> &Vec<TRIPLink> {
        &self.trip
    }
    pub fn trip_mut(&mut self) -> &mut Vec<TRIPLink> {
        &mut self.trip
    }
    pub fn states(&self) -> &Vec<Vec<&TRIPResult>> {
        todo!()//&self.states
    }
    pub fn states_mut(&mut self) -> &mut Vec<Vec<&mut TRIPResult>> {
        todo!()//&mut self.states
    }
    pub fn inputs(&self) -> &Vec<&TRIPInput> {
        todo!()//&self.inputs
    }
    pub fn inputs_mut(&mut self) -> &mut Vec<&mut TRIPInput> {
        todo!()//&mut self.inputs
    }
}

impl Execution {
    pub(crate) fn new() -> Self {
        Self {
            ..Default::default()
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum TRIPLink {
    Target(TRIPTarget),
    Result(TRIPResult),
    Parameter(TRIPParameter),
    Input(TRIPInput),
    FailedTarget(TRIPFailedTarget),
    FailedParameter(TRIPFailedParameter),
}
// Information about a used Version.
//
// Links to the Version and the argument it came from if any.

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct TRIPTarget {
    /// The argument name or the Path the target was part from
    pub origin: Path,
    /// The used version
    pub version: VID,
    pub state: Option<State>,
    //<pub region: Box<dyn Region>,

    pub results: Vec<TRIPID>,
    pub mark: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct TRIPResult {
    //pub origin: Option<ArgumentName>,
    /// The used version
    pub version: VID,
    pub attributes_state: Vec<(AttributeName, State)>,
    //<pub region: Box<dyn Region>,

    pub targets: Vec<TRIPID>,
    pub sources: Vec<TRIPID>,
    pub mark: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct TRIPParameter {
    /// The argument name or the Path the target was part from
    pub origin: Path,
    /// The used version
    pub version: VID,
    pub state: Option<State>,
    //<pub region: Box<dyn Region>,
    pub mark: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct TRIPInput {
    pub origin: Option<Path>,
    /// The used version
    pub version: IVID,
    /// The argument name the version was part of, if any
    pub mark: String,
}


#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct TRIPFailedTarget {
    /// The argument name or the Path the target was part from
    pub origin: Path,
    //<pub region: Box<dyn Region>,
    pub mark: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct TRIPFailedParameter {
    /// The argument name or the Path the target was part from
    pub origin: Path,
    //<pub region: Box<dyn Region>,
    pub mark: String,
}


#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Paradox<T> {
    pub initial: T,
    pub inconsistent: Option<Path>,
    pub candidates: Vec<Argument<Reference>>,
}
