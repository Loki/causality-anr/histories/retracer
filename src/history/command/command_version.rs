/*! [Command] versions, marking their execution.
[Command]: crate::interface::change::command::Command
*/

//< use std::collections::HashMap;

use std::ops::{Deref, DerefMut};

use serde::{Deserialize, Serialize};

use crate::{history::{
    common::version_component::VersionComponent,
    ids::{CommandTypeName, IDType, Timestamp, CVID},
    reference::Reference,
}, Path};
//< use crate::interface::change::command::solver::Solver;

use super::{arguments::Arguments, execution::Execution};

/// The trace of a [Command] execution.
///
/// [CommandVersions]s are the representation of the state of a Command instance at the time of its execution.
/// They keep track of the execution sequence (the caller: [CommandVersion.parent]; and the [Command]s it called itself: [CommandVersion.children]),
/// the arguments it was given as well as the consequence of the execution on the history ([CommandVersion.execution]).
/// They are also given their own name, tags and annotations. These are mutable and not versioned.
/// Everything relating to the corresponding [Command] execution is versioned and thus immutable.
/// [Command]: crate::interface::change::command::Command
/// A new [CommandVersion] has to be created to represent a new [Command] execution.
#[derive(Clone, Default, Debug, Serialize, Deserialize)]
pub struct CommandVersion {
    command_type: CommandTypeName,

    session: IDType,

    /// Time of execution
    timestamp: Timestamp,

    /// Arguments used at execution
    arguments: Box<Arguments<Reference>>,

    /// Whether the View was narrowed down to a substate at execution
    sub_state: Option<Path>,

    /// Actual versions used at execution
    execution: Execution,

    /// When the execution has terminated, whether it succeeded or failed
    succeeded: Option<bool>,

    /// Command that executed this one, if any
    parent: Option<CVID>,
    /// Commands that got executed by this one, if any
    children: Vec<CVID>,

    /// Component data
    cp: VersionComponent,
}

impl Deref for CommandVersion {
    type Target = VersionComponent;

    fn deref(&self) -> &Self::Target {
        &self.cp
    }
}

impl DerefMut for CommandVersion {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.cp
    }
}


impl CommandVersion {
    pub fn new(command_type: CommandTypeName, session: IDType, timestamp: Timestamp) -> Self {
        Self {
            command_type,
            session,
            timestamp,
            ..Default::default()
        }
    }
    pub fn command_type(&self) -> &CommandTypeName {
        &self.command_type
    }
    pub fn command_type_mut(&mut self) -> &mut CommandTypeName {
        &mut self.command_type
    }
    pub fn arguments(&self) -> &Arguments<Reference> {
        &self.arguments
    }
    pub fn arguments_mut(&mut self) -> &mut Arguments<Reference> {
        &mut self.arguments
    }
    pub fn execution(&self) -> &Execution {
        &self.execution
    }
    pub fn execution_mut(&mut self) -> &mut Execution {
        &mut self.execution
    }
    pub fn parent(&self) -> Option<CVID> {
        self.parent
    }
    pub fn parent_mut(&mut self) -> &mut Option<CVID> {
        &mut self.parent
    }
    pub fn children(&self) -> &Vec<CVID> {
        &self.children
    }
    pub fn children_mut(&mut self) -> &mut Vec<CVID> {
        &mut self.children
    }

    pub fn session(&self) -> IDType {
        self.session
    }

    pub fn session_mut(&mut self) -> &mut IDType {
        &mut self.session
    }
    pub fn timestamp(&self) -> &Timestamp {
        &self.timestamp
    }
    pub fn timestamp_mut(&mut self) -> &mut Timestamp {
        &mut self.timestamp
    }
}



impl PartialOrd for CommandVersion {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.timestamp().partial_cmp(&other.timestamp())
    }
}

impl PartialEq for CommandVersion {
    fn eq(&self, other: &Self) -> bool {
        self.timestamp().eq(&other.timestamp())
    }
}

impl Eq for CommandVersion {}


impl Ord for CommandVersion {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.timestamp().cmp(other.timestamp())
    }
}
