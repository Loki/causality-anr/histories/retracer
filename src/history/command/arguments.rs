/*! Recorded [Command] arguments.
  Each recorded Command will have its corresponding CommandVersion with the arguments it used at execution.
[Command]: crate::interface::change::command::Command
*/

use std::{
    collections::HashMap,
    fmt::Debug,
    ops::{Deref, DerefMut},
};

use serde::{Deserialize, Serialize};

use crate::{
    history::{
        common::{annotation::Annotations, tag::Tags},
        ids::{ArgumentName, ElementUID, NameType, SessionUID, TypeName},
        reference::change::ChangeReference,
        reference::Reference,
    },
    Path,
};

/*
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Updater {
    arguments: ArgumentsMap<Input<D>>,
    updater_type: UpdaterType,
    // fn( updater_arguments: &ArgumentsMap, command_arguments: &mut Arguments,) -> Result<(), Vec<ArgumentName>>,
}*/

/// [Command] Arguments, with update information.
/// Note: Updater Arguments have no ArgumentName in common with the main ones (a prefix can be used to prevent any collision for instance).
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Arguments<I> {
    /// If these Arguments come from a previous execution, the Change they originated from
    from: Option<ChangeReference>,
    arguments: ArgumentsMap<I>,
    // Whether the Arguments should be updated according to other Arguments.
    // An example can be the `layer: StateReference` argument of a `DrawStroke` [Command]. Updating it normaly
    // will use a StateReference currently matching the given Path (for more information, see the [Dynamic] marker).
    // To update according to something else, like the current selected layer, Updater will be used.
    //updater: Option<Updater>, //todo: do they have inputs? And how to manage them?
}

impl<I> Default for Arguments<I> {
    fn default() -> Self {
        Self {
            from: None,
            arguments: ArgumentsMap::default()
        }
    }
}

impl<I> Arguments<I> {
    pub fn new() -> Self {
        Default::default()
    }
    pub fn from(change: ChangeReference) -> Self {
        Self {
            from: Some(change),
            ..Default::default()
        }
    }

    pub fn paradoxical_arguments() -> Vec<ArgumentName> {
        todo!()
    }
    /// Resets the paradoxal arguments to their original value
    pub fn reset_paradoxical_arguments() {
        todo!()
    }

    /// Returns a list of input argument names that haven't yet been registered
    fn unregistered_inputs() -> Vec<ArgumentName> {
        todo!()
    }
}

impl<I> Deref for Arguments<I> {
    type Target = ArgumentsMap<I>;

    fn deref(&self) -> &Self::Target {
        &self.arguments
    }
}

impl<I> DerefMut for Arguments<I> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.arguments
    }
}

// Option for Argument because of decoupling needs when deleting Changes from the history
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct ArgumentsMap<I> (HashMap<ArgumentName, (Option<Argument<I>>, Option<Path>)>);

impl<I> Default for ArgumentsMap<I> {
    fn default() -> Self {
        Self(HashMap::default())
    }
}

//? type Resolve = bool;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum Argument<I> {
    Reference(Reference),
    Type(TypeName),
    Element(ElementUID),
    Session(SessionUID),
    Input(I),
    Path(Path),
    Name(NameType),
    Tags(Tags),
    Annotations(Annotations),
}
