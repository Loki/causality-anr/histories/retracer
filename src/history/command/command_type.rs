/*! [Command] type structure.
[Command]: crate::interface::change::command::Command
*/
use serde::{Deserialize, Serialize};

use std::{
    collections::HashMap,
    ops::{Deref, DerefMut},
};

use crate::history::{
    common::type_component::TypeComponent,
    ids::{ArgumentName, CommandTypeName, TypeName},
};

/** A description of a [Command] type.

It saves information about the [Command]'s [Argument](super::arguments::Argument) types.
It also saves [Command] type inheritance data.
*/
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct CommandType {
    /// The command's argument types
    arguments: HashMap<ArgumentName, TypeName>,

    cp: TypeComponent,
}

impl Deref for CommandType {
    type Target = TypeComponent;

    fn deref(&self) -> &Self::Target {
        &self.cp
    }
}

impl DerefMut for CommandType {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.cp
    }
}

impl CommandType {
    pub(crate) fn new(name: CommandTypeName) -> Self {
        Self {
            cp: TypeComponent::new(name),
            arguments: Default::default(),
        }
    }

    pub fn arguments(&self) -> &HashMap<ArgumentName, TypeName> {
        &self.arguments
    }
    pub(crate) fn argument_mut(&mut self) -> &mut HashMap<ArgumentName, TypeName> {
        &mut self.arguments
    }
}
