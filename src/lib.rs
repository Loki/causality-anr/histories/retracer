/*! The ReTracer command history library.

This library follows the [ReTracer command history architecture]() to implement the [ESCI history model]() which is an evolution of the [Causality](https://hal.inria.fr/hal-01523298) model.
It provides a persistent and serializable history trace and means to navigate through it and reuse its data in various ways.
Common usage examples are sequential and selective Undo-Redo, Repeat, past state consultation, macro creation, CAD history style editing, collaborative editing both real time and asynchronously, exploration, mistake fixing, sharing and improving one's workflow and more.
It also provides means to manage potential [paradoxes] and [conflicts].

# Crate organisation

There are two main modules to the crate:
- the history module is the internal side, containing the actual history trace.
    Associated to it are two modules:
    - the database module saves versions of the the edited data.
    - the serialization module provides means of saving the history trace.
- the interface module is the external side, containing an API to read and build the history.
    Associated to it is one module:
    - the modules module contains predefined Commands and other tools readily usable.

Finally, it is possible to edit the history trace with the control module, and thus add, change and remove data if needed.

To use the history, interface is the main module to check out.
To learn about its inner workings, you can refer to the history module.

# Background

The [Causality command history model]() integrates various features described in the literature in an inter-compatible way, allowing them to co-exist and be combinable.
The [ESCI model]() delves further into the details of how such a structure could work, and the [ReTracer architecture]() presents a way to implement it.
This library is an implementation of this architecture in Rust.

# Pros and cons

This is an advanced history model, which doesn't mean that it's the best solution for every project.
This library focuses on functionnality and flexibility, providing a trace that allows for a large variety of operations, as mentionned at the start of this introduction.
The cost for it is added complexity at Command execution to record the information needed for these operations, and more data to be managed.
So if your main concern is to have a light and simple command history system, this may not be what you need.
However, if you want a powerful history system, or want to experiment with various history operations to create your own, this project could be useful to you!
*/

//pub(crate) mod daily;
/// History editing
//<pub mod control;
/// History database
//pub mod database;
/// Factories used by the Interface
//pub mod factory;
//pub mod fuses;
/// History trace
pub(crate) mod history;
/// History API
//pub mod interface;
/// Predefined Commands and utilities
//<pub mod modules;
/// History serialization
pub mod serialization;
pub use history::path::Path;
