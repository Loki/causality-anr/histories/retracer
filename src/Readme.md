The library contains two main modules:
- history: Core components forming the architecture of the history (ET, IT, CT, E, EV, CV, IV, …).
- interface: Everything API and usage related in general (Command, Paradox, Change, CommandFactory, iterators, queries, …)

