
use std::ops::Index;

use serde::{Serialize, Deserialize};
use getset::{Getters, MutGetters};

use crate::{identifiers::{idxs::{IDXType, ETIDX, EVIDX}, uids::ElementUID}, record::change::ChangeRecord, register::Register};

use super::{element_version::ElementVersion, meta_data::Metadata};

#[derive(Clone, Debug, Default, PartialEq, Eq, Hash, Getters, MutGetters, Serialize, Deserialize)]
#[getset(get_copy = "pub", get = "pub", get_mut = "pub")]
pub struct Element {
    uid: ElementUID,
    element_type: ETIDX,
    copies: Vec<EVIDX>,
    versions: Vec<ElementVersion>,
    metadata: Metadata,
}

impl Index<IDXType> for Element {
    type Output = ElementVersion;

    fn index(&self, index: IDXType) -> &Self::Output {
        &self.versions[index]
    }
}

impl Element {
    pub fn new(element_type: ETIDX, uid: ElementUID) -> Self {
        Self {
            uid,
            element_type,
            ..Default::default()
        }
    }
}
