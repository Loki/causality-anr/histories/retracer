use std::hash::Hash;

use serde::{Deserialize, Serialize};
use getset::{Getters, MutGetters};
use self::{annotation::Annotations, tag::Tags};

pub mod annotation;
pub mod tag;

#[derive(Clone, Debug, Default, PartialEq, Eq, Getters, MutGetters, Serialize, Deserialize)]
#[getset(get_copy = "pub", get_mut = "pub")]
pub struct Metadata {
    name: String,
    tags: Tags,
    annotations: Annotations,
}

impl Hash for Metadata {
    fn hash<H: std::hash::Hasher>(&self, _state: &mut H) {}
}
