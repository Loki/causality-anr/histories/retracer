use std::ops::{Deref, DerefMut};

use serde::{Deserialize, Serialize};

pub type Tags = Vec<Tag>;

#[derive(Clone, Debug, PartialEq, Eq, Default, Serialize, Deserialize)]
pub struct Tag(String);

impl Deref for Tag {
    type Target = String;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for Tag {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
