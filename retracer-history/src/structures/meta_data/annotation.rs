use std::collections::HashMap;

use serde::{Deserialize, Serialize};


pub type Annotations = HashMap<String, Annotation>;

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub enum Annotation {
    Text(String),
    Binary(Vec<u8>),
    Map(Box<Annotations>),
}
