use std::{collections::HashMap, hash::Hash};

use getset::{Getters, MutGetters};
use itertools::Itertools;
use serde::{Serialize, Deserialize};

use crate::identifiers::{references::{Reference, change::ChangeReference}, uids::{TypeName, ElementUID, SessionUID, ArgumentName}};

#[derive(Clone, Debug, Default, PartialEq, Eq, Getters, MutGetters, Serialize, Deserialize)]
#[getset(get_copy = "pub", get = "pub", get_mut = "pub")]
pub struct Arguments {
    from: Option<ChangeReference>,
    arguments: HashMap<ArgumentName, Option<Argument>>,
}

impl Hash for Arguments {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.from.hash(state);
        self.arguments.iter().sorted().collect::<Vec<(&ArgumentName, &Option<Argument>)>>().hash(state);
    }
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub enum Argument {
    Reference(Reference),
    Type(TypeName),
    Element(ElementUID),
    Session(SessionUID),
}
