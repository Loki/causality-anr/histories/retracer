
use serde::{Serialize, Deserialize};
use getset::{Getters, MutGetters};

use crate::identifiers::idxs::ITIDX;

use super::{trip::usage::Usage, meta_data::Metadata};

#[derive(Clone, Debug, Default, PartialEq, Eq, Hash, Getters, MutGetters, Serialize, Deserialize)]
#[getset(get_copy = "pub", get = "pub", get_mut = "pub")]
pub struct InputVersion {
    input_type: ITIDX,

    usages: Vec<Usage>,
    metadata: Metadata,
}
