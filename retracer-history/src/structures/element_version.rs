
use std::{collections::HashMap, hash::Hash};
use getset::{Getters, MutGetters};

use itertools::Itertools;
use serde::{Serialize, Deserialize};

use crate::identifiers::{idxs::{EIDX, EVIDX}, uids::AttributeName};

use super::{meta_data::Metadata, trip::usage::Usage};
#[derive(Clone, Debug, Default, PartialEq, Eq, Getters, MutGetters, Serialize, Deserialize)]
#[getset(get_copy = "pub", get = "pub", get_mut = "pub")]
pub struct ElementVersion {
    element: EIDX,
    parent: Option<EVIDX>,
    children: HashMap<AttributeName, EVIDX>,
    usages: Vec<Usage>,
    metadata: Metadata,
}

impl Hash for ElementVersion {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.element.hash(state);
        self.parent.hash(state);
        self.children.iter().sorted().collect::<Vec<(&AttributeName, &EVIDX)>>().hash(state);
        self.usages.hash(state);
        self.metadata.hash(state);
    }
}


impl ElementVersion {
    pub fn new(element: EIDX) -> Self {
        Self {
            element,
            ..Default::default()
        }
    }
}
