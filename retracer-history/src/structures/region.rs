use std::fmt::Debug;
use dyn_clone::DynClone;

#[typetag::serde(tag = "type")]
pub trait Region: ToString+Debug+DynClone {}
