use getset::{Getters, MutGetters};
use serde::{Deserialize, Serialize};

use crate::{
    identifiers::{
        idxs::{CTIDX, CVIDX, SIDX},
        timestamp::Timestamp,
    },
    path::Path,
};

use super::{arguments::Arguments, execution::Execution, meta_data::Metadata, trip::usage::Usage};
#[derive(
    Clone, Debug, Default, PartialEq, Eq, Hash, Getters, MutGetters, Serialize, Deserialize,
)]
#[getset(get_copy = "pub", get = "pub", get_mut = "pub")]
pub struct CommandVersion {
    command_type: CTIDX,
    session: SIDX,

    timestamp: Timestamp,
    arguments: Box<Arguments>,
    sub_state: Option<Path>,
    execution: Execution,

    succeeded: Option<bool>,

    parent: Option<CVIDX>,
    //children: Option<CVIDX>,
    usages: Vec<Usage>,
    metadata: Metadata,
}
