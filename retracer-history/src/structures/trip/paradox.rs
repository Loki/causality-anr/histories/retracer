use getset::{Getters, MutGetters};
use serde::{Deserialize, Serialize};

use crate::{path::Path, structures::arguments::Argument};

#[derive(
    Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Getters, MutGetters, Serialize, Deserialize,
)]
#[getset(get_copy = "pub", get = "pub", get_mut = "pub")]
pub struct Paradox<T> {
    initial: T,
    inconsistent: Option<Path>,
    candidates: Vec<Argument>,
}
