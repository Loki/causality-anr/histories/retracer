pub mod paradox;
pub mod usage;

use std::{fmt::Debug, hash::Hash};

use getset::{Getters, MutGetters};
use serde::{Deserialize, Serialize};

use crate::{
    identifiers::{
        idxs::{IDXType, StIDX, IVIDX, TRIPIDX, VIDX},
        uids::AttributeName,
    },
    path::{Path, StatePath},
};

use super::region::Region;
#[derive(Clone, Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum TRIPLink {
    Target(TRIPTarget),
    Result(TRIPResult),
    Parameter(TRIPParameter),
    Input(TRIPInput),
    FailedTarget(TRIPFailedTarget),
    FailedParameter(TRIPFailedParameter),
    // Done with Result already?
    // Execution of a child Command
    // Holds the index of the CV
    //SubChange(IDXType),
}
// Information about a used Version.
//
// Links to the Version and the argument it came from if any.

#[derive(Debug, Getters, MutGetters, Serialize, Deserialize)]
#[getset(get_copy = "pub", get = "pub", get_mut = "pub")]
pub struct TRIPTarget {
    /// The argument name or the Path the target was at
    origin: Path,
    /// The used version
    version: VIDX,
    state: Option<StIDX>,
    region: Box<dyn Region>,

    results: Vec<TRIPIDX>,
    mark: String,
}

#[derive(Debug, Getters, MutGetters, Serialize, Deserialize)]
#[getset(get_copy = "pub", get = "pub", get_mut = "pub")]
pub struct TRIPResult {
    /// The used version
    version: VIDX,
    //TODO: propagate `at`
    // the position of the result in the State, mirroring the `origin` of Targets
    state_path: Option<StatePath>,
    attributes_state: Vec<(AttributeName, StIDX)>,
    region: Box<dyn Region>,

    targets: Vec<TRIPIDX>,
    sources: Vec<TRIPIDX>,
    mark: String,
}

#[derive(Debug, Getters, MutGetters, Serialize, Deserialize)]
#[getset(get_copy = "pub", get = "pub", get_mut = "pub")]
pub struct TRIPParameter {
    /// The argument name or the Path the target was part from
    origin: Path,
    /// The used version
    version: VIDX,
    state: Option<StIDX>,
    region: Box<dyn Region>,
    mark: String,
}

#[derive(Clone, Debug, PartialEq, Eq, Hash, Getters, MutGetters, Serialize, Deserialize)]
#[getset(get_copy = "pub", get = "pub", get_mut = "pub")]
pub struct TRIPInput {
    origin: Option<Path>,
    /// The used version
    version: IVIDX,
    /// The argument name the version was part of, if any
    mark: String,
}

#[derive(Debug, Getters, MutGetters, Serialize, Deserialize)]
#[getset(get_copy = "pub", get = "pub", get_mut = "pub")]
pub struct TRIPFailedTarget {
    /// The argument name or the Path the target was part from
    origin: Path,
    region: Box<dyn Region>,
    mark: String,
}

#[derive(Debug, Getters, MutGetters, Serialize, Deserialize)]
#[getset(get_copy = "pub", get = "pub", get_mut = "pub")]
pub struct TRIPFailedParameter {
    /// The argument name or the Path the target was part from
    origin: Path,
    region: Box<dyn Region>,
    mark: String,
}

impl Clone for TRIPTarget {
    fn clone(&self) -> Self {
        Self {
            origin: self.origin.clone(),
            version: self.version.clone(),
            state: self.state.clone(),
            region: dyn_clone::clone_box(&*self.region),
            results: self.results.clone(),
            mark: self.mark.clone(),
        }
    }
}

impl Hash for TRIPTarget {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.origin.hash(state);
        self.version.hash(state);
        self.state.hash(state);
        self.region.to_string().hash(state);
        self.results.hash(state);
        self.mark.hash(state);
    }
}

impl PartialEq for TRIPTarget {
    fn eq(&self, other: &Self) -> bool {
        self.origin.eq(&other.origin)
            && self.version.eq(&other.version)
            && self.state.eq(&other.state)
            && self.region.to_string().eq(&other.region.to_string())
            && self.results.eq(&other.results)
            && self.mark.eq(&other.mark)
    }
}

impl Eq for TRIPTarget {}

impl Clone for TRIPResult {
    fn clone(&self) -> Self {
        Self {
            at: self.at.clone(),
            version: self.version.clone(),
            attributes_state: self.attributes_state.clone(),
            region: dyn_clone::clone_box(&*self.region),
            targets: self.targets.clone(),
            sources: self.sources.clone(),
            mark: self.mark.clone(),
        }
    }
}

impl Hash for TRIPResult {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.version.hash(state);
        self.attributes_state.hash(state);
        self.region.to_string().hash(state);
        self.targets.hash(state);
        self.sources.hash(state);
        self.mark.hash(state);
    }
}

impl PartialEq for TRIPResult {
    fn eq(&self, other: &Self) -> bool {
        self.version.eq(&other.version)
            && self.attributes_state.eq(&other.attributes_state)
            && self.region.to_string().eq(&other.region.to_string())
            && self.targets.eq(&other.targets)
            && self.sources.eq(&other.sources)
            && self.mark.eq(&other.mark)
    }
}

impl Eq for TRIPResult {}

impl Clone for TRIPParameter {
    fn clone(&self) -> Self {
        Self {
            origin: self.origin.clone(),
            version: self.version.clone(),
            state: self.state.clone(),
            region: dyn_clone::clone_box(&*self.region),
            mark: self.mark.clone(),
        }
    }
}

impl Hash for TRIPParameter {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.origin.hash(state);
        self.version.hash(state);
        self.state.hash(state);
        self.region.to_string().hash(state);
        self.mark.hash(state);
    }
}

impl PartialEq for TRIPParameter {
    fn eq(&self, other: &Self) -> bool {
        self.origin.eq(&other.origin)
            && self.version.eq(&other.version)
            && self.state.eq(&other.state)
            && self.region.to_string().eq(&other.region.to_string())
            && self.mark.eq(&other.mark)
    }
}

impl Eq for TRIPParameter {}

impl Clone for TRIPFailedTarget {
    fn clone(&self) -> Self {
        Self {
            origin: self.origin.clone(),
            region: dyn_clone::clone_box(&*self.region),
            mark: self.mark.clone(),
        }
    }
}

impl Hash for TRIPFailedTarget {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.origin.hash(state);
        self.region.to_string().hash(state);
        self.mark.hash(state);
    }
}

impl PartialEq for TRIPFailedTarget {
    fn eq(&self, other: &Self) -> bool {
        self.origin.eq(&other.origin)
            && self.region.to_string().eq(&other.region.to_string())
            && self.mark.eq(&other.mark)
    }
}

impl Eq for TRIPFailedTarget {}

impl Clone for TRIPFailedParameter {
    fn clone(&self) -> Self {
        Self {
            origin: self.origin.clone(),
            region: dyn_clone::clone_box(&*self.region),
            mark: self.mark.clone(),
        }
    }
}

impl Hash for TRIPFailedParameter {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.origin.hash(state);
        self.region.to_string().hash(state);
        self.mark.hash(state);
    }
}

impl PartialEq for TRIPFailedParameter {
    fn eq(&self, other: &Self) -> bool {
        self.origin.eq(&other.origin)
            && self.region.to_string().eq(&other.region.to_string())
            && self.mark.eq(&other.mark)
    }
}

impl Eq for TRIPFailedParameter {}
