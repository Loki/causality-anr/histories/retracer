use std::{collections::HashMap, hash::Hash};

use getset::{Getters, MutGetters};
use itertools::Itertools;
use serde::{Deserialize, Serialize};

use crate::{
    identifiers::{
        idxs::{StIDX, CVIDX, TRIPIDX},
        references::state::StateTreeNodeBuilder,
        uids::ArgumentName,
    },
    path::StatePath,
};

use super::{
    arguments::Argument,
    meta_data::Metadata,
    trip::{paradox::Paradox, TRIPLink},
};

//CHECK: coherence
#[derive(Clone, Debug, Default, PartialEq, Eq, Getters, MutGetters, Serialize, Deserialize)]
#[getset(get_copy = "pub", get = "pub", get_mut = "pub")]
pub struct Execution {
    /// The root of the \View the \Command is run at
    // how was that deleted again? : at: Path,
    /// Current state of the execution
    argument_paradoxes: HashMap<ArgumentName, Paradox<ArgumentName>>,
    paradoxes: HashMap<TRIPIDX, Paradox<(CVIDX, TRIPIDX)>>,
    overrides: HashMap<TRIPIDX, Argument>,
    trip: Vec<TRIPLink>,
    /// Branch that moves the View
    /// The branching is always relative (TODO: force it through typing?)
    attached: Branch,
    /// detached Branches
    detached: Vec<Branch>,
    inputs: Vec<(TRIPIDX, Option<Metadata>)>,
    change_meta_data: Option<Metadata>,
    // Versions consumed by the command.
    // Versions created by the command. (The results are separated into several groups, the first one impacting the View, and the others being silent)
    // Input data versions created by the command.
    // Versions read by the command.
}

impl Hash for Execution {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.argument_paradoxes
            .iter()
            .sorted()
            .collect::<Vec<(&ArgumentName, &Paradox<ArgumentName>)>>()
            .hash(state);
        self.paradoxes
            .iter()
            .sorted()
            .collect::<Vec<(&TRIPIDX, &Paradox<(CVIDX, TRIPIDX)>)>>()
            .hash(state);
        self.overrides
            .iter()
            .sorted()
            .collect::<Vec<(&TRIPIDX, &Argument)>>()
            .hash(state);
        self.trip.hash(state);
        self.attached.hash(state);
        self.detached.hash(state);
        self.inputs.hash(state);
        self.change_meta_data.hash(state);
    }
}

#[derive(Clone, Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
enum Branching {
    Relative(Option<StatePath>),
    Absolute(StIDX),
}

impl Default for Branching {
    fn default() -> Self {
        Branching::Relative(None)
    }
}

#[derive(
    Clone, Debug, Default, PartialEq, Eq, Hash, Getters, MutGetters, Serialize, Deserialize,
)]
#[getset(get_copy = "pub", get = "pub", get_mut = "pub")]
pub(crate) struct Branch {
    branching: Branching,
    states: Vec<StateDiff>,
}

#[derive(
    Clone, Debug, Default, PartialEq, Eq, Hash, Getters, MutGetters, Serialize, Deserialize,
)]
#[getset(get_copy = "pub", get = "pub", get_mut = "pub")]
pub(crate) struct StateDiff {
    diffs: Vec<TRIPIDX>,
    state_meta_data: Option<Metadata>,
}

impl StateDiff {
    // TODO: Manage regions?
    pub fn nodes_before(&self, trip: &Vec<TRIPLink>) -> Vec<StateTreeNodeBuilder> {
        self.diffs.iter().fold(HashMap::new(), |h, tidx| {
            let link = trip[tidx];
            match link {
                TRIPLink::Target(t) => t.,
                TRIPLink::Result(r) => ,
                _ => (),
            };
        })
    }
    pub fn nodes_after(&self, trip: &Vec<TRIPLink>) -> Vec<StateTreeNodeBuilder> {}
}

/*
/// Detached State Branch
/// Only the first Branch, if attached to the View, will make the View advance
#[derive(
    Clone, Debug, Default, PartialEq, Eq, Hash, Getters, MutGetters, Serialize, Deserialize,
)]
#[getset(get_copy = "pub", get = "pub", get_mut = "pub")]
struct DetachedBranch {
    branching: StIDX,
    //TODO: not necessary? (Moving management would move to RTItf)
    // Whether [branching] tracks the View State or is the branch View State.
    // If set to None, no moving of the View is allowed, and [branching] is taken as the View State.
    // If an AttributeName is given, the View State will be set to the sub State at the given AttributeName and will thus be movable, the moves being recorded by the [branching] root Element.
    //tracking: Option<AttributeName>,
    states: Vec<(Vec<TRIPIDX>, Option<Metadata>)>,
}

/// Attached branches are always tracking.
#[derive(
    Clone, Debug, Default, PartialEq, Eq, Hash, Getters, MutGetters, Serialize, Deserialize,
)]
#[getset(get_copy = "pub", get = "pub", get_mut = "pub")]
struct AttachedBranch {
    //sub_state: Option<(StatePath, Option<AttributeName>)>,
    sub_state: Option<StatePath>,
    states: Vec<(Vec<TRIPIDX>, Option<Metadata>)>,
}
*/
