use getset::{Getters, MutGetters};
use itertools::Itertools;
use std::{collections::HashMap, hash::Hash, ops::Index};

use serde::{Deserialize, Serialize};

use crate::{
    identifiers::{
        idxs::{IDXType, EIDX, SIDX},
        references::change::ChangeReference,
        uids::SessionUID,
    },
    path::StatePath,
};

use super::{command_version::CommandVersion, meta_data::Metadata};
#[derive(
    Clone, Debug, Default, PartialEq, Eq, Hash, Getters, MutGetters, Serialize, Deserialize,
)]
#[getset(get_copy = "pub", get = "pub", get_mut = "pub")]
pub struct Session {
    uid: SessionUID,
    view: EIDX,
    /// All CommandVersions are sorted in chronological execution order.
    versions: Vec<CommandVersion>,
    /// Synchronization data is sorted chronologically
    //TODO[synchronization] synchronization: SessionSync,
    metadata: Metadata,
}

impl Index<IDXType> for Session {
    type Output = CommandVersion;

    fn index(&self, index: IDXType) -> &Self::Output {
        &self.versions[index]
    }
}

impl Session {
    pub fn new(uid: SessionUID) -> Self {
        Self {
            uid,
            ..Default::default()
        }
    }
}

#[derive(Clone, Debug, Default, PartialEq, Eq, Getters, MutGetters, Serialize, Deserialize)]
#[getset(get_copy = "pub", get = "pub", get_mut = "pub")]
pub struct SessionSync {
    starts: Vec<(ChangeReference, StatePath)>,
    spans: HashMap<(ChangeReference, StatePath), (Vec<SIDX>, Option<ChangeReference>)>,
}

impl Hash for SessionSync {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.starts.hash(state);
        self.spans
            .iter()
            .sorted()
            .collect::<Vec<(
                &(ChangeReference, StatePath),
                &(Vec<SIDX>, Option<ChangeReference>),
            )>>()
            .hash(state);
    }
}
