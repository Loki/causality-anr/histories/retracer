
use std::{collections::HashMap, hash::Hash};

use serde::{Serialize, Deserialize};
use getset::{Getters, MutGetters};
use itertools::Itertools;

use crate::{identifiers::{uids::{CommandTypeName, ArgumentName, TypeName}, idxs::CVIDX}, record::change::ChangeRecord, register::Register};

use super::meta_data::Metadata;
#[derive(Clone, Debug, Default, PartialEq, Eq, Getters, MutGetters, Serialize, Deserialize)]
#[getset(get_copy = "pub", get = "pub", get_mut = "pub")]
pub struct CommandType {
    name: CommandTypeName,
    arguments: HashMap<ArgumentName, Option<TypeName>>,
    inherits: Vec<TypeName>,
    metadata: Metadata,
    versions: Vec<CVIDX>,
}

impl Hash for CommandType {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.name.hash(state);
        self.arguments.iter().sorted().collect::<Vec<(&ArgumentName, &Option<TypeName>)>>().hash(state);
        self.inherits.hash(state);
        self.metadata.hash(state);
        self.versions.hash(state);
    }
}


impl ChangeRecord for CommandType {
    fn register(&self) -> crate::register::Register {
        Register::default()
    }
}
