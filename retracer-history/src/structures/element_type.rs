
use std::{collections::HashMap, hash::Hash};

use itertools::Itertools;
use getset::{Getters, MutGetters};
use serde::{Serialize, Deserialize};

use crate::{identifiers::{uids::{ElementTypeName, AttributeName, TypeName}, idxs::EIDX}, record::change::ChangeRecord, register::Register};

use super::meta_data::Metadata;
#[derive(Clone, Debug, Default, PartialEq, Eq, Getters, MutGetters, Serialize, Deserialize)]
#[getset(get_copy = "pub", get = "pub", get_mut = "pub")]
pub struct ElementType {
    name: ElementTypeName,
    attributes: HashMap<AttributeName, Option<TypeName>>,
    inherits: Vec<TypeName>,
    elements: Vec<EIDX>,
    metadata: Metadata,
}

impl Hash for ElementType {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.name.hash(state);
        self.attributes.iter().sorted().collect::<Vec<(&AttributeName, &Option<TypeName>)>>().hash(state);
        self.inherits.hash(state);
        self.elements.hash(state);
        self.metadata.hash(state);
    }
}


impl ChangeRecord for ElementType {
    fn register(&self) -> crate::register::Register {
        Register::default()
    }
}


impl ElementType {
    pub fn new(name: ElementTypeName) -> Self {
        Self {
            name,
            ..Default::default()
        }
    }
}
