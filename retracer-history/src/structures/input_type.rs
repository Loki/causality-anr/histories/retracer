
use std::ops::Index;

use serde::{Serialize, Deserialize};
use getset::{Getters, MutGetters};

use crate::{identifiers::{idxs::IDXType, uids::{InputTypeName, TypeName}}, record::change::ChangeRecord, register::Register};

use super::{input_version::InputVersion, meta_data::Metadata};
#[derive(Clone, Debug, Default, PartialEq, Eq, Hash, Getters, MutGetters, Serialize, Deserialize)]
#[getset(get_copy = "pub", get = "pub", get_mut = "pub")]
pub struct InputType {
    name: InputTypeName,
    inherits: Vec<TypeName>,
    metadata: Metadata,
    versions: Vec<InputVersion>,
}

impl Index<IDXType> for InputType {
    type Output = InputVersion;

    fn index(&self, index: IDXType) -> &Self::Output {
        &self.versions[index]
    }
}

impl ChangeRecord for InputType {
    fn register(&self) -> crate::register::Register {
        Register::default()
    }
}
