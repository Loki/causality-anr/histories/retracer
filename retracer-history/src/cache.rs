use std::{
    collections::{BTreeMap, HashMap},
    hash::Hash,
};

//use itertools::Itertools;
use serde::{Deserialize, Serialize};

use crate::{
    identifiers::{
        idxs::{BranchIDX, BranchStateIDX, StIDX, CVIDX, IVIDX},
        references::{
            change::ChangeReference,
            input::InputReference,
            state::{StateReference, StateTree},
        },
        timestamp::Timestamp,
        uids::SessionUID,
    },
    path::StatePath,
};

#[derive(Clone, Debug, Default, PartialEq, Eq, Serialize, Deserialize)]
pub struct Cache {
    states: HashMap<CVIDX, HashMap<BranchIDX, BTreeMap<BranchStateIDX, StateTreeCache>>>,
    changes: HashMap<SessionUID, BTreeMap<Timestamp, CVIDX>>,
    inputs: HashMap<InputReference, IVIDX>,
}

impl Hash for Cache {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        /*
        self.states
            .iter()
            .map(|(cv_idx, &hm)| {
                hm.iter()
                    .sorted_by_key(|d| d.0)
                    .collect::<Vec<(BranchIDX, BTreeMap<BranchStateIDX, StateTreeCache>)>>()
            })
            .collect::<Vec<(
                CVIDX,
                Vec<(BranchIDX, BTreeMap<BranchStateIDX, StateTreeCache>)>,
            )>>()
            .hash(state);
        */
        todo!()
    }
}

impl Cache {
    pub fn state(&self, state: StIDX) -> Result<&StateTree, Option<(StIDX, &StateTree)>> {
        let branch = self
            .states
            .get(&state.cv)
            .ok_or(None)?
            .get(&state.branch)
            .ok_or(None)?;
        match branch.range(..=state.idx).next_back() {
            Some((bst_idx, tree_cache)) if bst_idx == &state.idx => Ok(&tree_cache.tree(self)),
            Some((prev_bst_idx, tree)) => Err(Some((
                StIDX::new(state.cv.clone(), state.branch, prev_bst_idx.clone(), None),
                &tree.tree(self),
            ))),
            None => Err(None),
        }
    }
    pub fn input(&self, input: InputReference) -> Option<&IVIDX> {
        self.inputs.get(&input)
    }
    pub fn change(&self, change: ChangeReference) -> Result<&CVIDX, Option<(&Timestamp, &CVIDX)>> {
        match self
            .changes
            .get(&change.session)
            .ok_or(None)?
            .range(..=&change.timestamp)
            .next_back()
        {
            Some((ts, cv_idx)) if ts == &change.timestamp => Ok(cv_idx),
            Some(r) => Err(Some(r)),
            None => Err(None),
        }
    }

    /// Save input index, overwrite if already existent
    pub fn cache_input(&mut self, input: InputReference, idx: IVIDX) {
        self.inputs.insert(input, idx);
    }

    /// Save change index, overwrite if already existent
    pub fn cache_change(&mut self, change: ChangeReference, idx: CVIDX) {
        let s = self.changes.get_mut(&change.session).unwrap_or_else(|| {
            // create CV data
            self.changes.insert(change.session, BTreeMap::new());
            &mut self.changes[&change.session]
        });

        s.insert(change.timestamp, idx);
    }

    /// Save state tree, overwrite if already existent
    pub fn cache_state(&mut self, state: StateReference, cv: CVIDX, tree: StateTree) {
        let states = self.states;

        let cv = states.get_mut(&cv).unwrap_or_else(|| {
            // create CV data
            states.insert(cv, HashMap::new());
            &mut states[&cv]
        });

        let branch = cv.get_mut(&state.branch).unwrap_or_else(|| {
            // create branch
            cv.insert(state.branch, BTreeMap::new());
            &mut cv[&state.branch]
        });

        branch.insert(state.idx, StateTreeCache::Tree(tree));
    }
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub enum StateTreeCache {
    Tree(StateTree),
    Reference(CVIDX, BranchIDX, BranchStateIDX, StatePath),
}
impl StateTreeCache {
    pub fn tree(self, cache: &Cache) -> StateTree {
        match self {
            StateTreeCache::Tree(tree) => tree,
            StateTreeCache::Reference(cv_idx, b_idx, bst_idx, st_p) => {
                cache.states[&cv_idx][&b_idx][&bst_idx].tree(cache)
            }
        }
    }
}
