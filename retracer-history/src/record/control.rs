use crate::structures::{element_type::ElementType, command_type::CommandType, input_type::InputType, element::Element, session::Session, element_version::ElementVersion, command_version::CommandVersion, input_version::InputVersion};
use super::change::ChangeRecord;

pub trait ControlRecord: ChangeRecord {
    fn changed_element_types(&self) -> Vec<&ElementType> {Vec::new()}
    fn changed_command_types(&self) -> Vec<&CommandType> {Vec::new()}
    fn changed_input_types(&self) -> Vec<&InputType> {Vec::new()}
    fn changed_elements(&self) -> Vec<&Element> {Vec::new()}
    fn changed_sessions(&self) -> Vec<&Session> {Vec::new()}
    fn changed_element_versions(&self) -> Vec<&ElementVersion> {Vec::new()}
    fn changed_command_versions(&self) -> Vec<&CommandVersion> {Vec::new()}
    fn changed_input_versions(&self) -> Vec<&InputVersion> {Vec::new()}

    fn removed_element_types(&self) -> Vec<&ElementType> {Vec::new()}
    fn removed_command_types(&self) -> Vec<&CommandType> {Vec::new()}
    fn removed_input_types(&self) -> Vec<&InputType> {Vec::new()}
    fn removed_elements(&self) -> Vec<&Element> {Vec::new()}
    fn removed_sessions(&self) -> Vec<&Session> {Vec::new()}
    fn removed_element_versions(&self) -> Vec<&ElementVersion> {Vec::new()}
    fn removed_command_versions(&self) -> Vec<&CommandVersion> {Vec::new()}
    fn removed_input_versions(&self) -> Vec<&InputVersion> {Vec::new()}
}
