use std::collections::HashMap;

use crate::{register::Register, structures::{element_type::ElementType, command_type::CommandType, input_type::InputType, element::Element, session::Session, element_version::ElementVersion, command_version::CommandVersion, input_version::InputVersion, meta_data::Metadata}, identifiers::{references::Reference, idxs::{ETIDX, CTIDX, ITIDX, EIDX, SIDX, EVIDX, CVIDX, IVIDX}, uids::StructureUID}};


pub trait ChangeRecord {
    fn register(&self) -> Register;

    fn new_element_types(&self) -> HashMap<ETIDX, ElementType> {HashMap::new()}
    fn new_command_types(&self) -> HashMap<CTIDX, CommandType> {HashMap::new()}
    fn new_input_types(&self) -> HashMap<ITIDX, InputType> {HashMap::new()}
    /// Elements can contain ElementVersions
    fn new_elements(&self) -> HashMap<EIDX, Element> {HashMap::new()}
    /// Sessions can contain CommandVersions
    fn new_sessions(&self) -> HashMap<SIDX, Session> {HashMap::new()}
    fn new_element_versions(&self) -> HashMap<EVIDX, ElementVersion> {HashMap::new()}
    fn new_command_versions(&self) -> HashMap<CVIDX, CommandVersion> {HashMap::new()}
    fn new_input_versions(&self) -> HashMap<IVIDX, InputVersion> {HashMap::new()}

    fn changed_meta_data(&self) -> HashMap<StructureUID, Metadata> {HashMap::new()}
}





/*
pub trait ChangeRecord {
    fn register(&self) -> Register;

    fn new_element_types(&self) -> Option<&[ElementType]> {None}
    fn new_command_types(&self) -> Option<&[CommandType]> {None}
    fn new_input_types(&self) -> Option<&[InputType]> {None}
    /// Elements can contain ElementVersions
    fn new_elements(&self) -> Option<&[Element]> {None}
    /// Sessions can contain CommandVersions
    fn new_sessions(&self) -> Option<&[Session]> {None}
    fn new_element_versions(&self) -> Option<&[ElementVersion]> {None}
    fn new_command_versions(&self) -> Option<&[CommandVersion]> {None}
    fn new_input_versions(&self) -> Option<&[InputVersion]> {None}

    fn changed_meta_data(&self) -> Vec<(Reference, Metadata)> {Vec::new()}
}
*/
