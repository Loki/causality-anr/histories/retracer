use crate::{identifiers::{uids::{InputTypeName, CommandTypeName, SessionUID}, idxs::{EIDX, SIDX, ETIDX, CTIDX, ITIDX, StructureIDX, EVIDX, CVIDX, IVIDX, SyncStartIdx}, references::{state::StateReference, change::ChangeReference, input::InputReference}}, path::StatePath, structures::{element_type::ElementType, input_type::InputType, command_type::CommandType, element::Element, session::Session}};

use super::identifiers::uids::{ElementUID, ElementTypeName};

use getset::Getters;
use itertools::Itertools;
use serde::{Serialize, Deserialize};
use std::{collections::HashMap, hash::Hash};

#[derive(Clone, Debug, Default, PartialEq, Eq, Getters, Serialize, Deserialize)]
#[getset(get_copy = "pub", get = "pub")]
pub struct Register {
    elements: HashMap<ElementUID, EIDX>,
    sessions: HashMap<SessionUID, SIDX>,
    element_types: HashMap<ElementTypeName, ETIDX>,
    command_types: HashMap<CommandTypeName, CTIDX>,
    input_types: HashMap<InputTypeName, ITIDX>,

    element_versions_references: HashMap<StateReference, EVIDX>,
    command_versions_references: HashMap<ChangeReference, CVIDX>,
    input_versions_references: HashMap<InputReference, IVIDX>,

    //incomplete: HashMap<StructureIDX, Register>,
    session_synchronization: HashMap<SIDX, Vec<SyncStartIdx>>,
}

impl Hash for Register {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.elements.keys().collect::<Vec<&ElementUID>>().sort().hash(state);
        self.sessions.keys().sorted().collect::<Vec<&SessionUID>>().sort().hash(state);
        self.element_types.keys().sorted().collect::<Vec<&ElementTypeName>>().sort().hash(state);
        self.command_types.keys().sorted().collect::<Vec<&CommandTypeName>>().sort().hash(state);
        self.input_types.keys().sorted().collect::<Vec<&InputTypeName>>().sort().hash(state);
    }
}

impl Register {
    pub fn try_get<I: IntoIndex>(&self, identifier: &I) -> Option<I::Index> {
        identifier.try_index(self)
    }

    pub fn get<I: IntoIndex>(&self, identifier: &I) -> I::Index {
        identifier.index(self)
    }

    pub fn new_element_uid(&mut self, idx: EIDX) -> ElementUID {
        let mut uid = ElementUID::default();
        while self.elements.contains_key(&uid) {
            uid = ElementUID::default();
        }
        self.elements.insert(uid.clone(), idx);
        uid
    }

    pub fn new_session_uid(&mut self, idx: SIDX) -> SessionUID {
        let mut uid = SessionUID::default();
        while self.sessions.contains_key(&uid) {
            uid = SessionUID::default();
        }
        self.sessions.insert(uid.clone(), idx);
        uid
    }

    pub fn add_element_type(&mut self, name: &ElementTypeName, idx: ETIDX) -> bool {
        if self.element_types().contains_key(name) {
            false
        } else {
            self.element_types.insert(name.clone(), idx);
            true
        }
    }

    pub fn add_input_type(&mut self, name: &InputTypeName, idx: ITIDX) -> bool {
        if self.input_types().contains_key(name) {
            false
        } else {
            self.input_types.insert(name.clone(), idx);
            true
        }
    }

    pub fn add_command_type(&mut self, name: &CommandTypeName, idx: CTIDX) -> bool {
        if self.command_types().contains_key(name) {
            false
        } else {
            self.command_types.insert(name.clone(), idx);
            true
        }
    }

    pub fn add_element(&mut self, uid: &ElementUID, idx: EIDX) -> bool {
        if self.elements().contains_key(uid) {
            false
        } else {
            self.elements.insert(uid.clone(), idx);
            true
        }
    }

    pub fn add_session(&mut self, uid: &SessionUID, idx: SIDX) -> bool {
        if self.sessions().contains_key(uid) {
            false
        } else {
            self.sessions.insert(uid.clone(), idx);
            true
        }
    }

    pub fn element_type_name_of(&self, idx: ETIDX) -> Option<ElementTypeName> {
        for (&n, &i) in self.element_types() {
            if i == idx {
                return Some(n);
            }
        }
        None
    }

    pub fn command_type_name_of(&self, idx: CTIDX) -> Option<CommandTypeName> {
        for (&n, &i) in self.command_types() {
            if i == idx {
                return Some(n);
            }
        }
        None
    }

    pub fn input_type_name_of(&self, idx: ITIDX) -> Option<InputTypeName> {
        for (&n, &i) in self.input_types() {
            if i == idx {
                return Some(n);
            }
        }
        None
    }

    pub(crate) fn append(&mut self, register: Self) -> Result<(), Self> {
        todo!()
    }
}


pub trait IntoIndex {
    type Index;
    fn index(&self, register: &Register) -> Self::Index;
    fn try_index(&self, register: &Register) -> Option<Self::Index>;
}

