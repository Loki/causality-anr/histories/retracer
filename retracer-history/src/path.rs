use std::{
    ops::{Deref, DerefMut},
    vec::IntoIter,
};

use serde::{Deserialize, Serialize};

use crate::identifiers::{
    idxs::{BranchIDX, StateDiffIDX},
    uids::{ArgumentName, AttributeName},
};

#[derive(Clone, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct StatePath {
    path: Vec<AttributeName>,
}

impl StatePath {
    pub fn new() -> Self {
        Self { path: Vec::new() }
    }
}

impl Deref for StatePath {
    type Target = Vec<AttributeName>;

    fn deref(&self) -> &Self::Target {
        &self.path
    }
}

impl DerefMut for StatePath {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.path
    }
}

impl IntoIterator for StatePath {
    type Item = AttributeName;

    type IntoIter = IntoIter<AttributeName>;

    fn into_iter(self) -> Self::IntoIter {
        self.path.into_iter()
    }
}

#[derive(Clone, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct ChangePath {
    path: Vec<usize>,
}

impl ChangePath {
    pub fn add_child(&mut self, idx: usize) {
        self.path.push(idx)
    }
    pub fn pop(&mut self) -> Option<usize> {
        self.path.pop()
    }
    pub fn add_parent(&mut self, idx: usize) {
        self.path.insert(0, idx);
    }
}

#[derive(Clone, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct Path {
    path: Option<PathSegment>,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
enum PathSegment {
    State(PathSegmentState),
    Change(PathSegmentChange),
    Input(PathSegmentInput),
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
enum PathSegmentState {
    State(StatePath, NextPath<Self>),
    StToChange(NextPath<PathSegmentChange>),
    Forward(StateBranchIDX, NextPath<Self>),
    Element(Option<MDPath>),
    ElementType(Option<MDPath>),
    MD(Option<MDPath>),
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
enum PathSegmentChange {
    Change(ChangePath, NextPath<Self>),
    ChToState(BranchIDX, StateDiffIDX, NextPath<PathSegmentState>),
    ChToInput(usize, NextPath<PathSegmentInput>),
    Argument(ArgumentName, NextPath<PathSegment>),
    Session(Option<MDPath>),
    CommandType(Option<MDPath>),
    MD(Option<MDPath>),
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
enum PathSegmentInput {
    IToChange(NextPath<PathSegmentChange>),
    InputType(Option<MDPath>),
    MD(Option<MDPath>),
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
enum MDPath {
    Annotations(Option<String>),
    Tags,
    Name,
}

type NextPath<T> = Option<Box<T>>;
type StateBranchIDX = Option<usize>;
