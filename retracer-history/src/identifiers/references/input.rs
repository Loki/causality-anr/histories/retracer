use std::hash::Hash;

use getset::Getters;
use serde::{Deserialize, Serialize};

use crate::{identifiers::idxs::IVIDX, structures::trip::TRIPLink, History, Idx};

use super::change::ChangeReference;

#[derive(Clone, Debug, Getters, Serialize, Deserialize)]
#[getset(get_copy = "pub", get = "pub")]
pub struct InputReference {
    /// The Change this Input originated from
    pub(crate) origin: ChangeReference,
    /// The Input Index in the Change
    pub(crate) number: usize,
}

impl Hash for InputReference {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.change.hash(state);
        self.number.hash(state);
    }
}

impl PartialEq for InputReference {
    fn eq(&self, other: &Self) -> bool {
        self.change.eq(&other.change) && self.number.eq(&other.number)
    }
}

impl Eq for InputReference {}

impl Idx for InputReference {
    type Output = IVIDX;

    fn get<'a>(&self, history: &'a History) -> &'a Self::Output {
        history.input(self)
    }
}
