use std::{collections::HashMap, hash::Hash};

use serde::{Deserialize, Serialize};

use crate::{
    identifiers::{
        idxs::{BranchIDX, BranchStateIDX, EVIDX},
        uids::AttributeName,
    },
    path::StatePath,
    History, Idx,
};

use super::change::ChangeReference;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct StateReference {
    pub(crate) origin: ChangeReference,
    pub(crate) branch: BranchIDX,
    pub(crate) idx: BranchStateIDX,
    pub(crate) sub_state: Option<StatePath>,
}

impl PartialEq for StateReference {
    fn eq(&self, other: &Self) -> bool {
        self.origin.eq(&other.origin)
            && self.branch.eq(&other.branch)
            && self.idx.eq(&other.idx)
            && self.sub_state.eq(&other.sub_state)
    }
}

impl Eq for StateReference {}

impl Hash for StateReference {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.origin.hash(state);
        self.branch.hash(state);
        self.idx.hash(state);
        self.sub_state.hash(state);
    }
}

//[TODO] performance: use a Vec instead to keep the data contiguous?
#[derive(Clone, Debug, Default, PartialEq, Eq, Serialize, Deserialize)]
pub struct StateTree {
    root: StateTreeNode,
}

impl StateTree {
    pub fn nodes(&self) -> Vec<(StatePath, EVIDX, Vec<AttributeName>)> {
        self.root.nodes(StatePath::new())
    }
}

#[derive(Clone, Debug, Default, PartialEq, Eq, Serialize, Deserialize)]
pub struct StateTreeNode {
    pub(crate) node: EVIDX,
    pub(crate) children: HashMap<AttributeName, Box<StateTreeNode>>,
}

impl StateTreeNode {
    pub fn nodes(&self, path: StatePath) -> Vec<(StatePath, EVIDX, Vec<AttributeName>)> {
        self.children.iter().fold(
            vec![(path, self.node, self.children.keys().cloned().collect())],
            |v, (an, node)| {
                let mut p = path.clone();
                p.push(*an);
                v.append(&mut node.nodes(p));
                v
            },
        )
    }
}

#[derive(Clone, Debug, Default, PartialEq, Eq, Serialize, Deserialize)]
pub struct StateTreeBuilder {
    missing_nodes: usize,
    root: StateTreeNodeBuilder,
}

impl StateTreeBuilder {
    pub fn new() -> Self {
        StateTreeBuilder {
            missing_nodes: 0,
            root: StateTreeNodeBuilder {
                node: None,
                children: HashMap::new(),
                removed_children: HashMap::new(),
            },
        }
    }
    pub fn complement(&mut self, path: StatePath, version: EVIDX, children: Vec<AttributeName>) {
        //(NOTE:!!! complement should not add EVs removed after !!!)
        // NOTE: Need to use references to navigate the tree?
        let mut node = self.root;
        for attr in path {
            if let Some(c) = node.children[&attr] {
                node = c;
            } else {
                node.children
                    .insert(attr, Some(StateTreeNodeBuilder::new()));
                node = node.children[&attr].unwrap();
            }
        }
    }
}

/// A complete node builder has either no data at all (removed node) or has at least an EVIDX and all children have an associated value.
#[derive(Clone, Debug, Default, PartialEq, Eq, Serialize, Deserialize)]
pub struct StateTreeNodeBuilder {
    node: Option<EVIDX>,
    children: HashMap<AttributeName, Option<StateTreeNodeBuilder>>,
}

impl StateTreeNodeBuilder {
    pub fn new() -> Self {
        Self {
            node: None,
            children: HashMap::new(),
        }
    }
    pub fn complete(&self) -> bool {
        (self.node.is_some() && self.children.values().find(|v| v.is_none()).is_none())
            || (self.node.is_none() && self.children.is_empty())
    }
}

//TODO: fix error propagation
#[derive(Debug)]
pub enum StTrBErr {
    MissingNode,
    MissingChild(Vec<AttributeName>),
}

impl StTrBErr {
    pub fn add(&mut self, a: AttributeName) {
        if let StTrBErr::MissingChild(cs) = self {
            cs.push(a);
        }
    }
}

impl TryInto<StateTree> for StateTreeBuilder {
    type Error = StTrBErr;

    fn try_into(self) -> Result<StateTree, Self::Error> {
        Ok(StateTree {
            root: self.root.try_into()?,
        })
    }
}

impl Idx for StateReference {
    type Output = Result<StateTree, &'static str>;

    fn get<'a>(&self, history: &'a History) -> &'a Self::Output {
        &history.state(*self)
    }
}

impl TryInto<StateTreeNode> for StateTreeNodeBuilder {
    type Error = StTrBErr;

    fn try_into(self) -> Result<StateTreeNode, Self::Error> {
        Ok(StateTreeNode {
            node: self.node.ok_or(StTrBErr::MissingNode)?,
            children: self
                .children
                .into_iter()
                .map(|(attr_n, o_st_tr)| {
                    o_st_tr
                        .ok_or(StTrBErr::MissingNode)
                        .and_then(|stb| stb.try_into())
                        .map_err(|e| {
                            e.add(attr_n);
                            e
                        })
                        .map(|sttr| (attr_n, Box::new(sttr)))
                })
                .collect::<Result<HashMap<AttributeName, Box<StateTreeNode>>, Self::Error>>()?,
        })
    }
}
