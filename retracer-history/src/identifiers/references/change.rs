use std::hash::Hash;

use getset::Getters;
use serde::{Deserialize, Serialize};

use crate::{
    identifiers::{
        idxs::CVIDX,
        timestamp::Timestamp,
        uids::{HistoryUID, SessionUID},
    },
    path::ChangePath,
    structures::session,
    History, Idx,
};

#[derive(Clone, Debug, Getters, Serialize, Deserialize)]
#[getset(get_copy = "pub", get = "pub")]
pub struct ChangeReference {
    pub(crate) history: HistoryUID,
    pub(crate) session: SessionUID,
    pub(crate) timestamp: Timestamp,
    // TODO: revision 2
    // When the execution or one of its parent is currently running
    // running: Option<ChangeUID>,
    //TODO[synchronization] sync: HashMap<StatePath, Vec<CVIDX>>,
}

impl PartialEq for ChangeReference {
    fn eq(&self, other: &Self) -> bool {
        self.history.eq(&other.history)
            && self.session.eq(&other.session)
            && self.timestamp.eq(&other.timestamp)
    }
}

impl Eq for ChangeReference {}

impl PartialOrd for ChangeReference {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.timestamp.partial_cmp(&other.timestamp)
    }
}

impl Ord for ChangeReference {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.partial_cmp(other).unwrap()
    }
}

impl Hash for ChangeReference {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.history.hash(state);
        self.session.hash(state);
        self.timestamp.hash(state);
    }
}

impl ChangeReference {
    /*
    // TODO: add sync support
    pub fn compute(&mut self, history: &History) -> Result<(), ()> {
        if &self.history != history.uid() {
            return Err(());
        }

        //find closest previous cached StTr
        //if some, rebuild from there
        //if none, go to starting Session and retry
        //if at a root Session, return None

        let sid = history.register().sessions()[&self.session].idx();
        let session = history.sessions()[sid];
        // Does the Session contain a CV matching the timestamp?
        let found_idx = session
            .versions()
            .binary_search_by_key(&&self.timestamp, |v| v.timestamp());
        if let Ok(idx) = found_idx {
            // Set idxs
            self.cache = Some(CVIDX::new(sid, idx));
            // parent idx
            if let Some(pidx) = history.get(self.cache.get().unwrap()).parent() {
                let mut path = ChangePath::default();
                path.add_parent(pidx.version());
                //self.sub_change = Some(self.get_path(history, pidx.clone(), path));
            };
            Ok(())
        } else {
            //TODO[synchronization] self.compute_sync(history, &session)
            Err(())
        }
    }*/

    /*fn get_path(&self, history: &History, cvidx: CVIDX, mut path: ChangePath) -> ChangePath {
        match history.get(cvidx).parent() {
            Some(pidx) => {
                //CHECK: correct data added?
                path.add_parent(pidx.version());
                self.get_path(history, pidx.clone(), path)
            }
            None => path,
        }
    }*/
    /*
    fn get_root(&self, history: &History, cvidx: CVIDX) -> CVIDX {
        match history.get(cvidx).parent() {
            Some(pidx) => self.get_root(history, pidx.clone()),
            None => cvidx,
        }
    }*/
    /*
    fn search(&self, history: &History, cvidx: CVIDX, path: &mut ChangePath, timestamp: Timestamp) -> Option<CVIDX> {
        let cv = history.get(cvidx);
        if cv.timestamp() == &timestamp {Some(cvidx)}
        else {
            for (i, ccv) in cv.children().iter().enumerate() {
                path.push(i);
                if let Some(idx) = self.search(history, *ccv, path, timestamp) {
                    return Some(idx);
                };
                path.pop();
            }
            None
        }
    }*/

    /*TODO[synchronization]
    fn compute_sync(&mut self, history: &History, session: &Session) -> Result<(), ()> {
        //CHECK: correctness
        let mut ss: HashMap<&crate::path::StatePath, Vec<SIDX>> = HashMap::new();
        for (start_ref, path) in session.synchronization().starts() {
            if start_ref.timestamp >= self.timestamp {
                break;
            }
            let (sessions, end_change) = history.sessions()
                [history.register().sessions()[&start_ref.session].idx()]
            .synchronization()
            .spans()[&(*start_ref, *path)];
            // Synchronization...
            if let Some(end) = end_change {
                // ...ended
                if end.timestamp >= self.timestamp {
                    if ss.contains_key(path) {
                        ss[path].extend(sessions.iter());
                    } else {
                        ss.insert(path, sessions);
                    }
                }
            } else {
                // ...still going
                if ss.contains_key(path) {
                    ss[path].extend(sessions.iter());
                } else {
                    ss.insert(path, sessions);
                }
            }
            for (ph, ss) in ss {
                self.sync.insert(ph.clone(), Vec::new());
                for s in ss {
                    let session = history.sessions()[s.idx()];
                    let idx = match session
                        .versions()
                        .binary_search_by_key(&&self.timestamp, |v| v.timestamp())
                    {
                        Ok(idx) => idx,
                        Err(idx) => idx,
                    };
                    let idx = self.get_root(history, CVIDX::new(s.idx(), idx)).version();
                    self.sync[ph].push(CVIDX::new(s.idx(), idx));
                }
            }
        }
        Ok(())
    }*/

    /* Old
    //=======C===========C===
    //---------C-------------
    //----C-------C--C-------
    //C----------------------
    //--C--------------------
    //
    //TODO[synchronization, sub_state, detached]
    pub fn next(&self, history: &History) -> Option<CVIDX> {
        if self.idx.is_none() {
            self.compute(history).ok()?;
        };
        // sub command or top level
        if let Some(parent) = history.get(self.idx.unwrap()).parent() {
            let iter = history.get(*parent).children().iter();
            iter.find(|&&i| i == self.idx.unwrap());
            // remaining sub changes or last sub change
            if let Some(sub) = iter.next().cloned() {
                Some(sub)
            } else {
                None
            }
        } else {
            let s = history.sessions()[self.idx.unwrap().session()];
            let iter = s
                .versions()
                .iter()
                .enumerate()
                .skip(self.idx.unwrap().version())
                .filter(|(i, &v)| v.parent().is_none());
            //old  skip instead iter.find(|&&i| i == self.idx);
            // remaining changes or last change
            if let Some(change) = iter.next() {
                Some(CVIDX::new(self.idx().unwrap().session(), change.0))
            } else {
                None
            }
        }
    }*/

    /// Get the following Change.
    /// It skips the sub Changes in between. Use `first_child` to get the first sub child.
    pub fn next(&self, history: &History) -> Option<Self> {
        if self.idx.is_none() {
            self.compute(history).ok()?;
        };

        let session = history.sessions()[self.idx.unwrap().session()];
        let parent = session.versions()[self.idx.unwrap().version()].parent();
        session
            .versions()
            .iter()
            .enumerate()
            .skip(self.idx.unwrap().version() + 1)
            .find(|(_, v)| v.parent() == parent)
            .map(|(i, v)| Self {
                timestamp: v.timestamp().clone(),
                cache: Cell::new(Some(CVIDX::new(self.idx.unwrap().session(), i))),
                ..*self
            })
    }

    /// Get the previous Change.
    /// It skips the sub Changes in between.
    pub fn prev(&self, history: &History) -> Option<Self> {
        if self.idx.is_none() {
            self.compute(history).ok()?;
        };

        let session = history.sessions()[self.idx.unwrap().session()];
        let parent = session.versions()[self.idx.unwrap().version()].parent();
        session
            .versions()
            .iter()
            .enumerate()
            .take(self.idx.unwrap().version())
            .rev()
            .find(|(_, v)| v.parent() == parent)
            .map(|(i, v)| Self {
                timestamp: v.timestamp().clone(),
                cache: Cell::new(Some(CVIDX::new(self.idx.unwrap().session(), i))),
                ..*self
            })
    }

    pub fn parent(&self, history: &History) -> Option<Self> {
        if self.idx.is_none() {
            self.compute(history).ok()?;
        };

        let session = history.sessions()[self.idx.unwrap().session()];
        session.versions()[self.cache.get().unwrap().version()]
            .parent()
            .map(|v| Self {
                timestamp: session.versions()[v.version()].timestamp().clone(),
                cache: Cell::new(Some(CVIDX::new(
                    self.cache.get().unwrap().session(),
                    v.version(),
                ))),
                ..*self
            })
    }

    pub fn first_child(&self, history: &History) -> Option<Self> {
        if self.cache.get().is_none() {
            self.compute(history).ok()?;
        };

        history.sessions()[self.cache.get().unwrap().session()]
            .versions()
            .get(self.cache.get().unwrap().version() + 1)
            .and_then(|v| {
                if v.parent() == &self.cache.get() {
                    Some(Self {
                        timestamp: v.timestamp().clone(),
                        cache: Cell::new(Some(CVIDX::new(
                            self.cache.get().unwrap().session(),
                            self.cache.get().unwrap().version() + 1,
                        ))),
                        ..*self
                    })
                } else {
                    None
                }
            })
    }

    /*TODO
    pub fn prev(&self, history: &History) -> Self {
        let ch = self.clone();
        ch
    }*/
}

impl Idx for ChangeReference {
    type Output = CVIDX;

    fn get<'a>(&self, history: &'a History) -> &'a Self::Output {
        history.change(self)
    }
}
