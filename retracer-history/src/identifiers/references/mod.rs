pub mod state;
pub mod change;
pub mod input;

use serde::{Serialize, Deserialize};

use self::{state::StateReference, change::ChangeReference, input::InputReference};

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub enum Reference {
    State(StateReference),
    Change(ChangeReference),
    Input(InputReference),
}
