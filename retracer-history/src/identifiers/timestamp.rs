use serde::{Serialize, Deserialize};

#[derive(Clone, Debug, PartialEq, PartialOrd, Ord, Eq, Hash, Serialize, Deserialize)]
pub struct Timestamp(#[serde(with = "timestamp_serialization")] chrono::DateTime<chrono::Utc>);

impl Timestamp {
    pub fn new() -> Self {
        Self(chrono::Utc::now())
    }
}

impl Default for Timestamp {
    fn default() -> Self {
        Self(chrono::Utc::now())
    }
}

mod timestamp_serialization {
    use chrono::{DateTime, TimeZone, Utc};
    use serde::{Deserialize, Serializer, Deserializer};

    const FORMAT: &'static str = "%Y/%m/%d %H:%M:%S.%f %Z";

    pub fn serialize<S>(date: &DateTime<Utc>, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let s = format!("{}", date.format(FORMAT));
        serializer.serialize_str(&s)
    }

    pub fn deserialize<'de, D>(deserializer: D) -> Result<DateTime<Utc>, D::Error>
    where
        D: Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?;
        Utc.datetime_from_str(&s, FORMAT)
            .map_err(serde::de::Error::custom)
    }
}
