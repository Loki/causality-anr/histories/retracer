use serde::{Deserialize, Serialize};

use crate::{
    path::StatePath,
    structures::{
        command_type::CommandType, command_version::CommandVersion, element::Element,
        element_type::ElementType, element_version::ElementVersion, input_type::InputType,
        input_version::InputVersion, session::Session,
    },
    History, Idx,
};

// types
pub type IDXType = usize;

pub type TRIPIDX = usize;

pub type SyncIDX = usize;

/// A CommandVersion's State branch index
/// None represents the attached branch and the other the detached branches index
pub type BranchIDX = Option<IDXType>;

/// The position of the State within the branch
pub type StateDiffIDX = IDXType;

#[derive(Clone, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct StIDX {
    pub(crate) cv: CVIDX,
    pub(crate) branch: BranchIDX,
    pub(crate) idx: BranchStateIDX,
    pub(crate) sub_state: Option<StatePath>,
}

impl StIDX {
    pub(crate) fn new(
        cv: CVIDX,
        branch: BranchIDX,
        idx: IDXType,
        sub_state: Option<StatePath>,
    ) -> Self {
        Self {
            cv,
            branch,
            idx,
            sub_state,
        }
    }

    pub fn next(&mut self) {
        todo!()
    }
}

#[derive(
    Copy, Clone, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize,
)]
pub struct PathIdx(IDXType);

impl PathIdx {
    pub fn new(idx: IDXType) -> Self {
        Self(idx)
    }
}

#[derive(
    Copy, Clone, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize,
)]
pub struct EVIDX(IDXType, IDXType);

impl EVIDX {
    pub(crate) fn new(element: IDXType, version: IDXType) -> Self {
        Self(element, version)
    }
    pub fn element(&self) -> IDXType {
        self.0
    }
    pub fn version(&self) -> IDXType {
        self.1
    }

    pub fn next(&mut self) {
        self.0 += 1;
    }
}

impl Idx for EVIDX {
    type Output = ElementVersion;

    fn get<'a>(&self, history: &'a History) -> &'a Self::Output {
        &history.elements()[self.element()][self.version()]
    }
}

#[derive(
    Copy, Clone, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize,
)]
pub struct CVIDX(IDXType, IDXType);

impl CVIDX {
    pub(crate) fn new(session: IDXType, version: IDXType) -> Self {
        Self(session, version)
    }
    pub fn session(&self) -> IDXType {
        self.0
    }
    pub fn version(&self) -> IDXType {
        self.1
    }

    pub fn next(&mut self) {
        self.0 += 1;
    }
}

impl Idx for CVIDX {
    type Output = CommandVersion;

    fn get<'a>(&self, history: &'a History) -> &'a Self::Output {
        &history.sessions()[self.session()][self.version()]
    }
}

#[derive(
    Copy, Clone, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize,
)]
pub struct IVIDX(IDXType, IDXType);

impl IVIDX {
    pub(crate) fn new(input_type: IDXType, version: IDXType) -> Self {
        Self(input_type, version)
    }
    pub fn input_type(&self) -> IDXType {
        self.0.clone()
    }
    pub fn version(&self) -> IDXType {
        self.1
    }

    pub fn next(&mut self) {
        self.0 += 1;
    }
}

impl Idx for IVIDX {
    type Output = InputVersion;

    fn get<'a>(&self, history: &'a History) -> &'a Self::Output {
        &history.input_types()[self.input_type()][self.version()]
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub enum VIDX {
    Element(EVIDX),
    Command(CVIDX),
    Input(IVIDX),
}

#[derive(
    Copy, Clone, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize,
)]
pub struct ETIDX(IDXType);

impl ETIDX {
    pub(crate) fn new(idx: IDXType) -> Self {
        Self(idx)
    }

    pub fn next(&mut self) {
        self.0 += 1;
    }
}

impl Idx for ETIDX {
    type Output = ElementType;

    fn get<'a>(&self, history: &'a History) -> &'a Self::Output {
        &history.element_types()[self.0]
    }
}

#[derive(
    Copy, Clone, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize,
)]
pub struct CTIDX(IDXType);

impl CTIDX {
    pub(crate) fn new(idx: IDXType) -> Self {
        Self(idx)
    }

    pub fn next(&mut self) {
        self.0 += 1;
    }
}

impl Idx for CTIDX {
    type Output = CommandType;

    fn get<'a>(&self, history: &'a History) -> &'a Self::Output {
        &history.command_types()[self.0]
    }
}

#[derive(
    Copy, Clone, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize,
)]
pub struct ITIDX(IDXType);

impl ITIDX {
    pub(crate) fn new(idx: IDXType) -> Self {
        Self(idx)
    }

    pub fn next(&mut self) {
        self.0 += 1;
    }
}

impl Idx for ITIDX {
    type Output = InputType;

    fn get<'a>(&self, history: &'a History) -> &'a Self::Output {
        &history.input_types()[self.0]
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub enum TIDX {
    Element(ETIDX),
    Command(CTIDX),
    Input(ITIDX),
}

#[derive(
    Copy, Clone, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize,
)]
pub struct EIDX(IDXType);

impl EIDX {
    pub(crate) fn new(idx: IDXType) -> Self {
        Self(idx)
    }

    pub fn idx(&self) -> IDXType {
        self.0
    }

    pub fn next(&mut self) {
        self.0 += 1;
    }
}

impl Idx for EIDX {
    type Output = Element;

    fn get<'a>(&self, history: &'a History) -> &'a Self::Output {
        &history.elements()[self.0]
    }
}

impl Into<usize> for EIDX {
    fn into(self) -> usize {
        self.0
    }
}

#[derive(
    Copy, Clone, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize,
)]
pub struct SIDX(IDXType);

impl SIDX {
    pub(crate) fn new(idx: IDXType) -> Self {
        Self(idx)
    }

    pub fn idx(&self) -> IDXType {
        self.0
    }

    pub fn next(&mut self) {
        self.0 += 1;
    }
}

impl Idx for SIDX {
    type Output = Session;

    fn get<'a>(&self, history: &'a History) -> &'a Self::Output {
        &history.sessions()[self.0]
    }
}

impl From<IDXType> for SIDX {
    fn from(value: IDXType) -> Self {
        SIDX(value)
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub enum StructureIDX {
    ElementType(ETIDX),
    InputType(ITIDX),
    CommandType(CTIDX),
    Element(EIDX),
    Session(SIDX),
    ElementVersion(EVIDX),
    InputVersion(IVIDX),
    CommandVersion(CVIDX),
}
