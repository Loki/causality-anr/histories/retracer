use std::fmt::Display;

use serde::{Serialize, Deserialize};

use crate::register::IntoIndex;

use super::{references::Reference, idxs::{EIDX, ETIDX, SIDX, ITIDX, CTIDX, TIDX}};

// types
pub type UIDType = uuid::Uuid;
pub type NameType = String;





#[derive(
    Copy, Clone, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize,
)]
pub struct HistoryUID(UIDType);

impl HistoryUID {
    pub fn new(idx: UIDType) -> Self {
        Self(idx)
    }
}

#[derive(
    Copy, Clone, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize,
)]
pub struct ChangeUID(UIDType);

impl ChangeUID {
    pub fn new(idx: UIDType) -> Self {
        Self(idx)
    }
}

#[derive(
    Clone, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize,
)]
pub struct ElementTypeName (NameType);

impl ElementTypeName {
    pub fn new(name: NameType) -> Self {
        Self(name)
    }
}

impl Display for ElementTypeName {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl IntoIndex for ElementTypeName {
    type Index = ETIDX;

    fn index(&self, register: &crate::register::Register) -> Self::Index {
        register.element_types()[&self]
    }

    fn try_index(&self, register: &crate::register::Register) -> Option<Self::Index> {
        if register.element_types().contains_key(&self) {Some(register.element_types()[&self])} else {None}
    }
}

#[derive(
    Clone, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize,
)]
pub struct CommandTypeName (NameType);

impl CommandTypeName {
    pub fn new(name: NameType) -> Self {
        Self(name)
    }
}

impl Display for CommandTypeName {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl IntoIndex for CommandTypeName {
    type Index = CTIDX;

    fn index(&self, register: &crate::register::Register) -> Self::Index {
        register.command_types()[&self]
    }

    fn try_index(&self, register: &crate::register::Register) -> Option<Self::Index> {
        if register.command_types().contains_key(&self) {Some(register.command_types()[&self])} else {None}
    }
}

#[derive(
    Clone, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize,
)]
pub struct InputTypeName (NameType);

impl InputTypeName {
    pub fn new(name: NameType) -> Self {
        Self(name)
    }
}

impl Display for InputTypeName {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl IntoIndex for InputTypeName {
    type Index = ITIDX;

    fn index(&self, register: &crate::register::Register) -> Self::Index {
        register.input_types()[&self]
    }

    fn try_index(&self, register: &crate::register::Register) -> Option<Self::Index> {
        if register.input_types().contains_key(&self) {Some(register.input_types()[&self])} else {None}
    }
}


#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub enum TypeName {
    Element(ElementTypeName),
    Command(CommandTypeName),
    Input(InputTypeName),
}

impl Display for TypeName {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            TypeName::Element(name) => name.fmt(f),
            TypeName::Command(name) => name.fmt(f),
            TypeName::Input(name) => name.fmt(f),
        }
    }
}

impl IntoIndex for TypeName {
    type Index = TIDX;

    fn index(&self, register: &crate::register::Register) -> Self::Index {
        match self {
            TypeName::Element(t) => TIDX::Element(register.element_types()[t]),
            TypeName::Command(t) => TIDX::Command(register.command_types()[t]),
            TypeName::Input(t) => TIDX::Input(register.input_types()[t]),
        }
    }

    fn try_index(&self, register: &crate::register::Register) -> Option<Self::Index> {
        match self {
            TypeName::Element(t) => if register.element_types().contains_key(t) {Some(TIDX::Element(register.element_types()[t]))} else {None},
            TypeName::Command(t) => if register.command_types().contains_key(t) {Some(TIDX::Command(register.command_types()[t]))} else {None},
            TypeName::Input(t) => if register.input_types().contains_key(t) {Some(TIDX::Input(register.input_types()[t]))} else {None},
        }
    }
}



#[derive(
    Clone, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize,
)]
pub struct AttributeName (NameType);

impl AttributeName {
    pub fn new(name: NameType) -> Self {
        Self(name)
    }
}

impl Display for AttributeName {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}


#[derive(
    Clone, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize,
)]
pub struct ArgumentName (NameType);

impl ArgumentName {
    pub fn new(name: NameType) -> Self {
        Self(name)
    }
}

impl Display for ArgumentName {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}



#[derive(
    Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize,
)]
pub struct ElementUID(UIDType);

impl Default for ElementUID {
    fn default() -> Self {
        ElementUID(new_uuid())
    }
}

impl Display for ElementUID {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self.0)
    }
}

impl IntoIndex for ElementUID {
    type Index = EIDX;

    fn index(&self, register: &crate::register::Register) -> Self::Index {
        register.elements()[&self]
    }

    fn try_index(&self, register: &crate::register::Register) -> Option<Self::Index> {
        if register.elements().contains_key(&self) {Some(register.elements()[&self])} else {None}
    }
}


#[derive(
    Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize,
)]
pub struct SessionUID (UIDType);

impl Default for SessionUID {
    fn default() -> Self {
        SessionUID(new_uuid())
    }
}

impl Display for SessionUID {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self.0)
    }
}


impl IntoIndex for SessionUID {
    type Index = SIDX;

    fn index(&self, register: &crate::register::Register) -> Self::Index {
        register.sessions()[&self]
    }

    fn try_index(&self, register: &crate::register::Register) -> Option<Self::Index> {
        if register.sessions().contains_key(&self) {Some(register.sessions()[&self])} else {None}
    }
}

#[derive(Clone, Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum StructureUID {
    Type(TypeName),
    Element(ElementUID),
    Session(SessionUID),
    Record(Reference),
}




pub fn new_uuid() -> uuid::Uuid {
    // create a Uuid
    /*
    use std::time::SystemTime;
    let ts = SystemTime::now()
        .duration_since(SystemTime::UNIX_EPOCH)
        .expect("Current time is earlier than the UNIX epoch.");
    let random_bytes = rand::prelude::random();
    uuid::Builder::from_ufrom_unix_timestamp_millis(
        ts.as_millis()
            .try_into()
            .expect("Couldn't get milliseconds from current time"),
        &random_bytes,
    )
    .into_uuid() */
    uuid::Uuid::new_v4()
}
