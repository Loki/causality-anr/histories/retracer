use self::{uids::NameType, idxs::IDXType};

pub mod idxs;
pub mod uids;
pub mod timestamp;
pub mod references;
pub mod state;

pub enum Identifier<Id> {
    Index(IDXType),
    Name(NameType),
    Id(Id),
}

pub trait Identify<IdentityT> {
    fn id(&self) -> Option<IdentityT>;
    fn name(&self) -> Option<NameType>;
    fn index(&self) -> Option<IDXType>;
}
