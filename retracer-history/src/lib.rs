//! # ReTracer History crate.
//!
//! This is a low level interface to the history.
//! See the [retracer-interface] crate for information on how to use a ReTracer history in a program.
//!
//! This crate contains the structures to build a command history.
//! The main structure is [History] which contains the history, organizes it and checks the validity of any change.
//! This crate is not meant to be used directly in programs, but is an intermediate interface to
//! present the history's internals and allow for new interfaces to be developped.
//! If you want to integrate a ReTracer command history, you can use the parent [retracer] crate
//! which will expose the [retracer-interface] modules for building and interacting with the
//! history at a higher level.
//!
//! # Basic Usage
//!
//! ```
//! let mut history = History::new();
//!
//!
//! // Types //
//! // ElementType creation
//! let layer_et = ElementType::new("Layer");
//! let mut canvas_et = ElementType::new("Canvas");
//! canvas_et.attributes_mut().insert("color_space", Some("ColorSpace"));
//! canvas_et.attributes_mut().insert("dimensions", None);
//! history.record(layer_et);
//! history.record(canvas_et);
//! // CommandType creation
//! let mut init_ct = CommandType::new("InitializeProject");
//! let mut new_layer_ct = CommandType::new("NewLayer");
//! let mut remove_layer_ct = CommandType::new("RemoveLayer");
//! remove_layer_ct.arguments_mut().insert("layer", Some("Layer"));
//! history.record(new_layer_ct);
//! history.record(remove_layer_ct);
//! // InputType creation
//! let stroke_it = InputType::new("Stroke");
//! let layer_index_it = InputType::new("LayerIndex");
//! history.record(stroke_it);
//! history.record(layer_index_it);
//!
//!
//! // Elements and Sessions //
//! // They can be created directly
//! history.new_element("Project");
//! let session_uid = history.new_session();
//!
//!
//! // or through a ChangeRecord.
//! let mut record = ChangeRecord::new();
//! //… record new data (Types, Elements, Sessions, Versions)
//! history.record(record);
//! // Versions can only be added with Records
//! // More on creating and changing Types, Elements, Sessions and Versions in the
//! // [structures](./structures) module.
//! ```

pub mod cache;
/// Structures for referencing Element, Command and Input data.
pub mod identifiers;
/// Trait to append and change history structures
pub mod record;
/// Registration of history identifiers.
pub mod register;
/// Structures composing a history.
pub mod structures;

pub mod path;

use std::{hash::Hash, cell::RefCell};

use cache::Cache;
use getset::Getters;
use identifiers::{
    idxs::{CTIDX, CVIDX, EIDX, ETIDX, ITIDX, SIDX, IVIDX, StIDX},
    references::{
        change::ChangeReference,
        state::{StateTree, StateReference}, input::InputReference,
    },
    uids::{ElementTypeName, HistoryUID},
};
use path::ChangePath;
use record::change::ChangeRecord;
use register::Register;
use serde::{Deserialize, Serialize};
use structures::{
    command_type::CommandType,
    element_type::ElementType,
    input_type::InputType,
    session::Session, trip::TRIPLink, element::Element,
};

use crate::identifiers::references::state::StateTreeBuilder;

#[derive(Clone, Debug, Default, PartialEq, Eq, Getters, Serialize, Deserialize)]
#[getset(get_copy = "pub", get = "pub")]
pub struct History {
    uid: HistoryUID,

    element_types: Vec<ElementType>,
    command_types: Vec<CommandType>,
    input_types: Vec<InputType>,

    elements: Vec<Element>,
    sessions: Vec<Session>,

    register: Register,
    cache: RefCell<Cache>,
}

impl History {
    pub fn new() -> Self {
        Default::default()
    }

    pub fn get<I: Idx>(&self, index: I) -> &I::Output {
        index.get(self)
    }

    pub fn new_element(&mut self, element_type: ElementTypeName) -> Result<&Element, ()> {
        if self.register.element_types().contains_key(&element_type) {
            return Err(());
        }
        let idx = EIDX::new(self.elements.len());
        let element = Element::new(
            self.register
                .element_types()
                .get(&element_type)
                .unwrap()
                .clone(),
            self.register.new_element_uid(idx),
        );
        self.elements.push(element);
        Ok(&self.elements[idx.idx()])
    }


    pub(crate) fn input(&self, input: InputReference) -> Result<IVIDX, &'static str> {
        // Check cache
        if let Some(idx) = self.cache.borrow().input(input) {
            return Ok(idx);
        }

        // compute the change
        let cv_idx = self.change(input.origin)?;

        // check validity
        if let TRIPLink::Input(input_idx) = self.sessions()[cv_idx.session()].versions()
            [cv_idx.version()]
        .execution()
        .trip()[input.number]
        {
            self.cache.borrow().cache_input(input, input_idx.version().clone());
            Ok(input_idx.version().clone())
        } else {
            Err("No corresponding version found")
        }
    }

    // TODO: add sync support
    pub fn change(&self, change: ChangeReference) -> Result<CVIDX, &'static str> {
        // Check cache
        if let Some(idx) = self.cache.borrow().change(change) {
            return Ok(idx);
        }

        // assert the change is from this history
        if &change.history != self.uid() {
            return Err("The Change does not belong to this history");
        }

        let session_idx = self.register().sessions()[&change.session].idx();
        let session = self.sessions()[session_idx];

        // does the Session contain a CV matching the timestamp?
        if let Ok(idx) = session
            .versions()
            .binary_search_by_key(&&change.timestamp, |v| v.timestamp())
        {
            let cvidx = CVIDX::new(session_idx, idx);
            // set idxs
            self.cache.borrow().cache_change(change, cvidx);
            // parent idx
            if let Some(pidx) = self.get(*self.get(change)).parent() {
                let mut path = ChangePath::default();
                path.add_parent(pidx.version());
                //change.sub_change = Some(change.get_path(self, pidx.clone(), path));
            };
            Ok(cvidx)
        } else {
            //TODO[synchronization] change.compute_sync(self, &session)
            Err("No corresponding version found")
        }
    }

    // TODO: reorganize
    pub(crate) fn to_stidx(&self, state: StateReference) -> StIDX {
        let cvidx = self.change(state.origin).expect("The Change corresponding to the StateReference is not in this History");
        StIDX::new(cvidx, state.branch, state.idx, state.sub_state)
    }


    // TODO: add sync, sub state and sub change support
    pub fn state(&self, state: StateReference) -> Result<StateTree, &'static str> {
        // TODO: Paradox management?
        self.get_state(self.to_stidx(state), StateTreeBuilder::new())
    }

    fn get_state(&self, state: StIDX, stb: StateTreeBuilder) -> Result<StateTree, &'static str> {
        // check cache
        let c = self.cache.borrow().state(state);
        if let Ok(tree) = c {
            // use cache data to complete the tree
            tree.nodes().iter().map(|(stph, evidx, children)| stb.complement(*stph, *evidx, *children));
            return Ok(stb.try_into().expect("XXX Completing with a previous StateTree should make the constructed StateTree complete"));
        }

        let exe = self.get(state.cv).execution();

        // get the branch data:

        // detached?
        let (start_state, branch_states) = if let Some(b_idx) = state.branch {
            // Detached
            let branch = exe.detached()[b_idx];
            (branch.branching(), branch.states())
        }
        else {
            // Attached
            (exe.attached().branching(), exe.attached().states())
        };

        // iterate over branch_states from st.idx to the start while incomplete and no cache 
        branch_states[..state.idx].iter().rev().fold_while(stb, |stb, st| {
            stb.complement(stb.0)
        });

        // complementing:
        
        // if incomplete, move to branch pointed at by start_state and call get_state on it


        
        
        let cv_idx = self.change(state.origin)?;
        return Ok(tree);

        //find closest previous cached StTr
        //if some, rebuild from there
        //if none, go to start of Session and rebuild from there

        //let session = self.register.get(state.origin.session);
        // retrieve the closest state and reconstruct the state
        self.cache.borrow().state(cv_idx).map_err(|err| {
            if let Some((ch, st)) = err {
                // start from a previous State found
                self.reconstruct_state_from(cv_idx, ch, st)
            } else {
                // start from Session start
                self.reconstruct_state(cv_idx)
            }
        })
        //.binary_search_by(|c| c.0.cmp(session))
    }

    pub fn new_session(&mut self) -> &Session {
        let idx = self.sessions.len();
        let session = Session::new(self.register.new_session_uid(SIDX::new(idx)));
        self.sessions.push(session);
        &self.sessions[idx]
    }

    pub fn close_session(&mut self, _session: SIDX) {
        /*
        for synchronization in self.sessions[session.idx()].synchronization() {
        match synchronization {
                SessionSynchronization::Start(chr, ph) => {},
                SessionSynchronization::End(chr, orig) => {},
            }
        }
        */
        todo!()
    }


    pub fn reconstruct_state_from(
        &self,
        target: CVIDX,
        start: CVIDX,
        mut tree: StateTree,
    ) -> StateTree {

        tree.apply_…
        // apply changes to tree

        tree
    }

    pub fn reconstruct_state(&self, target: CVIDX) -> StateTree {
        let tree = StateTree::new().apply_branch(session[0], 0);
        self.reconstruct_state_from(target, session[0].branch_state(0), tree)
    }
    /// Records new structures to the history, checking for linked but missing structures and
    /// history validity.
    /// Partial records with missing structures are not yet supported.
    // TODO: add current sync info to register
    fn record<R: ChangeRecord>(&mut self, record: &R) -> Result<(), String> {
        let mut register = Register::default();
        let mut record_register = record.register();

        let mut et_idx = ETIDX::new(self.element_types().len());
        let mut new_ets = record.new_element_types();
        // Gather the new ElementTypes
        for (name, idx) in record_register.element_types() {
            if self.register.element_types().contains_key(name) {
                // the ET is already in the history
                new_ets.remove(idx);
                continue;
            } else if register.add_element_type(name, et_idx) {
                // the ET is new
                et_idx.next();
            } else {
                // the ET has already been registered
                return Err(format!("Duplicate new {} ElementType", name));
            }
        }
        // Check for missing types
        for (&idx, &et) in new_ets.iter() {
            // check inheritance
            let types = et.inherits();
            for t in types {
                if self.register().try_get(t).is_none() && register.try_get(t).is_none() {
                    // an inherited type is unknown
                    return Err(format!("Type {} inherited by {} is unknown", t, et.name()));
                }
            }
            // check attributes
            let attributes = et.attributes();
            for a in attributes {
                if let Some(t) = a.1 {
                    if self.register().try_get(t).is_none() && register.try_get(t).is_none() {
                        // an inherited type is unknown
                        return Err(format!(
                            "Type {} of attribute {}.{} is unknown",
                            t,
                            et.name(),
                            a.0
                        ));
                    }
                }
            }
        }

        let mut it_idx = ITIDX::new(self.input_types().len());
        let mut new_its = record.new_input_types();
        for (name, idx) in record_register.input_types() {
            if self.register.input_types().contains_key(name) {
                // TODO: check all IVs
                new_its.remove(idx);
                continue;
            } else if register.add_input_type(name, it_idx) {
                it_idx.next();
            } else {
                return Err(format!("Duplicate new {} InputType", name));
            }
        }
        // Check for missing types
        for (&idx, &it) in new_its.iter() {
            // check inheritance
            let types = it.inherits();
            for t in types {
                if self.register().try_get(t).is_none() && register.try_get(t).is_none() {
                    // an inherited type is unknown
                    return Err(format!("Type {} inherited by {} is unknown", t, it.name()));
                }
            }
        }

        let mut ct_idx = CTIDX::new(self.command_types().len());
        let mut new_cts = record.new_command_types();
        for (name, idx) in record_register.command_types() {
            if self.register.command_types().contains_key(name) {
                new_cts.remove(idx);
                continue;
            } else if register.add_command_type(name, ct_idx) {
                ct_idx.next();
            } else {
                return Err(format!("Duplicate new {} CommandType", name));
            }
        }
        // Check for missing types
        for (&idx, &ct) in new_cts.iter() {
            // check inheritance
            let types = ct.inherits();
            for t in types {
                if self.register().try_get(t).is_none() && register.try_get(t).is_none() {
                    // an inherited type is unknown
                    return Err(format!("Type {} inherited by {} is unknown", t, ct.name()));
                }
            }
            // check arguments
            let arguments = ct.arguments();
            for a in arguments {
                if let Some(t) = a.1 {
                    if self.register().try_get(t).is_none() && register.try_get(t).is_none() {
                        // an inherited type is unknown
                        return Err(format!(
                            "Type {} of argument {}.{} is unknown",
                            t,
                            ct.name(),
                            a.0
                        ));
                    }
                }
            }
        }

        let mut e_idx = EIDX::new(self.elements().len());
        let mut new_es = record.new_elements();
        for (uid, idx) in record_register.elements() {
            if self.register.elements().contains_key(uid) {
                // TODO: check EVs for new ones
                new_es.remove(idx);
                continue;
            } else if register.add_element(uid, e_idx) {
                if self
                    .register
                    .element_type_name_of(*new_es[idx].element_type())
                    .is_some()
                    || register
                        .element_type_name_of(*new_es[idx].element_type())
                        .is_some()
                {
                    e_idx.next();
                } else {
                    return Err(format!("ElementType of {} is missing", new_es[idx].uid()));
                }
            } else {
                return Err(format!("Duplicate new {} Elements", uid));
            }
        }

        let mut s_idx = SIDX::new(self.sessions().len() - 1);
        let mut new_ss = record.new_sessions();
        for (uid, idx) in record_register.sessions() {
            if self.register.sessions().contains_key(uid) {
                // TODO: check CVs for new ones
                new_ss.remove(idx);
                continue;
            } else if register.add_session(uid, s_idx) {
                s_idx.next();
            } else {
                return Err(format!("Duplicate new {} Sessions", uid));
            }
        }

        // TODO…
        let mut new_evs = record.new_element_versions();
        for (idx, ev) in record_register.element_versions_references() {
            //let eidx = self.register.elements()[record.new_elements()[ev.element()].uid()];
            if self.elements[eidx.idx()].versions().contains_key(ev.uid()) {
                // TODO: check CVs for new ones
                continue;
            } else if register.add_session(s, s_idx) {
                new_ss.push(i);
                s_idx.next();
            } else {
                return Err(format!("Duplicate new {} Sessions", s.uid()));
            }
        }

        // TODO: check E & S Version links (register missing ones)

        for (_, i) in record_register.element_types() {
            self.element_types.push(new_ets[i].clone());
        }
        for (_, i) in record_register.input_types() {
            self.input_types.push(new_its[i].clone());
        }
        for (_, i) in record_register.command_types() {
            self.command_types.push(new_cts[i].clone());
        }
        for (_, i) in record_register.elements() {
            self.elements.push(new_es[i].clone());
        }
        for (_, i) in record_register.sessions() {
            self.sessions.push(new_ss[i].clone());
        }
        self.register.append(register).map_err(|_| {
            format!(
                "[{:?}] The resulting register is invalid",
                Error::ERR_REC_REG
            )
        })
    }
}


impl Hash for History {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.uid.hash(state);
        self.element_types.hash(state);
        self.command_types.hash(state);
        self.input_types.hash(state);
        self.elements.hash(state);
        self.sessions.hash(state);
        self.register.hash(state);
        self.cache.borrow().hash(state);
    }
}


pub trait Idx {
    type Output;
    fn get<'a>(&self, history: &'a History) -> &'a Self::Output;
}

#[derive(Debug)]
pub(crate) enum Error {
    ERR = 0,
    ERR_REC_REG,
}

#[cfg(test)]
mod tests {
    use super::*;

    fn init() {
        History::new();
    }

    #[test]
    fn new_element_type() {}

    #[test]
    fn new_command_type() {}

    #[test]
    fn new_input_type() {}

    #[test]
    fn record_change() {}

    #[test]
    fn record_command_version() {}
}
